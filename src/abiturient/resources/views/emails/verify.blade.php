<div class="container">
    <h5>Account Verification</h5>
    <br>
    <p>{{ $name }},</p>
    <br>
    <p>{{ $msg }}<p>
    <br>
    <a href="{{ $confirmationLink }}">Verify your email address</a>
</div>

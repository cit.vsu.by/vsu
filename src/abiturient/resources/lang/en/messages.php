<?php

return [
    'noFreeDesks' => 'There are no free tables for this time. Please choose another.',
    'alreadySignUp' => 'There is already an entry for this phone number.',
    'fillTheProfile' => 'Complete the profile at least 75%.',
    'registrationHasNotStarted' => 'Registration has not started.',
    'errorReferringToEluni' => 'Error referring to ELUNI. Try again later.',
];

<?php

return [
    'subject' => 'Verify Email Address',
    'line' => 'Click the button below to verify your email address.',
    'action' => 'Verify Email Address',
    'greeting' => 'Hello!',
    'from' => 'VSU named after P. M. Masherov',
];

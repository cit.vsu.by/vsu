<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Services\EducationalInstitutionRefreshingService;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class EducationalInstitutionSeeder extends Seeder
{
    private $educationalInstitutionRefreshingService;

    public function __construct(EducationalInstitutionRefreshingService $educationalInstitutionRefreshinService)
    {
        $this->educationalInstitutionRefreshingService = $educationalInstitutionRefreshinService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->educationalInstitutionRefreshingService->refresh();
    }
}

<?php

namespace Database\Seeders;

use App\Models\Gender;
use Illuminate\Database\Seeder;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class GenderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Gender::create([
            'name' => 'мужской',
        ]);
        Gender::create([
            'name' => 'женский',
        ]);
        // Schema::table('genders')->create();
    }
}

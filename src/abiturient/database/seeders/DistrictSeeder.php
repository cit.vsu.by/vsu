<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Services\DistrictRefreshingService;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class DistrictSeeder extends Seeder
{
    protected $districtRefreshingService;

    public function __construct(DistrictRefreshingService $districtRefreshingService)
    {
        $this->districtRefreshingService = $districtRefreshingService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->districtRefreshingService->refresh();
    }
}

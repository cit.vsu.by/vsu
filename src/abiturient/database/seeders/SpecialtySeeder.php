<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Services\SpecialtyRefreshingService;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class SpecialtySeeder extends Seeder
{
    private $refreshingService;

    public function __construct(SpecialtyRefreshingService $refreshingService)
    {
        $this->refreshingService = $refreshingService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->refreshingService->refresh();
    }
}

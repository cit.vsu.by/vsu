<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Services\TrialRefreshingService;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class TrialSeeder extends Seeder
{
    private $trialRefreshingService;

    public function __construct(TrialRefreshingService $trialRefreshingService)
    {
        $this->trialRefreshingService = $trialRefreshingService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->trialRefreshingService->refresh();
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Services\RegionRefreshingService;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class RegionSeeder extends Seeder
{
    protected $regionRefreshingService;

    public function __construct(RegionRefreshingService $regionsRefreshingService)
    {
        $this->regionRefreshingService = $regionsRefreshingService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->regionRefreshingService->refresh();
    }
}

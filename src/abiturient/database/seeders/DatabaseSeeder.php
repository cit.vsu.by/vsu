<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * Вызывается при выполнении команды:
     * php artisan db:seed
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            GenderSeeder::class,
            CountrySeeder::class,
            RegionSeeder::class,
            DistrictSeeder::class,
            LocalityTypeSeeder::class,
            LocalitySeeder::class,
            PersonalDocumentTypeSeeder::class,
            StreetTypeSeeder::class,
            EducationalDocumentTypeSeeder::class,
            EducationalInstitutionTypeSeeder::class,
            EducationalInstitutionSeeder::class,
            ForeignLanguageSeeder::class,
            HonorSeeder::class,
            SportTypeSeeder::class,
            SportCategorySeeder::class,
            RecruitingOfficeSeeder::class,
            FacultySeeder::class,
            EducationFormSeeder::class,
            SpecialtySeeder::class,
            TrialTypeSeeder::class,
            TrialSeeder::class,
            SpecialtyTrialSeeder::class,
            RoleSeeder::class,
        ]);
    }
}

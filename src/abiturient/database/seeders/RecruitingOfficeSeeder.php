<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Services\RecruitingOfficeRefreshingService;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class RecruitingOfficeSeeder extends Seeder
{
    private $recruitingOfficeRefreshingService;

    public function __construct(RecruitingOfficeRefreshingService $recruitingOfficeRefreshingService)
    {
        $this->recruitingOfficeRefreshingService = $recruitingOfficeRefreshingService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->recruitingOfficeRefreshingService->refresh();
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Services\LocalityRefreshingService;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class LocalitySeeder extends Seeder
{
    protected $localityRefreshingService;

    public function __construct(LocalityRefreshingService $localityRefreshingService)
    {
        $this->localityRefreshingService = $localityRefreshingService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->localityRefreshingService->refresh();
    }
}

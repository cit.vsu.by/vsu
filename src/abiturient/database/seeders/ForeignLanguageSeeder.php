<?php

namespace Database\Seeders;

use App\Models\ForeignLanguage;
use Illuminate\Database\Seeder;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class ForeignLanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ForeignLanguage::create([
            'name' => 'Английский',
        ]);
        ForeignLanguage::create([
            'name' => 'Немецкий',
        ]);
        ForeignLanguage::create([
            'name' => 'Французский',
        ]);
        ForeignLanguage::create([
            'name' => 'Испанский',
        ]);
        ForeignLanguage::create([
            'name' => 'Китайский',
        ]);
        ForeignLanguage::create([
            'name' => 'Латинский',
        ]);
        ForeignLanguage::create([
            'name' => 'Итальянский',
        ]);
        ForeignLanguage::create([
            'name' => 'Русский',
        ]);
    }
}

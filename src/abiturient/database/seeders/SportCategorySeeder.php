<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Services\SportCategoryRefreshingService;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class SportCategorySeeder extends Seeder
{
    private $sportCategoryRefreshingService;

    public function __construct(SportCategoryRefreshingService $sportCategoryRefreshingService)
    {
        $this->sportCategoryRefreshingService = $sportCategoryRefreshingService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->sportCategoryRefreshingService->refresh();
    }
}

<?php

namespace Database\Seeders;

use App\Models\Honor;
use Illuminate\Database\Seeder;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class HonorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Honor::create([
            'name' => 'Диплом с отличием ПТУ',
        ]);
        Honor::create([
            'name' => 'Диплом с отличием ССУЗ',
        ]);
        Honor::create([
            'name' => 'Золотая/серебрянная медаль',
        ]);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Services\FacultyRefreshingService;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class FacultySeeder extends Seeder
{
    private $facultyRefreshingService;

    public function __construct(FacultyRefreshingService $facultyRefreshingService)
    {
        $this->facultyRefreshingService = $facultyRefreshingService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->facultyRefreshingService->refresh();
    }
}

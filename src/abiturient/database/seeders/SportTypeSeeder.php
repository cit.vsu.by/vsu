<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Services\SportTypeRefreshingService;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class SportTypeSeeder extends Seeder
{
    private $sportTypeRefreshingService;

    public function __construct(SportTypeRefreshingService $sportTypeRefreshingService)
    {
        $this->sportTypeRefreshingService = $sportTypeRefreshingService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->sportTypeRefreshingService->refresh();
    }
}

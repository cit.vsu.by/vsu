<?php

namespace Database\Seeders;

use App\Services\CountryRefreshingService;
use Illuminate\Database\Seeder;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class CountrySeeder extends Seeder
{
    private $countryRefreshingService;

    public function __construct(CountryRefreshingService $countryRefreshingService)
    {
        $this->countryRefreshingService = $countryRefreshingService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->countryRefreshingService->refresh();
    }
}

<?php

namespace Database\Seeders;

use App\Models\EducationalDocumentType;
use Illuminate\Database\Seeder;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class EducationalDocumentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EducationalDocumentType::create([
            'name' => 'Аттестат',
        ]);
        EducationalDocumentType::create([
            'name' => 'Диплом',
        ]);
        EducationalDocumentType::create([
            'name' => 'Академическая справка',
        ]);
    }
}

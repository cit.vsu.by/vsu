<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class CreateLocalityTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locality_types', function (Blueprint $table) {
            $table->id();

            $table->string('name')
                ->nullable();
            $table->string('short_name')
                ->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locality_types');
    }
}

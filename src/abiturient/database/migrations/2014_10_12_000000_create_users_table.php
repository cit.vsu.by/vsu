<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * Выполняется при "накатывание" миграции, т.е. во время выполнения команды:
     * php artisan migrate
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            // Создаёт первичный ключ (обязательно, кроме таблиц many-to-many)
            // UNSIGNED BIGINTEGER PRIMARY KEY NOT NULL AUTOINCREMENT
            $table->id();

            $table->string('email')
                ->nullable(false)
                ->unique();
            $table->string('password')
                ->nullable(false);
            $table->timestamp('email_verified_at')
                ->nullable();
            $table->rememberToken();

            $table->string('phone')
                ->nullable();
            $table->string('first_name')
                ->nullable(false);
            $table->string('last_name')
                ->nullable(false);
            $table->string('patronymic')
                ->nullable();
            $table->string('first_name_by')
                ->nullable();
            $table->string('last_name_by')
                ->nullable();
            $table->string('patronymic_by')
                ->nullable();
            $table->string('first_name_en')
                ->nullable();
            $table->string('last_name_en')
                ->nullable();
            // Создаёт внешний ключ по названию поля: gender => genders (таблица), ссылается на поле id
            $table->foreignId('gender_id')
                ->nullable(false)
                ->constrained();
            $table->date('date_of_birth')
                ->nullable(false);

            // Создание внешнего ключа вручну.
            // Создаём колонку citizenship типа UNSIGNED BIGINTEGER
            $table->unsignedBigInteger('citizenship')
                ->nullable(false);
            // Добавляем внешний ключ к полю citizenship
            $table->foreign('citizenship')
                ->references('id')  // колонка, на которую ссылаемся
                ->on('countries');  // таблица
            $table->foreignId('personal_document_type_id')
                ->nullable(false)
                ->constrained();
            $table->string('personal_document_series')
                ->nullable();
            $table->string('personal_document_number')
                ->nullable();
            $table->string('personal_identification_number')
                ->nullable();
            $table->string('personal_document_organization')
                ->nullable();
            $table->date('personal_document_issue_date')
                ->nullable();
            $table->date('personal_document_validity')
                ->nullable();

            $table->unsignedBigInteger('place_of_birth')
                ->nullable();
            $table->unsignedBigInteger('place_of_registration')
                ->nullable();
            $table->string('street')
                ->nullable();
            $table->string('house')
                ->nullable();
            $table->string('housing')
                ->nullable();
            $table->string('apartment')
                ->nullable();
            $table->string('postcode')
                ->nullable();

            $table->foreignId('educational_document_type_id')
                ->nullable()
                ->constrained();
            $table->foreignId('educational_institution_id')
                ->nullable()
                ->constrained();
            $table->date('graduation_date')
                ->nullable();
            $table->string('educational_document_number')
                ->nullable();
            $table->boolean('is_rural_educational_institution')
                ->nullable(false)
                ->default(false); // задание значения по умолчанию для колонки
            $table->boolean('is_pedagogical_class')
                ->nullable(false)
                ->default(false);
            $table->foreignId('foreign_language_id')
                ->nullable()
                ->constrained();
            $table->foreignId('honor_id')
                ->nullable()
                ->constrained();
            $table->boolean('is_pre_university_training')
                ->nullable(false)
                ->default(false);

            $table->string('mother_first_name')
                ->nullable();
            $table->string('mother_last_name')
                ->nullable();
            $table->string('mother_patronymic')
                ->nullable();
            $table->string('mother_work_place')
                ->nullable();
            $table->string('mother_phone')
                ->nullable();
            $table->string('father_first_name')
                ->nullable();
            $table->string('father_last_name')
                ->nullable();
            $table->string('father_patronymic')
                ->nullable();
            $table->string('father_work_place')
                ->nullable();
            $table->string('father_phone')
                ->nullable();

            $table->boolean('is_married')
                ->nullable(false)
                ->default(false);
            $table->integer('number_of_siblings')
                ->nullable(false)
                ->default(0);
            $table->boolean('is_brsm_member')
                ->nullable(false)
                ->default(false);
            $table->boolean('is_dormitory_needed')
                ->nullable(false)
                ->default(false);
            $table->boolean('is_military_duty')
                ->nullable(false)
                ->default(false);
            $table->foreignId('recruiting_office_id')
                ->nullable()
                ->constrained();
            $table->string('work_place')
                ->nullable();
            $table->integer('seniority')
                ->nullable(false)
                ->default(0);

            $table->foreign('place_of_birth')
                ->references('id')
                ->on('localities');
            $table->foreign('place_of_registration')
                ->references('id')
                ->on('localities');

            // Создаёт два поля: created_at и updated_at - показывают дату создания записи и дату последнего изменения записи
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * Выполняет при "откате" миграции, т.е. при команде:
     * php artisan migrate:reset
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

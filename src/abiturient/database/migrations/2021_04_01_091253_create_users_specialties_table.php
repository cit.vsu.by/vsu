<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class CreateUsersSpecialtiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_specialties', function (Blueprint $table) {
            $table->foreignId('user_id')
                ->nullable(false)
                ->contrained();
            $table->foreignId('specialty_id')
                ->nullable(false)
                ->constrained();

            $table->timestamps();

            $table->primary([
                'user_id',
                'specialty_id',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_specialties');
    }
}

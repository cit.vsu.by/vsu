<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class CreateDesksSpecialtiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('desks_specialties', function (Blueprint $table) {
            $table->foreignId('desk_id')
                ->nullable(false)
                ->constrained();
            $table->foreignId('specialty_id')
                ->nullable(false)
                ->constrained();

            $table->timestamps();

            $table->primary([
                'desk_id',
                'specialty_id'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desks_specialties');
    }
}

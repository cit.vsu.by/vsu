<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule', function (Blueprint $table) {
            $table->id();

            $table->foreignId('admission_id')
                ->nullable(false)
                ->constrained();
            $table->date('date')
                ->nullable(false);
            $table->time('start_time')
                ->nullable(false);
            $table->time('end_time')
                ->nullable(false);

            $table->timestamps();

            $table->unique([
                'admission_id',
                'date',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule');
    }
}

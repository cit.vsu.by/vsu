<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class CreateAdmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admissions', function (Blueprint $table) {
            $table->id();

            $table->date('start_date')
                ->nullable(false);
            $table->date('end_date')
                ->nullable(false);
            $table->time('start_time')
                ->nullable(false);
            $table->time('end_time')
                ->nullable(false);
            $table->date('start_registration')
                ->nullable(false);
            $table->date('end_registration')
                ->nullable(false);
            $table->integer('time_for_service')
                ->nullable(false)
                ->unsigned();
            $table->boolean('is_magistrant')
                ->nullable(false)
                ->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admissions');
    }
}

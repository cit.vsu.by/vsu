<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class CreateUsersSportTypesSportCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_sport_types_sport_categories', function (Blueprint $table) {
            $table->id();

            $table->foreignId('user_id')
                ->nullable(false)
                ->constrained();
            $table->foreignId('sport_type_id')
                ->nullable(false)
                ->constrained();
            $table->foreignId('sport_category_id')
                ->nullable(false)
                ->constrained();
            $table->timestamps();

            $table->unique([
                'user_id',
                'sport_type_id',
            ], 'users_sport_types_sport_categories_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_sport_types_sport_categories');
    }
}

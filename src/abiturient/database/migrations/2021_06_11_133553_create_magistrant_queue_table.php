<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagistrantQueueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magistrant_queue', function (Blueprint $table) {
            $table->id();

            $table->string('phone')
                ->nullable(false);
            $table->string('first_name')
                ->nullable(false);
            $table->string('last_name')
                ->nullable(false);
            $table->string('patronymic')
                ->nullable();
            $table->foreignId('admission_id')
                ->nullable(false)
                ->constrained();
            $table->foreignId('desk_id')
                ->nullable(false)
                ->constrained();
            $table->dateTime('date_time')
                ->nullable(false);

            $table->timestamps();

            $table->unique([
                'phone',
                'admission_id',
            ]);
            $table->unique([
                'desk_id',
                'date_time',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('magistrant_queue');
    }
}

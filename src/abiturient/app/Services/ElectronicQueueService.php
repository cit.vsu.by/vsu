<?php

namespace App\Services;

use App\Models\User;
use App\Models\Desk;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;

/**
 * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
 */
class ElectronicQueueService
{
    /**
     * @var AdmissionService;
     */
    private $admissionService;

    public function __construct(AdmissionService $admissionService)
    {
        $this->admissionService = $admissionService;
    }

    /**
     * Returns true if the user attribute counted as percentage of profile completion and false otherwise
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @param string $attribute
     * @param User $user
     * @return boolean
     */
    private function isCounted($attribute, $user)
    {
        switch ($attribute) {
            // This parameters are not counted
            case 'id':
            case 'email':
            case 'emailVerifiedAt':
            case 'createdAt':
            case 'updatedAt':
            case 'rememberToken':
            case 'password':

            case 'patronymic':
            case 'patronymicBy':
            case 'street':
            case 'housing':
            case 'apartment':
            case 'educationalInstitutionId':
            case 'motherPatronymic':
            case 'fatherPatronymic':
            case 'motherWorkPlace':
            case 'fatherWorkPlace':
            case 'motherPhone':
            case 'fatherPhone':
            case 'honorId':
            case 'numberOfSiblings':
            case 'workPlace':
            case 'seniority':

            case 'educationalInstitution':

            case 'isRuralEducationalInstitution':
            case 'isPedagogicalClass':
            case 'isPreUniversityTraining':
            case 'isMarried':
            case 'isBrsmMember':
            case 'isDormitoryNeeded':
            case 'isMilitaryDuty':
                return false;
            // This parameters are counted if personalDocumentTypeId not equals 3
            case 'personalDocumentSeries':
            case 'personalDocumentNumber':
            case 'personalIdentificationNumber':
            case 'personalDocumentIssueDate':
            case 'personalDocumentValidity':
            case 'personalDocumentOrganization':
                return $user->personalDocumentTypeId !== 3;
            // This parameter is counted if isMilitaryDuty is true
            case 'recruitingOfficeId':
                return $user->isMilitaryDuty;
            // All other parameters are counted
            default:
                return true;
        }
    }

    /**
     * Returns true if the user attribute is not empty and false otherwise
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @param string $property
     * @param App\Models\User;
     * @return boolean
     */
    private function isNotEmpty($attribute, $user)
    {
        return isset($user->{$attribute}) &&
            $user->{$attribute} !== '' &&
            $user->{$attribute} !== 0;
    }

    /**
     * Returns the percentage of full user profile
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @param User $user
     * @return integer
     */
    private function getProgress(User $user)
    {
        $countedAttributes = array_filter(array_map(function ($attribute) {
            return Str::camel($attribute);
        }, array_keys($user->getAttributes())), function ($attribute) use ($user) {
            return $this->isCounted($attribute, $user);
        });
        return round(count(array_filter($countedAttributes, function ($attribute) use ($user) {
            return $this->isNotEmpty($attribute, $user);
        })) * 100 / count($countedAttributes));
    }

    /**
     * Checks if the user can sign up in the electronic queue
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @param App\Models\User $user
     * @return boolean
     */
    public function isHasAccess(User $user)
    {
        return $this->getProgress($user) >= 75;
    }

    public function isRegistrationStarted($isMagistrant = false)
    {
        $admission = $this->admissionService->getCurrent($isMagistrant);
        return $admission !== null && date('Y-m-d') >= $admission->startRegistration;
    }

    private function filterFreeDesk($queueTableName, $desk, $dateTime)
    {
        return DB::table($queueTableName)
            ->where('desk_id', $desk->id)
            ->where('date_time', '=', Carbon::parse($dateTime)->toDateTimeString())
            ->get()
            ->isEmpty();
    }

    /**
     * Returns desks that serve specified specialty and free at the specified time
     *
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @param int $specialtyId
     * @param date $dateTime
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function getAvailableDesks($admissionId, $specialtyId, $dateTime)
    {
        return Desk::where('admission_id', $admissionId)
            ->whereHas('specialties', function (Builder $query) use ($specialtyId) {
                $query->where('specialty_id', $specialtyId);
            })->get()
                ->filter(function ($desk) use ($dateTime) {
                    return $this->filterFreeDesk('queue', $desk, $dateTime);
                });
    }

    public function getAvailableMagistrantDesks($admissionId, $dateTime)
    {
        return Desk::where('admission_id', $admissionId)
            ->get()
            ->filter(function ($desk) use ($dateTime) {
                return $this->filterFreeDesk('magistrant_queue', $desk, $dateTime);
            });
    }

    /**
     * Adds user who made the request to queue
     *
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @param App\Models\Desk $desk
     * @param date $dateTime
     * @param int @userId
     */
    public function addToQueue($desk, $dateTime, $userId)
    {
        DB::table('queue')
            ->where('admission_id', $desk->admissionId)
            ->where('user_id', $userId)
            ->delete();
        DB::table('queue')->insert([
            'user_id' => $userId,
            'admission_id' => $desk->admissionId,
            'desk_id' => $desk->id,
            'date_time' => Carbon::parse($dateTime)->toDateTimeString(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        return DB::table('queue')
            ->where('id', DB::getPdo()->lastInsertId())
            ->get();
    }

    public function isAlreadySignUp($admissionId, $phone)
    {
        return !DB::table('magistrant_queue')
            ->where('admission_id', $admissionId)
            ->where('phone', $phone)
            ->get()
            ->isEmpty();
    }

    public function addMagistrantToQueue($desk, $dateTime, $phone, $firstName, $lastName, $patronymic)
    {
        DB::table('magistrant_queue')->insert([
            'phone' => $phone,
            'first_name' => $firstName,
            'last_name' => $lastName,
            'patronymic' => $patronymic,
            'admission_id' => $desk->admissionId,
            'desk_id' => $desk->id,
            'date_time' => Carbon::parse($dateTime)->toDateTimeString(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        return DB::table('magistrant_queue')
            ->where('id', DB::getPdo()->lastInsertId())
            ->get();
    }

    /**
     * Cancels user record in the queue in current year admission
     *
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @param int $id
     */
    public function cancelByUserId($id)
    {
        $admission = $this->admissionService->getCurrent();
        if ($admission !== null) {
            DB::table('queue')
                ->where('user_id', $id)
                ->where('admission_id', $admission->id)
                ->delete();
        }
    }
}

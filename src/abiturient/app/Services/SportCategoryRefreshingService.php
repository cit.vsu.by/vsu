<?php

namespace App\Services;

use App\Models\SportCategory;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class SportCategoryRefreshingService extends RefreshingService
{
    public function __construct()
    {
        parent::__construct();
    }

    public function refresh($onComplete = null)
    {
        foreach ($this->get('sport_categories') as $sportCategory) {
            $dbSportCategory = SportCategory::find($sportCategory['id']);
            if ($dbSportCategory === null)
                SportCategory::create($sportCategory);
            else
                $dbSportCategory->update($sportCategory);
        }
        if (isset($onComplete) && is_callable($onComplete))
            $onComplete();
    }
}

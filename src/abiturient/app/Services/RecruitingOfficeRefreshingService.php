<?php

namespace App\Services;

use App\Models\RecruitingOffice;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class RecruitingOfficeRefreshingService extends RefreshingService
{
    public function __construct()
    {
        parent::__construct();
    }

    public function refresh($onComplete = null)
    {
        foreach ($this->get('recruiting_offices') as $recruitingOffice) {
            $dbRecruitingOffice = RecruitingOffice::find($recruitingOffice['id']);
            if ($dbRecruitingOffice === null)
                RecruitingOffice::create($recruitingOffice);
            else
                $dbRecruitingOffice->update($recruitingOffice);
        }
        if (isset($onComplete) && is_callable($onComplete))
            $onComplete();
    }
}

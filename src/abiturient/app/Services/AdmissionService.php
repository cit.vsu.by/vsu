<?php

namespace App\Services;

use App\Models\Admission;

/**
 * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
 */
class AdmissionService
{
    /**
     * Returns current year admission
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @return App\Models\Admission
     */
    public function getCurrent($isMagistrant = false)
    {
        return Admission::whereYear('start_date', date('Y'))->with([
            'weekends',
            'breaks',
            'desks.specialties',
            'desks.queue',
            'desks.magistrantQueue',
            'schedule',
        ])->where('is_magistrant', $isMagistrant)
            ->first();
    }

    public function getAbiturientCurrent()
    {
        return $this->getCurrent();
    }

    public function getMagistrantCurrent()
    {
        return $this->getCurrent(true);
    }
}

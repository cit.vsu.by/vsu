<?php

namespace App\Services;

use App\Models\Specialty;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class SpecialtyRefreshingService extends RefreshingService
{
    public function __construct()
    {
        parent::__construct();
    }

    public function refresh($onComplete = null)
    {
        foreach ($this->get('specialties') as $entity) {
            $dbEntity = Specialty::find($entity['id']);
            if ($dbEntity === null)
                Specialty::create($entity);
            else
                $dbEntity->update($entity);
        }
        if (isset($onComplete) && is_callable($onComplete))
            $onComplete();
    }
}

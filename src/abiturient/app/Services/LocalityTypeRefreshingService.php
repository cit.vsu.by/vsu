<?php

namespace App\Services;

use App\Models\LocalityType;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class LocalityTypeRefreshingService extends RefreshingService
{
    public function __construct()
    {
        parent::__construct();
    }

    public function refresh($onComplete = null)
    {
        foreach ($this->get('locality_types') as $localityType) {
            $dbLocalityType = LocalityType::find($localityType['id']);
            if ($dbLocalityType === null)
                LocalityType::create($localityType);
            else
                $dbLocalityType->update($localityType);
        }
        if (isset($onComplete) && is_callable($onComplete))
            $onComplete();
    }
}

<?php

namespace App\Services;

use App\Models\EducationalInstitutionType;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class EducationalInstitutionTypeRefreshingService extends LocalityRefreshingService
{
    public function __construct()
    {
        parent::__construct();
    }

    public function refresh($onComplete = null)
    {
        foreach ($this->get('uo_types') as $educationalInstitutionType) {
            $dbEducationInstitutionType = EducationalInstitutionType::find($educationalInstitutionType['id']);
            if ($dbEducationInstitutionType === null)
                EducationalInstitutionType::create($educationalInstitutionType);
            else
                $dbEducationInstitutionType->update($educationalInstitutionType);
        }
        if (isset($onComplete) && is_callable($onComplete))
            $onComplete();
    }
}

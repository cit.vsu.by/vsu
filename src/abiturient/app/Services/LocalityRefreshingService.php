<?php

namespace App\Services;

use App\Models\Locality;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class LocalityRefreshingService extends RefreshingService
{
    public function __construct()
    {
        parent::__construct();
    }

    public function refresh($onComplete = null)
    {
        foreach ($this->get('localities') as $locality) {
            $dbLocality = Locality::find($locality['id']);
            if ($dbLocality === null)
                Locality::create($locality);
            else
                $dbLocality->update($locality);
        }
        if (isset($onComplete) && is_callable($onComplete))
            $onComplete();
    }
}

<?php

namespace App\Services;

use App\Models\EducationalInstitution;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class EducationalInstitutionRefreshingService extends RefreshingService
{
    public function __construct()
    {
        parent::__construct();
    }

    public function refresh($onComplete = null)
    {
        foreach ($this->get('uo') as $educationalInstitution) {
            $dbEducationalInstitution = EducationalInstitution::find($educationalInstitution['id']);
            if ($dbEducationalInstitution === null)
                EducationalInstitution::create($educationalInstitution);
            else
                $dbEducationalInstitution->update($educationalInstitution);
        }
        if (isset($onComplete) && is_callable($onComplete))
            $onComplete();
    }
}

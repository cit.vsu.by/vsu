<?php

namespace App\Services;

use App\Models\Faculty;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class FacultyRefreshingService extends RefreshingService
{
    public function __construct()
    {
        parent::__construct();
    }

    public function refresh($onComplete = null)
    {
        foreach ($this->get('faculties') as $faculty) {
            $dbFaculty = Faculty::find($faculty['id']);
            if ($dbFaculty === null)
                Faculty::create($faculty);
            else
                $dbFaculty->update($faculty);
        }
        if (isset($onComplete) && is_callable($onComplete))
            $onComplete();
    }
}

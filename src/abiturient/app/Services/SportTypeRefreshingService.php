<?php

namespace App\Services;

use App\Models\SportType;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class SportTypeRefreshingService extends RefreshingService
{
    public function __construct()
    {
        parent::__construct();
    }

    public function refresh($onComplete = null)
    {
        foreach ($this->get('sport_types') as $sportType) {
            $dbSportType = SportType::find($sportType['id']);
            if ($dbSportType === null)
                SportType::create($sportType);
            else
                $dbSportType->update($sportType);
        }
        if (isset($onComplete) && is_callable($onComplete))
            $onComplete();
    }
}

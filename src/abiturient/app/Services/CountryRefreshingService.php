<?php

namespace App\Services;

use App\Models\Country;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class CountryRefreshingService extends RefreshingService
{
    public function __construct()
    {
        parent::__construct();
    }

    public function refresh($onComplete = null)
    {
        foreach ($this->get('countries') as $country) {
            $dbCountry = Country::find($country['id']);
            if ($dbCountry === null)
                Country::create($country);
            else
                $dbCountry->update($country);
        }
        if (isset($onComplete) && is_callable($onComplete))
            $onComplete();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Sport category model
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class SportCategory extends AppModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'shortName',
    ];

    public function usersSportTypes()
    {
        return $this->hasMany(UserSportTypeSportCategory::class);
    }
}

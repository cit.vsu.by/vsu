<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Gender model
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class Gender extends AppModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
    ];
}

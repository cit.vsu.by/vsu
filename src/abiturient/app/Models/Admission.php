<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class Admission extends AppModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'startDate',
        'endDate',
        'startTime',
        'endTime',
        'startRegistration',
        'endRegistration',
        'timeForService',
        'isMagistrant',
    ];

    protected $casts = [
        'startDate' => 'date',
        'endDate' => 'date',
        'startTime' => 'string',
        'endTime' => 'string',
        'startRegistration' => 'date',
        'endRegistration' => 'date',
        'isMagistrant' => 'boolean',
    ];

    public function weekends()
    {
        return $this->hasMany(Weekend::class);
    }

    public function breaks()
    {
        return $this->hasMany(AppBreak::class);
    }

    public function desks()
    {
        return $this->hasMany(Desk::class);
    }

    public function schedule()
    {
        return $this->hasMany(Schedule::class);
    }
}

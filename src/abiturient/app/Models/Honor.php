<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Honor model
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class Honor extends AppModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class Desk extends AppModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'description',
        'admissionId',
    ];

    protected $casts = [
        //
    ];

    public function admission()
    {
        return $this->belongsTo(Admission::class);
    }

    public function specialties()
    {
        return $this->belongsToMany(Specialty::class, 'desks_specialties');
    }

    public function queue()
    {
        return $this->hasMany(Queue::class);
    }

    public function magistrantQueue()
    {
        return $this->hasMany(MagistrantQueue::class);
    }
}

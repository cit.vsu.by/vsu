<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Educational document type model
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class EducationalDocumentType extends AppModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Sport type model
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class SportType extends AppModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
    ];

    public function usersSportCategories()
    {
        return $this->hasMany(UserSportTypeSportCategory::class);
    }
}

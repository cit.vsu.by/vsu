<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class MagistrantQueue extends AppModel
{
    use HasFactory;

    protected $table = 'magistrant_queue';

    protected $fillable = [
        'id',
        'phone',
        'firstName',
        'lastName',
        'patronymic',
        'admissionId',
        'deskId',
        'dateTime',
    ];

    protected $casts = [
        'dateTime' => 'date',
    ];

    public function desk()
    {
        return $this->belongsTo(Desk::class);
    }
}

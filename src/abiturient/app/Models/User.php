<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'firstName',
        'lastName',
        'patronymic',
        'firstNameBy',
        'lastNameBy',
        'patronymicBy',
        'firstNameEn',
        'lastNameEn',
        'genderId',
        'dateOfBirth',
        'citizenship',
        'personalDocumentTypeId',
        'personalDocumentSeries',
        'personalDocumentNumber',
        'personalIdentificationNumber',
        'personalDocumentOrganization',
        'personalDocumentIssueDate',
        'personalDocumentValidity',
        'placeOfBirth',
        'placeOfRegistration',
        'street',
        'house',
        'housing',
        'apartment',
        'postcode',
        'educationalDocumentTypeId',
        'educationalInstitutionId',
        'graduationDate',
        'educationalDocumentNumber',
        'isRuralEducationalInstitution',
        'isPedagogicalClass',
        'foreignLanguageId',
        'honorId',
        'isPreUniversityTraining',
        'motherFirstName',
        'motherLastName',
        'motherPatronymic',
        'motherWorkPlace',
        'motherPhone',
        'fatherFirstName',
        'fatherLastName',
        'fatherPatronymic',
        'fatherWorkPlace',
        'fatherPhone',
        'isMarried',
        'numberOfSiblings',
        'isBrsmMember',
        'isDormitoryNeeded',
        'isMilitaryDuty',
        'recruitingOfficeId',
        'phone',
        'workPlace',
        'seniority',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'date_of_birth' => 'date',
        'personal_document_issue_date' => 'date',
        'personal_document_validity' => 'date',
        'graduation_date' => 'date',
        'is_rural_educational_institution' => 'boolean',
        'is_pedagogical_class' => 'boolean',
        'is_married' => 'boolean',
        'number_of_siblings' => 'integer',
        'is_brsm_member' => 'boolean',
        'is_dormitory_needed' => 'boolean',
        'is_military_duty' => 'boolean',
        'seniority' => 'integer',
    ];

    public function placeOfBirthLocality()
    {
        return $this->belongsTo(Locality::class, 'place_of_birth');
    }

    public function placeOfRegistrationLocality()
    {
        return $this->belongsTo(Locality::class, 'place_of_registration');
    }

    public function educationalInstitution()
    {
        return $this->belongsTo(EducationalInstitution::class);
    }

    public function sportTypesSportCategories()
    {
        return $this->hasMany(UserSportTypeSportCategory::class);
    }

    public function queue()
    {
        return $this->hasMany(Queue::class);
    }

    public function specialties()
    {
        return $this->belongsToMany(Specialty::class, 'users_specialties');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'users_roles');
    }

    /**
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @see App\Models\AppModel:getAttribute
     */
    public function getAttribute($key)
    {
        if (array_key_exists($key, $this->getRelations()))
            return parent::getAttribute($key);
        return parent::getAttribute(Str::snake($key));
    }

    /**
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @see App\Models\AppModel:setAttribute
     */
    public function setAttribute($key, $value)
    {
        return parent::setAttribute(Str::snake($key), $value);
    }

    /**
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @see App\Models\AppModel:toArray
     */
    public function toArray()
    {
        $array = parent::toArray();
        $renamed = [];
        foreach ($array as $key => $value)
            $renamed[Str::camel($key)] = $value;
        return $renamed;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class UserSpecialty extends AppModel
{
    use HasFactory;

    protected $table = 'users_specialties';

    protected $fillable = [
        'userId',
        'specialtyId',
    ];
}

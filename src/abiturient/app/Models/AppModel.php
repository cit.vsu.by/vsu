<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Basic class for all application eluqent models
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class AppModel extends Model
{
    use HasFactory;

    /**
     * This method allows to get attributes of model by camel case
     * 
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @link https://laravel.com/docs/8.x/eloquent-mutators
     */
    public function getAttribute($key)
    {
        if (array_key_exists($key, $this->getRelations()))
            return parent::getAttribute($key);
        return parent::getAttribute(Str::snake($key));
    }

    /**
     * This method allows to set attribute values of model by camel case
     * 
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @link https://laravel.com/docs/8.x/eloquent-mutators
     */
    public function setAttribute($key, $value)
    {
        return parent::setAttribute(Str::snake($key), $value);
    }

    /**
     * This method convert model attributes to camel case in json
     * 
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @link https://laravel.com/docs/8.x/eloquent-mutators
     */
    public function toArray()
    {
        $array = parent::toArray();
        $renamed = [];
        foreach ($array as $key => $value)
            $renamed[Str::camel($key)] = $value;
        return $renamed;
    }
}

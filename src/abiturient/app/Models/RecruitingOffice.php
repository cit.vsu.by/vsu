<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Recruiting office model
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class RecruitingOffice extends AppModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
    ];
}

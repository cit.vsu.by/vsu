<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Role extends AppModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
    ];
}

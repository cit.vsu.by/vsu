<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class Specialty extends AppModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'facultyId',
        'educationFormId',
        'year',
    ];

    public function faculty()
    {
        return $this->belongsTo(Faculty::class);
    }

    public function educationForm()
    {
        return $this->belongsTo(EducationForm::class);
    }

    public function desks()
    {
        return $this->belongsToMany(Desk::class, 'desks_specialties');
    }

    public function trials()
    {
        return $this->belongsToMany(Trial::class, 'specialties_trials');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class Trial extends AppModel
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'trialTypeId',
    ];

    public function trialType()
    {
        return $this->belongsTo(TrialType::class);
    }
}

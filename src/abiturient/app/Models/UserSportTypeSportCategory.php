<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * User-SportType-SportCategory relocation model
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class UserSportTypeSportCategory extends AppModel
{
    use HasFactory;

    protected $table = 'users_sport_types_sport_categories';

    protected $fillable = [
        'id',
        'userId',
        'sportTypeId',
        'sportCategoryId',
    ];
}

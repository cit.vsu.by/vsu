<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class SpecialtyTrial extends AppModel
{
    use HasFactory;

    protected $table = 'specialties_trials';

    protected $fillable = [
        'specialtyId',
        'trialId',
    ];
}

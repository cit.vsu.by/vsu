<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Schedule extends AppModel
{
    use HasFactory;

    protected $table = 'schedule';

    protected $fillable = [
        'id',
        'admissionId',
        'date',
        'startTime',
        'endTime',
    ];

    protected $casts = [
        'date' => 'date',
        'startTime' => 'strings',
        'endTime' => 'string',
    ];
}

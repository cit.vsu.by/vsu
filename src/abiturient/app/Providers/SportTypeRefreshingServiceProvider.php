<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Services\SportTypeRefreshingService;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class SportTypeRefreshingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SportTypeRefreshingService::class, function ($app) {
            return new SportTypeRefreshingService;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

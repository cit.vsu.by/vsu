<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Services\UserSpecialtyService;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class UserSpecialtyServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserSpecialtyService::class, function ($app) {
            return new UserSpecialtyService;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

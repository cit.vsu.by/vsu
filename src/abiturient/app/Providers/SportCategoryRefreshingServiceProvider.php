<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\SportCategoryRefreshingService;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class SportCategoryRefreshingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SportCategoryRefreshingService::class, function ($app) {
            return new SportCategoryRefreshingService;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

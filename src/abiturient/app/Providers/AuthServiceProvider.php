<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Notifications\Messages\MailMessage;

use App\Models\User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('employee', function (User $user) {
            return $user->roles
                ->contains('name', 'EMPLOYEE');
        });
        Gate::define('developer', function (User $user) {
            return $user->roles
                ->contains('name', 'DEVELOPER');
        });

        VerifyEmail::toMailUsing(function ($notifiable, $url) {
            return (new MailMessage)
                ->subject(__('emailVerification.subject'))
                ->from(env('MAIL_FROM_ADDRESS'), __('emailVerification.from'))
                ->greeting(__('emailVerification.greeting'))
                ->line(__('emailVerification.line'))
                ->action(__('emailVerification.action'), $url);
        });
    }
}

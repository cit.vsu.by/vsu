<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\RecruitingOfficeRefreshingService;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class RecruitingOfficeRefreshingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        return $this->app->singleton(RecruitingOfficeRefreshingService::class, function ($app) {
            return new RecruitingOfficeRefreshingService;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

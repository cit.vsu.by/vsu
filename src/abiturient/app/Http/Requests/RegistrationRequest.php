<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Request to save registration info
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users,email',
            'password' => 'required|string',
            'firstName' => 'required|string',
            'lastName' => 'required|string',
            'patronymic' => 'nullable|string',
            'genderId' => 'required|exists:genders,id',
            'dateOfBirth' => 'required|date',
            'citizenship' => 'required|exists:countries,id',
            'personalDocumentTypeId' => 'required|exists:personal_document_types,id',
            'personalDocumentSeries' => 'nullable|string',
            'personalDocumentNumber' => 'nullable|string',
        ];
    }
}

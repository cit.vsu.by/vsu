<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Request to save passport info
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class PassportInfoPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'citizenship' => 'required|exists:countries,id',
            'personalDocumentTypeId' => 'required|exists:personal_document_types,id',
            'personalDocumentSeries' => 'nullable|string',
            'personalDocumentNumber' => 'nullable|string',
            'personalIdentificationNumber' => 'nullable|string',
            'personalDocumentOrganization' => 'nullable|string',
            'personalDocumentIssueDate' => 'nullable|date',
            'personalDocumentValidity' => 'nullable|date',
        ];
    }
}

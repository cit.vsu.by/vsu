<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Request to save family info
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class FamilyPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'motherFirstName' => 'nullable|string',
            'motherLastName' => 'nullable|string',
            'motherPatronymic' => 'nullable|string',
            'motherWorkPlace' => 'nullable|string',
            'motherPhone' => 'nullable|regex:/^[+][0-9]{12}$/',
            'fatherFirstName' => 'nullable|string',
            'fatherLastName' => 'nullable|string',
            'fatherPatronymic' => 'nullable|string',
            'fatherWorkPlace' => 'nullable|string',
            'fatherPhone' => 'nullable|regex:/^[+][0-9]{12}$/',
            'isMarried' => 'required|boolean',
            'numberOfSiblings' => 'required|numeric|min:0',
        ];
    }
}

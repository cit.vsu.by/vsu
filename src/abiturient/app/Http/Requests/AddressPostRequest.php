<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Request to save address info
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class AddressPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'placeOfBirth' => 'nullable|exists:localities,id',
            'placeOfRegistration' => 'nullable|exists:localities,id',
            'street' => 'nullable|string',
            'house' => 'nullable|string',
            'housing' => 'nullable|string',
            'apartment' => 'nullable|string',
            'postcode' => 'nullable|string',
        ];
    }
}

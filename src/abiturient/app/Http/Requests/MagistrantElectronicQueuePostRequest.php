<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MagistrantElectronicQueuePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'patronymic' => 'nullable',
            'dateTime' => 'required|date',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Request to save basic info
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class BasicInfoPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'required|string',
            'lastName' => 'required|string',
            'patronymic' => 'nullable|string',
            'firstNameBy' => 'nullable|string',
            'lastNameBy' => 'nullable|string',
            'patronymicBy' => 'nullable|string',
            'firstNameEn' => 'nullable|string',
            'lastNameEn' => 'nullable|string',
            'genderId' => 'required|exists:genders,id',
            'dateOfBirth' => 'required|date',
            'phone' => 'nullable|regex:/^[+][0-9]{12}$/',
        ];
    }
}

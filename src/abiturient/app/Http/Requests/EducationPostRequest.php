<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Request to save education info
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class EducationPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'educationalDocumentTypeId' => 'nullable|exists:educational_document_types,id',
            'educationalInstitutionId' => 'nullable|exists:educational_institutions,id',
            'graduationDate' => 'nullable|date',
            'educationalDocumentNumber' => 'nullable|string',
            'isRuralEducationalInstitution' => 'required|boolean',
            'isPedagogicalClass' => 'required|boolean',
            'foreignLanguageId' => 'nullable|exists:foreign_languages,id',
            'honorId' => 'nullable|exists:honors,id',
            'isPreUniversityTraining' => 'required|boolean',
        ];
    }
}

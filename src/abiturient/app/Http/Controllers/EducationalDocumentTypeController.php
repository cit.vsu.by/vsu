<?php

namespace App\Http\Controllers;

use App\Models\EducationalDocumentType;

/**
 * Educational document type collection controller
 *
 * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
 */
class EducationalDocumentTypeController extends Controller
{
    /**
     * Returns educational document type by id or all types if id param is null
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @param int|null $id
     * @return App\Models\EducationalDocumentType|Illuminate\Database\Eloquent\Collection|null
     */
    public function get($id = null)
    {
        return $id === null ? EducationalDocumentType::all() : EducationalDocumentType::find($id);
    }
}

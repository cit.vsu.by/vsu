<?php

namespace App\Http\Controllers;

use App\Models\Specialty;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class SpecialtyController extends Controller
{
    /**
     * Returns specialty by id or all specialties if id param is null
     *
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @param int $id
     * @return App\Models\Specialty|Illuminate\Database\Eloquent\Collection
     */
    public function get($id = null)
    {
        return $id === null ? Specialty::all() : Specialty::find($id);
    }

    /**
     * Returns current year specialties
     *
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function getCurrentYear()
    {
        return Specialty::with([
            'trials.trialType',
            'faculty',
            'educationForm',
        ])->where('year', date('Y'))
            ->get();
    }
}

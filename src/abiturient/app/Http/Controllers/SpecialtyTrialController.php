<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Facades\Gate;

use App\Services\FacultyRefreshingService;
use App\Services\SpecialtyRefreshingService;
use App\Services\TrialRefreshingService;
use App\Services\SpecialtyTrialRefreshingService;

/**
 * Specialties-trials reference collection controller
 *
 * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
 */
class SpecialtyTrialController extends Controller
{
    private FacultyRefreshingService $facultyRefreshingService;

    private SpecialtyRefreshingService $specialtyRefreshingService;

    private TrialRefreshingService $trialRefreshingService;

    private SpecialtyTrialRefreshingService $specialtyTrialRefreshingService;

    public function __construct(
        FacultyRefreshingService $facultyRefreshingService,
        SpecialtyRefreshingService $specialtyRefreshingService,
        TrialRefreshingService $trialRefreshingService,
        SpecialtyTrialRefreshingService $specialtyTrialRefreshingService
    ) {
        $this->facultyRefreshingService = $facultyRefreshingService;
        $this->specialtyRefreshingService = $specialtyRefreshingService;
        $this->trialRefreshingService = $trialRefreshingService;
        $this->specialtyTrialRefreshingService = $specialtyTrialRefreshingService;
    }

    public function refresh()
    {
        if (!Gate::allows('developer'))
            return response(null, 403);
        try {
            $this->facultyRefreshingService->refresh(function () {
                $this->specialtyRefreshingService->refresh(function () {
                    $this->trialRefreshingService->refresh(function () {
                        $this->specialtyTrialRefreshingService->refresh();
                    });
                });
            });
        } catch (ConnectException $exception) {
            return response(__('messages.errorReferringToEluni'), 500);
        }
        return response(null);
    }
}

<?php

namespace App\Http\Controllers;

use App\Services\AdmissionService;

/**
 * Admission collection controller
 *
 * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
 * Новый комментарий
 */
class AdmissionController extends Controller
{
    /**
     * @var App\Services\AdmissionService
     */
    private $admissionService;

    public function __construct(AdmissionService $admissionService)
    {
        $this->admissionService = $admissionService;
    }

    /**
     * Returns an admission of the current year for abiturients
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @return App\Models\Admission|null
     */
    public function getAbiturientCurrent()
    {
        return $this->admissionService
            ->getAbiturientCurrent();
    }

    /**
     * Returns an admission of the current year for magistrants
     *
     * @return App\Models\Admission|null
     */
    public function getMagistrantCurrent()
    {
        return $this->admissionService
            ->getMagistrantCurrent();
    }
}

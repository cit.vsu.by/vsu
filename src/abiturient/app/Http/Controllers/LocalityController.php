<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Facades\Gate;

use App\Models\Locality;
use App\Services\CountryRefreshingService;
use App\Services\RegionRefreshingService;
use App\Services\DistrictRefreshingService;
use App\Services\LocalityTypeRefreshingService;
use App\Services\LocalityRefreshingService;

/**
 * Locality collection controller
 *
 * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
 */
class LocalityController extends Controller
{
    private CountryRefreshingService $countryRefreshingService;

    private RegionRefreshingService $regionRefreshingService;

    private DistrictRefreshingService $districtRefreshingService;

    private LocalityTypeRefreshingService $localityTypeRefreshingService;

    private LocalityRefreshingService $localityRefreshingService;

    public function __construct(
        CountryRefreshingService $countryRefreshingService,
        RegionRefreshingService $regionRefreshingService,
        DistrictRefreshingService $districtRefreshingService,
        LocalityTypeRefreshingService $localityTypeRefreshingService,
        LocalityRefreshingService $localityRefreshingService
    ) {
        $this->countryRefreshingService = $countryRefreshingService;
        $this->regionRefreshingService = $regionRefreshingService;
        $this->districtRefreshingService = $districtRefreshingService;
        $this->localityTypeRefreshingService = $localityTypeRefreshingService;
        $this->localityRefreshingService = $localityRefreshingService;
    }

    /**
     * Returns locality by id or all localities if id param is null
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @param int|null $id
     * @return App\Models\Locality|Illuminate\Database\Eloquent\Collection|null
     */
    public function get($id = null)
    {
        return $id === null ? Locality::all() : Locality::find($id);
    }

    public function refresh()
    {
        if (!Gate::allows('developer'))
            return response(null, 403);
        try {
            $this->countryRefreshingService->refresh(function () {
                $this->regionRefreshingService->refresh(function () {
                    $this->districtRefreshingService->refresh(function () {
                        $this->localityTypeRefreshingService->refresh(function () {
                            $this->localityRefreshingService->refresh();
                        });
                    });
                });
            });
        } catch (ConnectException $exception) {
            return response(__('messages.errorReferringToEluni'), 500);
        }
        return response(null);
    }
}

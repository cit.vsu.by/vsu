<?php

namespace App\Http\Controllers;

use App\Models\Country;

/**
 * Country collection controller
 *
 * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
 */
class CountryController extends Controller
{
    /**
     * Returns country by id or all countries if id param is null
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @param int|null $id
     * @return App\Models\Country|Illuminate\Database\Eloquent\Collection|null
     */
    public function get($id = null)
    {
        return $id === null ? Country::all() : Country::find($id);
    }
}

<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Facades\Gate;

use App\Models\SportType;
use App\Services\SportTypeRefreshingService;

/**
 * Sport type collection controller
 *
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class SportTypeController extends Controller
{
    private SportTypeRefreshingService $sportTypeRefreshingService;

    public function __construct(SportTypeRefreshingService $sportTypeRefreshingService)
    {
        $this->sportTypeRefreshingService = $sportTypeRefreshingService;
    }

    /**
     * Returns sport type by id or all types if id param is null
     *
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @param int $id
     * @return App\Models\SportType|Illuminate\Database\Eloquent\Collection
     */
    public function get($id = null)
    {
        return $id === null ? SportType::all() : SportType::find($id);
    }

    public function refresh()
    {
        if (!Gate::allows('developer'))
            return response(null, 403);
        try {
            $this->sportTypeRefreshingService->refresh();
        } catch (ConnectException $exception) {
            return response(__('messages.errorReferringToEluni'), 500);
        }
        return response(null);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Gate;

use App\Models\Queue;
use App\Models\MagistrantQueue;
use App\Services\AdmissionService;

class MonitoringController extends Controller
{
    private AdmissionService $admissionService;

    public function __construct(AdmissionService $admissionService)
    {
        $this->admissionService = $admissionService;
    }

    public function getAbiturientScheduleByDeskId(int $deskId = 0)
    {
        if (!Gate::allows('employee'))
            return response(null, 403);

        return $deskId !== 0 ?
            Queue::with('user', 'desk')
                ->where("desk_id", $deskId)
                ->get() :
            Queue::with('user', 'desk')
                ->where(
                    'admission_id',
                    $this->admissionService->getAbiturientCurrent()->id
                )
                ->get();
    }

    public function getMagistrantScheduleByDeskId(int $deskId = 0)
    {
        if (!Gate::allows('employee'))
            return response(null, 403);

        return $deskId !== 0 ?
            MagistrantQueue::with('desk')
                ->where('desk_id', $deskId)
                ->get() :
            MagistrantQueue::with('desk')
                ->where(
                    'admission_id',
                    $this->admissionService->getMagistrantCurrent()->id
                )
                ->get();
    }
}

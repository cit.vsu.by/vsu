<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Facades\Gate;

use App\Models\EducationalInstitution;
use App\Services\EducationalInstitutionTypeRefreshingService;
use App\Services\EducationalInstitutionRefreshingService;

/**
 * Educational intitution collection controller
 *
 * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
 */
class EducationalInstitutionController extends Controller
{
    private EducationalInstitutionTypeRefreshingService $educationalInstitutionTypeRefreshingService;

    private EducationalInstitutionRefreshingService $educationalInstitutionRefreshingService;

    public function __construct(
        EducationalInstitutionTypeRefreshingService $educationalInstitutionTypeRefreshingService,
        EducationalInstitutionRefreshingService $educationalInstitutionRefreshingService
    ) {
        $this->educationalInstitutionTypeRefreshingService = $educationalInstitutionTypeRefreshingService;
        $this->educationalInstitutionRefreshingService = $educationalInstitutionRefreshingService;
    }

    /**
     * Returns educational institution by id or all institutions if id param is null
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @param int|null $id
     * @return App\Models\EducationalInstitution|Illuminate\Database\Eloquent\Collection|null
     */
    public function get($id = null)
    {
        return $id === null ? EducationalInstitution::all() : EducationalInstitution::find($id);
    }

    public function refresh()
    {
        if (!Gate::allows('developer'))
            return response(null, 403);
        try {
            $this->educationalInstitutionTypeRefreshingService->refresh(function () {
                $this->educationalInstitutionRefreshingService->refresh();
            });
        } catch (ConnectException $exception) {
            return response(__('messages.errorReferringToEluni'), 500);
        }
        return response(null);
    }
}

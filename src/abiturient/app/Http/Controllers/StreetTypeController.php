<?php

namespace App\Http\Controllers;

use App\Models\StreetType;

/**
 * Street type collection controller
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class StreetTypeController extends Controller
{
    /**
     * Returns street type by id or all types if id param is null
     * 
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @param int $id
     * @return App\Models\StreetType|Illuminate\Database\Eloquent\Collection
     */
    public function get($id = null)
    {
        return $id === null ? StreetType::all() : StreetType::find($id);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\LocalityType;

/**
 * Locality type collection controller
 *
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class LocalityTypeController extends Controller
{
    /**
     * Returns locality type by id or all types if id param is null
     *
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @param int $id
     * @return App\Models\LocalityType|Illuminate\Database\Eloquent\Collection
     */
    public function get($id = null)
    {
        return $id === null ? LocalityType::all() : LocalityType::find($id);
    }
}

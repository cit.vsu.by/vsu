<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Services\ElectronicQueueService;
use App\Services\AdmissionService;
use App\Models\User;
use App\Http\Requests\ElectronicQueuePostRequest;
use App\Http\Requests\MagistrantElectronicQueuePostRequest;

/**
 * Electronic queue collection controller
 *
 * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
 */
class ElectronicQueueController extends Controller
{
    /**
     * @var ElectronicQueueService
     */
    private $electronicQueueService;

    /**
     * @var AdmissionService
     */
    private $admissionService;

    public function __construct(ElectronicQueueService $electronicQueueService, AdmissionService $admissionService)
    {
        $this->electronicQueueService = $electronicQueueService;
        $this->admissionService = $admissionService;
    }

    /**
     * Returns true if the user has access to the registration in electronic queue and false otherwise
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @return boolean
     */
    public function isHasAccess()
    {
        if ($this->electronicQueueService->isHasAccess(User::find(Auth::id())))
            return response(null);
        else
            return response(null, 403);
    }

    /**
     * Adds a person who made the request to the queue for abiturients if possible.
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @param App\Http\Requests\ElectronicQueuePostRequest $request
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function postMe(ElectronicQueuePostRequest $request)
    {
        $validated = $request->validated();
        $user = User::find(Auth::id());
        if (!$this->electronicQueueService->isHasAccess($user))
            return response(__('messages.fillTheProfile'), 403);
        if (!$this->electronicQueueService->isRegistrationStarted())
            return response(__('messages.registrationHasNotStarted'));
        $desks = $this->electronicQueueService->getAvailableDesks(
            $this->admissionService->getAbiturientCurrent()->id,
            $validated['specialtyId'],
            $validated['dateTime']
        );
        if ($desks->isEmpty())
            return response(__('messages.noFreeDesks'), 422);
        return $this->electronicQueueService->addToQueue(
            $desks->first(),
            $validated['dateTime'],
            $user->id
        );
    }

    /**
     * Adds a person who made the request to the queue for magistrants if possible
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @param MagistrantElectronicQueuePostRequest $request
     * @return Symfony\Component\HttpFoundation\Response|Illuminate\Database\Eloquent\Collection
     */
    public function postMagistrant(MagistrantElectronicQueuePostRequest $request)
    {
        $validated = $request->validated();
        $desks = $this->electronicQueueService
            ->getAvailableMagistrantDesks(
                $this->admissionService->getMagistrantCurrent()->id,
                $validated['dateTime']
            );
        if ($desks->isEmpty())
            return response(__('messages.noFreeDesks'), 422);
        if ($this->electronicQueueService->isAlreadySignUp(
            $this->admissionService->getMagistrantCurrent()->id,
            $validated['phone']
        ))
            return response(__('messages.alreadySignUp'), 422);
        return $this->electronicQueueService->addMagistrantToQueue(
            $desks->first(),
            $validated['dateTime'],
            $validated['phone'],
            $validated['firstName'],
            $validated['lastName'],
            $validated['patronymic']
        );
    }

    /**
     * Delets a person who made the request from the queue for abiturients
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function deleteMe()
    {
        $this->electronicQueueService
            ->cancelByUserId(Auth::id());
        return response(null);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\EmailVerificationRequest;

use App\Http\Requests\RegistrationRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Models\User;

/**
 * Controller containing methods for registration and authorization
 *
 * Application uses Laravel Sanctum authentication system. User data is stored in database.
 *
 * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
 * @author Illia Tsikhanau (illiatsikhanau@gmail.com)
 * @link https://laravel.com/docs/8.x/authentication#authenticating-users
 * @link https://laravel.com/docs/8.x/sanctum
 */
class AuthController extends Controller
{
    /**
     * This method authorizes the user in the system
     *
     * The request must contain body with email, password and isRemember parameters
     * The response returns status 200 OK if authorization is ok or 401 UNAUTHORIZED otherwise
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @param Illuminate\Http\Request
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $isRemember = $request->boolean('isRemember');

        if (Auth::attempt($credentials, $isRemember)) {
            $request->session()
                ->regenerate();
            return response(null);
        }
        return response(__('auth.failed'), 401);
    }

    /**
     * This method logouts user from the system
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @param Illuminate\Http\Request $request
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()
            ->invalidate();
        $request->session()
            ->regenerateToken();
        return response(null);
    }

    /**
     * This method registers new user in the system and generates event to send confirmation message to his email
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @author Illia Tsikhanau (illiatsikhanau@gmail.com)
     * @param App\Http\Requests\RegistrationRequest
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function register(RegistrationRequest $request)
    {
        $validated = $request->validated();
        $validated['id'] = null;
        $validated['password'] = Hash::make($validated['password']);
        $user = User::create($validated);

        event(new Registered($user));

        Auth::login($user);

        return response(null);
    }

    /**
     * This method verifies email by clicking the link in verification email message and redirects to the path from
     * the MAIL_REDIRECT environment variable
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @author Illia Tsikhanau (illiatsikhanau@gmail.com)
     * @param Illuminate\Foundation\Auth\EmailVerificationRequest
     * @return Illuminate\Http\RedirectResponse
     */
    public function verify(EmailVerificationRequest $request)
    {
        $request->fulfill();
        return redirect(env(
            'MAIL_REDIRECT',
            env('APP_URL', 'localhost')
        ));
    }

    /**
     * This method resends verification email message
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @param Illuminate\Http\Request
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function resend(Request $request)
    {
        $request->user()
            ->sendEmailVerificationNotification();
        return response(null);
    }

    /**
     * Change password for user who made the request
     *
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @param App\Http\Requests\ChangePasswordRequest
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        User::find(Auth::id())
            ->update(['password' => Hash::make($request->validated()['newPassword'])]);
        return response(null);
    }
}

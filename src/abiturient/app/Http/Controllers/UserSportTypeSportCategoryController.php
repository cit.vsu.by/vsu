<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserSportTypeSportCategoryPostRequest;
use App\Models\UserSportTypeSportCategory;
use Illuminate\Support\Facades\Auth;

/**
 * User-SportType-SportCategory relation collection controller
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 * @author Illia Thikhanau
 */
class UserSportTypeSportCategoryController extends Controller
{
    /**
     * Returns User-SportType-SportCategory relations for user who made the request
     * 
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     */
    public function get()
    {
        return UserSportTypeSportCategory::where('user_id', Auth::id())
            ->get();
    }

    /**
     * Saves relation for user who made the request
     * 
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @author Illia Thikhanau
     */
    public function post(UserSportTypeSportCategoryPostRequest $request)
    {
        $validated = $request->validated();
        $validated['userId'] = Auth::id();

        $dbUserSportTypeSportCategory = UserSportTypeSportCategory::where('user_id', $validated['userId'])
            ->where('sport_type_id', $validated['sportTypeId']);

        if ($dbUserSportTypeSportCategory->get()->isEmpty())
            return UserSportTypeSportCategory::create($validated);
        else
            return $dbUserSportTypeSportCategory->first()
                ->update($validated);
    }

    /**
     * Deletes reloation for user who made the request
     * 
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @author Illia Thikhanau
     */
    public function delete($sportTypeId)
    {
        UserSportTypeSportCategory::where('user_id', Auth::id())
            ->where('sport_type_id', $sportTypeId)
            ->delete();
        return response(null);
    }
}

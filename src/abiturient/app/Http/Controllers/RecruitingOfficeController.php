<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Facades\Gate;

use App\Models\RecruitingOffice;
use App\Services\RecruitingOfficeRefreshingService;

/**
 * Recruiting office collection controller
 *
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class RecruitingOfficeController extends Controller
{
    private RecruitingOfficeRefreshingService $recruitingOfficeRefreshingService;

    public function __construct(RecruitingOfficeRefreshingService $recruitingOfficeRefreshingService)
    {
        $this->recruitingOfficeRefreshingService = $recruitingOfficeRefreshingService;
    }

    /**
     * Returns recruting office by id or all offices if id param is null
     *
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @param int $id
     * @return App\Models\RecruitingOffice|Illuminate\Database\Eloquent\Collection
     */
    public function get($id = null)
    {
        return $id === null ? RecruitingOffice::all() : RecruitingOffice::find($id);
    }

    public function refresh()
    {
        if (!Gate::allows('developer'))
            return response(null, 403);
        try {
            $this->recruitingOfficeRefreshingService->refresh();
        } catch (ConnectException $exception) {
            return response(__('messages.errorReferringToEluni'), 500);
        }
        return response(null);
    }
}

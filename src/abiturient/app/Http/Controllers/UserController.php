<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

use App\Models\User;
use App\Http\Requests\BasicInfoPostRequest;
use App\Http\Requests\EducationPostRequest;
use App\Http\Requests\FamilyPostRequest;
use App\Http\Requests\OtherPostRequest;
use App\Http\Requests\PassportInfoPostRequest;
use App\Http\Requests\AddressPostRequest;
use App\Http\Requests\CheckEmailRequest;

/**
 * User collection controller
 *
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class UserController extends Controller
{
    /**
     * Returns user by id or all users if id param is null
     *
     * @param int|null $id
     * @return mixed
     */
    public function get($id = null)
    {
        if (!Gate::allows('employee'))
            return response(null, 403);
        $fields = [
            'id',
            'first_name',
            'last_name',
            'patronymic',
            'email',
            'phone',
            'updated_at',
        ];
        return $id === null ? User::select($fields)->get() : User::where('id', $id)->select($fields)->get();
    }

    /**
     * Returns data of authorized user who made the request
     *
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @return User
     */
    public function getMe()
    {
        return User::with([
            'placeOfBirthLocality.district.region.country',
            'placeOfRegistrationLocality.district.region.country',
            'educationalInstitution',
            'queue.desk',
            'specialties',
            'roles',
        ])->find(Auth::id());
    }

    /**
     * Saves basic info for user who made the request
     *
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @return App\Models\User
     */
    public function postBasicInfo(BasicInfoPostRequest $request)
    {
        return User::findOrFail(Auth::id())->update($request->validated());
    }

    /**
     * Saves passprot info for user who made the request
     *
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @return App\Models\User
     */
    public function postPassportInfo(PassportInfoPostRequest $request)
    {
        return User::findOrFail(Auth::id())
            ->update($request->validated());
    }

    /**
     * Saves address info for user who made the request
     *
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @return App\Models\User
     */
    public function postAddress(AddressPostRequest $request)
    {
        return User::findOrFail(Auth::id())
            ->update($request->validated());
    }

    /**
     * Saves education info for user who made the request
     *
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @return App\Models\User
     */
    public function postEducation(EducationPostRequest $request)
    {
        return User::findOrFail(Auth::id())
            ->update($request->validated());
    }

    /**
     * Saves family info for user who made the request
     *
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @return App\Models\User
     */
    public function postFamily(FamilyPostRequest $request)
    {
        return User::findOrFail(Auth::id())->update($request->validated());
    }

    /**
     * Saves other info for user who made the request
     *
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @return App\Models\User
     */
    public function postOther(OtherPostRequest $request)
    {
        return User::findOrFail(Auth::id())
            ->update($request->validated());
    }

    /**
     * This method checks if email is unique
     *
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @param App\Http\Requests\CheckEmailRequest
     * @return string
     */
    public function checkEmail(CheckEmailRequest $request)
    {
        return User::where('email', $request->validated()['email'])
            ->get()
            ->isEmpty() ? 'true' : 'false';
    }
}

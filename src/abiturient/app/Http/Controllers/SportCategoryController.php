<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Facades\Gate;

use App\Models\SportCategory;
use App\Services\SportCategoryRefreshingService;

/**
 * Sport category collection controller
 *
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class SportCategoryController extends Controller
{
    private SportCategoryRefreshingService $sportCategoryRefreshingService;

    public function __construct(SportCategoryRefreshingService $sportCategoryRefreshingService)
    {
        $this->sportCategoryRefreshingService = $sportCategoryRefreshingService;
    }

    /**
     * Returns sport category by id or all categories if id param is null
     *
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @param int $id
     * @return App\Models\SportCategory|Illuminate\Database\Eloquent\Collection
     */
    public function get($id = null)
    {
        return $id === null ? SportCategory::all() : SportCategory::find($id);
    }

    public function refresh()
    {
        if (!Gate::allows('developer'))
            return response(null, 403);
        try {
            $this->sportCategoryRefreshingService->refresh();
        } catch (ConnectException $exception) {
            return response(__('messages.errorReferringToEluni'), 500);
        }
        return response(null);
    }
}

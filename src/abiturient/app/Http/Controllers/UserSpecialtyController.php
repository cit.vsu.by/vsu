<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use App\Services\UserSpecialtyService;
use App\Http\Requests\UserSpecialtyPostRequest;

/**
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class UserSpecialtyController extends Controller
{
    /**
     * @var App\Services\UserSpecialtyService
     */
    private $userSpecialtyService;

    public function __construct(UserSpecialtyService $userSpecialtyService)
    {
        $this->userSpecialtyService = $userSpecialtyService;
    }

    /**
     * Returns all user specialties
     * 
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function getMe()
    {
        return $this->userSpecialtyService->getByUserId(Auth::id());
    }

    /**
     * Saves new user-specialty relation
     * 
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @param App\Http\Requests\UserSpecialtyPostRequest
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function post(UserSpecialtyPostRequest $request)
    {
        return response($this->userSpecialtyService->save(
            Auth::id(),
            $request->validated()['specialtyId']
        ), 201);
    }

    /**
     * Deletes user-specialty relationship for user who made the request
     * 
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @param int $specialtyId
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function delete($specialtyId)
    {
        $this->userSpecialtyService->deleteByUserIdAndSpecialtyId(
            Auth::id(),
            $specialtyId
        );
        return response(null);
    }
}

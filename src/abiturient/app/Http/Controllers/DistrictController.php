<?php

namespace App\Http\Controllers;

use App\Models\District;

/**
 * District collection controller
 *
 * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
 */
class DistrictController extends Controller
{
    /**
     * Returns district by id or all districts if id param is null
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @param int|null $id
     * @return App\Models\District|Illuminate\Database\Eloquent\Collection|null
     */
    public function get($id = null)
    {
        return $id === null ? District::all() : District::find($id);
    }
}

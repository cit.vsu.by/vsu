<?php

namespace App\Http\Controllers;

use App\Models\PersonalDocumentType;

/**
 * Personal document type collection controller
 * 
 * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
 */
class PersonalDocumentTypeController extends Controller
{
    /**
     * Returns personal document type by id or all types if id param is null
     * 
     * @author Vitaly Lobatsevich <vitaly.lobatsevich@gmail.com>
     * @param int $id
     * @return App\Models\PersonalDocumentType|Illuminate\Database\Eloquent\Collection
     */
    public function get($id = null)
    {
        return $id === null ? PersonalDocumentType::all() : PersonalDocumentType::find($id);
    }
}

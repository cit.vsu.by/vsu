<?php

namespace App\Http\Controllers;

use App\Models\EducationForm;

/**
 * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
 */
class EducationFormController extends Controller
{
    /**
     * Returns faculty by id or all faculties if id param is null
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @param int|null $id
     * @return App\Models\EducationForm|Illuminate\Database\Eloquent\Collection|null
     */
    public function get($id = null)
    {
        return $id === null ? EducationForm::all() : EducationForm::find($id);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Faculty;

/**
 * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
 */
class FacultyController extends Controller
{
    /**
     * Returns faculty by id or all faculties if id param is null
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @param int|null $id
     * @return App\Models\Faculty|Illuminate\Database\Eloquent\Collection|null
     */
    public function get($id = null)
    {
        return $id === null ? Faculty::all() : Faculty::find($id);
    }
}

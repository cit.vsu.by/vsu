<?php

namespace App\Http\Controllers;

use App\Models\Honor;

/**
 * Honor collection controller
 *
 * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
 */
class HonorController extends Controller
{
    /**
     * Returns honor by id or all honors if id param is null
     *
     * @author Vitaly Lobatsevich (vitaly.lobatsevich@gmail.com)
     * @param int|null $id
     * @return App\Models\Honor|Illuminate\Database\Eloquent\Collection|null
     */
    public function get($id = null)
    {
        return $id === null ? Honor::all() : Honor::find($id);
    }
}

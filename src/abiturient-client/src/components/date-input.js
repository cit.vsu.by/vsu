import React, {useState, useEffect} from 'react';
import InputMask from 'react-input-mask';

import formatter from '../formatter';
import strings from '../strings';
import {validate} from '../validators';

const DateInput = ({
    id = '',
    date = null,
    setDate = () => {},
    validationError = '',
    setValidationError = () => {},
    isValid = false,
    setIsValid = () => {},
    validators = [],
    help = '',
    afterChange = () => {},
}) => {
    const [value, setValue] = useState('');

    useEffect(() => {
        if (date)
            setValue(`${formatter.addLeadingZeroes(date.getDate())}.` +
                `${formatter.addLeadingZeroes(date.getMonth() + 1)}.` +
                `${formatter.addLeadingZeroes(date.getFullYear(), 4)}`);
    }, [date]);

    const toDate = (value = '01.01.0001') => {
        const parts = value.split('.');
        return new Date(`${parts[2]}-${parts[1]}-${parts[0]}`);
    };

    const isValueValidDate = value => value.match(/^[0-9]{2}.[0-9]{2}.[0-9]{4}$/) && !isNaN(toDate(value));

    const checkAndClear = () => {
        if (!isValueValidDate(value)) {
            setValue('');
            setDate(null);
            return false;
        }
        return true;
    };

    const handleChange = ({target}) => {
        setValidationError('');
        setIsValid(false);

        setValue(target.value);
        if (isValueValidDate(target.value))
            setDate(toDate(target.value));

        afterChange();
    };

    const handleBlur = () => {
        if (checkAndClear())
            validate(date, setValidationError, setIsValid, validators);
    };

    const handleKeyPress = event => {
        if (event.key === 'Enter')
            checkAndClear();
    };

    return (
        <div className="input-group">
            <InputMask 
                mask="99.99.9999" 
                value={value}
                onChange={handleChange}
                onBlur={handleBlur}
            >
                {({inputProps}) =>
                <input
                    {...inputProps}
                    id={id}
                    type="text"
                    className={`form-control 
                        ${isValid ? 'is-valid' : ''} 
                        ${validationError !== '' ? 'is-invalid' : ''}
                        ${help !== '' ? 'form-control-help' : ''}`}
                    placeholder={strings.datePlaceholder}
                    onKeyPress={handleKeyPress}
                />
                }
            </InputMask>
            {help !== '' &&
            <div className="input-group-append">
                <button
                    type="button"
                    className={`btn form-control-btn-help 
                        ${validationError !== '' ? 'form-control-btn-help-invalid' : ''}
                        ${isValid ? 'form-control-btn-help-valid' : ''}`}
                    data-container="body"
                    data-toggle="popover"
                    data-trigger="focus"
                    data-placement="bottom"
                    data-content={help}
                    tabIndex="-1"
                >
                    ?
                </button>
            </div>
            }
        </div>
    );
};

export default DateInput;

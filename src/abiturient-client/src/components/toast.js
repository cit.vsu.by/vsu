import React, {useEffect} from 'react';
import $ from 'jquery';

import './toast.scss';

const Toast = ({
    id = '',
    header = '',
    time = '',
    body = '',
    isShown = false,
    setIsShown = () => {},
}) => {
    useEffect(() => {
        $(document).ready(() => $(`#${id}`).toast('show'));
    }, [id]);

    return (
        <div>
            {isShown &&
            <div className="position-fixed bottom-0 right-0 p-3 position">
                <div
                    id={id}
                    className="toast hide" 
                    role="alert" 
                    aria-live="assertive" 
                    aria-atomic="true"
                    data-delay="2000"
                    data-autohide="false"
                >
                    <div className="toast-header">
                        <strong className="mr-auto">
                            {header}
                        </strong>
                        <small>
                            {time}
                        </small>
                        <button
                            type="button" 
                            className="ml-2 mb-1 close"
                            data-dismiss="toast" 
                            aria-label="Close"
                            onClick={() => setIsShown(false)}
                        >
                            <span aria-hidden="true">
                                &times;
                            </span>
                        </button>
                    </div>
                    <div className="toast-body">
                        {body}
                    </div>
                </div>
            </div>
            }
        </div>
    );
};

export default Toast;

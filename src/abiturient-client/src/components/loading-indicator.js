import React from 'react';
import {selectDownloaded} from '../redux/slices/downloaded-slice';
import {useSelector} from 'react-redux';
import {CSSTransition} from 'react-transition-group';

import LoadingIndicatorSpinner from './loading-indicator-spinner';

const LoadingIndicator = () => {
    const downloaded = useSelector(selectDownloaded);

    return (
        <CSSTransition
            in={Object.values(downloaded).includes(false)}
            timeout={300}
            classNames="alert-animation"
            unmountOnExit
        >
            <LoadingIndicatorSpinner />
        </CSSTransition>
    );
};

export default LoadingIndicator;

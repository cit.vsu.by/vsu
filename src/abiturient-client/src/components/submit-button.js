import React from 'react';

import strings from '../strings';

const SubmitButton = ({isLoading = false, value = null, isDisabled = false}) => {
    return (
        <button
            className="btn btn-primary btn-lg btn-block mt-4"
            type="submit"
            disabled={isLoading || isDisabled}
        >
            {isLoading &&
            <span
                className="spinner-border spinner-border-sm"
                role="status"
                aria-hidden="true"
            />
            }
            {value ? value : strings.save}
        </button>
    );
}

export default SubmitButton;

import React from 'react';

import DateInput from './date-input';
import FormGroup from './form-group';

const Date = ({
    id = '',
    label = '',
    date = null,
    setDate = () => {},
    validationError = '',
    setValidationError = () => {},
    isValid = false,
    setIsValid = () => {},
    validators = [],
    help = '',
    afterChange = () => {},
}) => {
    return (
        <FormGroup
            id={id}
            label={label}
            input={
                <DateInput
                    id={id}
                    date={date}
                    setDate={setDate}
                    validationError={validationError}
                    setValidationError={setValidationError}
                    isValid={isValid}
                    setIsValid={setIsValid}
                    validators={validators}
                    help={help}
                    afterChange={afterChange}
                />
            }
            validationError={validationError}
        />
    );
};

export default Date;

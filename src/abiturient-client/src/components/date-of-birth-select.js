import React from 'react';

import DateInputSelect from './date-input-select';
import strings from '../strings';

const countAge = date => Math.abs((new Date(Date.now() - date.getTime())).getUTCFullYear() - 1970);

export const validate = (value, setValidationError = () => {}) => {
    setValidationError('');

    if (!value) {
        setValidationError(strings.required);
        return false;
    }
    if (countAge(value) < 16) {
        setValidationError(strings.tooYoung);
        return false;
    }
    return true;
};

const DateOfBirthSelect = ({
    date = null,
    setDate = () => {},
    validationError = '',
    setValidationError = () => {}
}) => {
    return (
        <DateInputSelect
            id="dateOfBirth"
            label={strings.dateOfBirth}
            date={date}
            setDate={setDate}
            minYear={new Date().getFullYear() - 65}
            maxYear={new Date().getFullYear()}
            validationError={validationError}
            setValidationError={setValidationError}
        />
    );
};

export default DateOfBirthSelect;

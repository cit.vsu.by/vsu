import React from 'react';
import {Link} from 'react-router-dom';

import strings from '../../strings';
import Layout from '../layout';

const EmailVerified = () => {
    return (
        <Layout>
            <div className="container">
                <div className="row">
                    <div className="col">
                        <div className="card p-4">
                            <div className="card-body">
                                <h3 align="center">
                                    {strings.emailVerified}
                                </h3>
                                <p align="center">
                                    {strings.emailVerifiedText}
                                </p>
                                <div className="row d-flex justify-content-center text-center">
                                    <Link
                                        to="/profile"
                                        className="btn btn-primary btn-lg mt-4"
                                        style={{
                                            width: '350px',
                                        }}
                                    >
                                        {strings.toProfile}
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default EmailVerified;

import React from 'react';

const Modal = ({
    id,
    header,
    body,
    footer,
    isScrollable = true,
    modalDialogClasses = '',
}) => (
    <div
        id={id}
        className="modal fade"
        tabIndex="-1"
    >
        <div className={
            `modal-dialog modal-dialog-centered ${isScrollable ? 'modal-dialog-scrollable' : ''} ${modalDialogClasses}`
        }>
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title">
                        {header}
                    </h5>
                    <button
                        type="button"
                        className="close"
                        data-dismiss="modal"
                        aria-label="Close"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="modal-body">
                    {body}
                 </div>
                <div className="modal-footer">
                    {footer}
                </div>
             </div>
        </div>
    </div>
);

export default Modal;

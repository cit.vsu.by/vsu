import React, {useEffect, useState} from 'react';
import InputMask from 'react-input-mask';

import strings from '../strings';
import {validate} from '../validators';

const PhoneInput = ({
    id = '',
    phone = '',
    setPhone = () => {},
    validationError = '',
    setValidationError = () => {},
    isValid = false,
    setIsValid = () => {},
    validators = [],
    afterChange = () => {},
}) => {
    const [value, setValue] = useState('');

    useEffect(() => {
        if (phone !== '')
            setValue(`+${phone.slice(1, 4)} (${phone.slice(4, 6)}) ${phone.slice(6, 9)}-${phone.slice(9, 11)}-${phone.slice(11, 13)}`);
    }, [phone]);

    const toDbPhone = (value = '+375 (99) 999-99-99') => value.replace(/[- )(]/g, '');

    const isValueValidPhone = value => value.match(
        /^[+]{1}[0-9]{3}[ ]{1}[(]{1}[0-9]{2}[)]{1}[ ]{1}[0-9]{3}[-]{1}[0-9]{2}[-]{1}[0-9]{2}$/
    );

    const checkAndClear = () => {
        if (!isValueValidPhone(value)) {
            setValue('');
            setPhone('');
            return false;
        }
        return true;
    };

    const handleChange = ({target}) => {
        setValidationError('');
        setIsValid(false);

        setValue(target.value);
        if (isValueValidPhone(target.value))
            setPhone(toDbPhone(target.value));

        afterChange();
    };

    const handleBlur = () => {
        if (checkAndClear())
            validate(phone, setValidationError, setIsValid, validators);
    };

    const handleKeyPress = event => {
        if (event.key === 'Enter')
            checkAndClear();
    };

    return (
        <InputMask
            mask="+375 (99) 999-99-99"
            value={value}
            onChange={handleChange}
            onBlur={handleBlur}
        >
            {({inputProps}) =>
            <input
                {...inputProps}
                id={id}
                type="text"
                className={`form-control ${isValid ? 'is-valid' : ''} ${validationError !== '' ? 'is-invalid' : ''}`}
                placeholder={strings.phonePlaceholder}
                onKeyPress={handleKeyPress}
            />
            }
        </InputMask>
    );
};

export default PhoneInput;

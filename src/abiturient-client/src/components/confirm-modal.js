import React from 'react';

import strings from '../strings';
import Modal from './modal';

const ConfirmModal = ({
    id = '',
    content = '',
    onConfirm = () => {},
    onCancel = () => {},
    header = strings.confirmAction,
    confirmText = strings.ok,
    cancelText = strings.cancel,
}) => (
    <Modal
        id={`confirmModal${id}`}
        header={header}
        body={content}
        footer={
            <div>
                <button
                    type="button"
                    className="btn btn-secondary"
                    data-dismiss="modal"
                    onClick={() => onCancel()}
                >
                    {cancelText}
                </button>
                <button
                    type="button"
                    className="btn btn-primary ml-2"
                    data-dismiss="modal"
                    onClick={() => onConfirm()}
                >
                    {confirmText}
                </button>
            </div>
        }
    />
);

export default ConfirmModal;

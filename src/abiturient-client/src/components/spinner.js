import React from 'react';
import strings from '../strings';

const Spinner = props => {
    return (
        <div
            {...props}
            className="spinner-border"
            role="status"
        >
            <span className="sr-only">
                {strings.loading}...
            </span>
        </div>
    );
};

export default Spinner;

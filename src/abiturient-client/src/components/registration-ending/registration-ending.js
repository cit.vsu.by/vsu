import React, {useContext, useState} from 'react';
import {Link, useHistory} from 'react-router-dom';
import {useSelector, useDispatch} from 'react-redux';
import {useBeforeunload} from 'react-beforeunload';

import strings from '../../strings';
import client from '../../client';
import formatter from '../../formatter';
import { AuthContext } from '../../contexts';
import {set, selectRegistrationData, defaultRegistrationData} from '../../redux/slices/registration-data-slice';
import Layout from '../layout';
import Tabs from '../tabs';
import AlertModal from '../alert-modal';

import './registration-ending.scss';

export const checkPassportInfo = ({citizenship}) => !!citizenship;

const RegistrationEnding = () => {
    useBeforeunload(event => event.preventDefault());

    const history = useHistory();

    const dispatch = useDispatch();

    const registrationData = useSelector(selectRegistrationData);

    const authContext = useContext(AuthContext);

    const [isLoading, setIsLoading] = useState(false);

    const handleRegisterClick = () => {
        setIsLoading(true);

        client.post('/api/register', {
                ...registrationData,
                dateOfBirth: formatter.date(registrationData.dateOfBirth),
            }).then(() => {
                dispatch(set(defaultRegistrationData));
                authContext.getMe(() => {
                    setIsLoading(false);
                    history.push('/profile');
                });
            }).catch(error => {
                if (error.response) {
                    switch (error.response.status) {
                        case 422:
                            document.getElementById('showAlertModal').click();
                            break;
                        default:
                            console.log(error.response);
                    }
                }
                setIsLoading(false);
            });
    };

    return (
        <Layout title={strings.titles.registrationEnding}>
            <AlertModal content={strings.incorrectData} />
            <input
                type="hidden"
                id="showAlertModal"
                data-toggle="modal"
                data-target="#alertModal"
            />
            <div className="container">
                <div className="row">
                    <div className="col-md-1 col-lg-2 col-xl-3" />
                    <div className="col-12 col-sm-12 col-md-10 col-lg-8 col-xl-6">
                        <div className="card">
                            <div className="card-body p-5">
                                <h2 align="center">
                                    {strings.registrationEndingTitle}
                                </h2>
                                <p align="justify">
                                    {strings.registrationEndingText1}
                                </p>
                                <p align="justify">
                                    {strings.registrationEndingText2}
                                </p>
                                <p align="justify">
                                    {strings.registrationEndingText3}
                                </p>
                                <div className="row mt-4">
                                    <div className="col d-flex justify-content-center">
                                        <button
                                            type="button"
                                            className="btn btn-primary btn-lg btnRegister"
                                            onClick={handleRegisterClick}
                                            disabled={isLoading}
                                        >
                                            {isLoading &&
                                            <span
                                                className="spinner-border spinner-border-sm"
                                                role="status"
                                                aria-hidden="true"
                                            />
                                            }
                                            {strings.register}
                                        </button>
                                    </div>
                                </div>
                                <div className="row mt-3">
                                    <div className="col d-flex justify-content-center">
                                        <Tabs numberOfTabs={3} currentTab={2} />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <Link to="/registration/passport-info">{strings.back}</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-1 col-lg-2 col-xl-3" />
                </div>
            </div>
        </Layout>
    );
};

export default RegistrationEnding;

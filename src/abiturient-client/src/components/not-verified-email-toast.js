import React, {useContext, useState, useEffect} from 'react';

import strings from '../strings';
import {AuthContext} from '../contexts';
import client from '../client';
import Toast from './toast';

const NotVerifiedEmailToast = () => {
    const authContext = useContext(AuthContext);
    const [isShown, setIsShown] = useState(false);
    const [isEmailResended, setIsEmailResended] = useState(false);

    useEffect(() => {
        setIsShown(!authContext.isEmailVerified);
    }, [authContext.isEmailVerified]);

    const handleResendVerificationEmailClick = event => {
        event.preventDefault();

        client.post('/api/email/verification-notification')
            .then(() => setIsEmailResended(true))
            .catch(error => {
                if(error.response)
                    console.log(error.response);
            });
    };

    return (
        <Toast
            id="notVerifiedEmailToast"
            header={strings.warning}
            time={strings.justNow}
            body={
                isEmailResended ? 
                <span className="text-success">
                    {strings.emailResended}
                </span>
                :
                <div>
                    {strings.notVerifiedEmail}
                    <a
                        href='/api/email/verification-notification'
                        onClick={handleResendVerificationEmailClick}
                    >
                        {strings.resendVerificationEmail}
                    </a>
                </div>
            }
            isShown={isShown}
            setIsShown={setIsShown}
        />
    );
};

export default NotVerifiedEmailToast;

import React from 'react';

import strings from '../../strings';
import Layout from '../layout';
import AbiturientCalendar from './abiturient-calendar';
import MagistrantCalendar from './magistrant-calendar';

const ElectronicQueue = ({isMagistrant = false}) => (
    <Layout title={strings.titles.electronicQueue}>
        <div className="container">
            <div className="row">
                <div className="col">
                    {!isMagistrant ? <AbiturientCalendar /> : <MagistrantCalendar />}
                </div>
            </div>
        </div>
    </Layout>
);

export default ElectronicQueue;

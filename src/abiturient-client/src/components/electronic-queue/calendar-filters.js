import React from 'react';
import {useSelector} from 'react-redux';

import strings from '../../strings';
import {selectConstants} from '../../redux/slices/constants-slice';
import FormGroup from '../form-group';
import Select from '../select';

const CalendarFilters = ({
    facultyId = 1,
    setFacultyId = () => {},
    educationFormId = 1,
    setEducationFormId = () => {},
    specialtyId = 0,
    setSpecialtyId = () => {},
}) => {
    const {faculties, educationForms, specialties} = useSelector(selectConstants);

    const toOptions = constants => constants.map(constant => ({
        value: constant.id,
        label: constant.name,
    }));

    const doSetEducationFormId = value => {
        setSpecialtyId(0);
        setEducationFormId(value);
    };

    const doSetFacultyId = value => {
        doSetEducationFormId(1);
        setFacultyId(value);
    };

    return (
        <div className="mt-3">
            <FormGroup
                id="facultyId"
                label={strings.faculty}
                input={
                    <Select
                        id="facultyId"
                        value={facultyId}
                        setValue={doSetFacultyId}
                        options={toOptions(faculties)}
                        withNotSelected={true}
                    />
                }
            />
            <FormGroup
                id="educationFormId"
                label={strings.educationForm}
                input={
                    <Select
                        id="educationFormId"
                        value={educationFormId}
                        setValue={doSetEducationFormId}
                        options={toOptions(educationForms)}
                        withNotSelected={true}
                    />
                }
            />
            <FormGroup
                id="specialtyId"
                label={strings.specialty}
                input={
                    <Select
                        id="specialtyId"
                        value={specialtyId}
                        setValue={setSpecialtyId}
                        options={toOptions(specialties.filter(specialty => specialty.facultyId === facultyId &&
                            specialty.educationFormId === educationFormId))}
                    />
                }
            />
        </div>
    );
};

export default CalendarFilters;

import React from 'react';

import CommonCurrentTime from './common-current-time';

const MagistrantCurrentTime = () => {
    const getCurrentTime = () => localStorage.getItem('magistrantQueueResultDateTime');
    const getDescription = () => localStorage.getItem('magistrantQueueResultDescription');

    return (
        getCurrentTime() && getDescription() ?
        <CommonCurrentTime
            currentTime={getCurrentTime()}
            description={getDescription()}
        /> :
        <div />
    );
};

export default MagistrantCurrentTime;

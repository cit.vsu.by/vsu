import React, {useEffect} from 'react';
import $ from 'jquery';
import {useCookies} from 'react-cookie';
import moment from 'moment';
import {useSelector} from 'react-redux';
import {Link} from 'react-router-dom';

import strings from '../../strings';
import {selectMagistrantAdmission} from '../../redux/slices/magistrant-admission-slice';
import {selectMagistrantAdmissionDownloaded} from '../../redux/slices/magistrant-admission-downloaded-slice';
import AlertModal from '../alert-modal';
import CommonCalendar from './common-calendar';
import MagistrantCalendarArea from './magistrant-calendar-area';
import MagistrantCurrentTime from './magistrant-current-time';

const MagistrantCalendar = () => {
    const admission = useSelector(selectMagistrantAdmission);
    const isAdmissionDownloaded = useSelector(selectMagistrantAdmissionDownloaded);

    const [cookies, setCookie] = useCookies([
        'isShownHelp',
        'currentHelp',
    ]);

    const isLoading = () => !isAdmissionDownloaded;

    const isNoDesks = () => !admission || !admission.desks || admission.desks.length === 0;

    useEffect(() => {
        if (cookies.isShownHelp === true || cookies.isShownHelp === 'true') {
            setCookie('currentHelp', 2);
            $('#helpModal').modal('toggle');
        }
    }, [cookies.isShownHelp, setCookie]);

    return (
        <div>
            <AlertModal
                id="EndMagistrantQueueRegistration"
                content={
                    <div>
                        {strings.youAreRegisteredOn}:
                        &nbsp;
                        <span className="text-danger">
                            {localStorage.getItem('magistrantQueueResultDateTime') &&
                                moment(localStorage.getItem('magistrantQueueResultDateTime'))
                                    .format('DD.MM.YYYY HH:mm')},
                            &nbsp;
                            {localStorage.getItem('magistrantQueueResultDescription')}
                        </span>
                        <br />
                        <b>
                            {strings.pleaseRememberThis}
                        </b>
                    </div>
                }
            />
            <CommonCalendar
                isLoading={isLoading()}
                admission={admission}
                isNoDesks={isNoDesks()}
                calendarArea={<MagistrantCalendarArea />}
                backLink={
                    <Link to="/" className="btn btn-outline-primary">
                        {strings.backToHome}
                    </Link>
                }
                currentTimeComponent={<MagistrantCurrentTime />}
            />
        </div>
    );
};

export default MagistrantCalendar;

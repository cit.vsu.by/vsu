import React from 'react';
import moment from 'moment';
import Moment from 'react-moment';

import strings from '../../strings';

import './common-calendar-cell.scss';

/*
 * Раскомментировать закомментированные строки для того, чтобы в календаре даты следующего или предыдущего от
 * выбранного месяца стали неактивными.
 */
const CommonCalendarCell = ({
    admission,
    isDayYour = day => true,
    day = moment(),
    month = parseInt(moment().format('M'), 10),
    selectedDay = null,
    setSelectedDay = () => {},
}) => {
    const isYourDay = () => isDayYour(day);

    const getFirstDay = () => moment(month, 'M').startOf('month');

    const getLastDay = () => moment(month, 'M').endOf('month');

    const isOutOfMonth = () => day.isBefore(getFirstDay()) || day.isAfter(getLastDay());

    const isWeekend = () => admission.weekends &&
        admission.weekends.findIndex(weekend => moment(weekend.weekendDate).isSame(day)) !== -1;

    const isOutOfAdmission = () => day.isBefore(moment(admission.startDate)) ||
        day.isAfter(moment(admission.endDate));

    const isPast = () => day.isBefore(moment());

    const isEnabled = () => /*!isOutOfMonth() && */!isWeekend() && !isOutOfAdmission() && !isPast();

    const isSelected = () => selectedDay && day.isSame(selectedDay);

    const handleClick = day => {
        if (isEnabled(day)) {
            if (!selectedDay || !day.isSame(selectedDay))
                setSelectedDay(day);
            else
                setSelectedDay(null);
        }
    };

    return (
        <div
            onClick={() => handleClick(day)}
            className={
                `calendar-cell 
                ${/*isOutOfMonth() || */isOutOfAdmission() || isPast() ? 'disabled' : ''} 
                ${isWeekend() ? 'weekend' : ''} 
                ${isSelected() ? 'selected' : ''} 
                ${isEnabled() ? 'enabled' : ''} 
                ${isYourDay() ? 'your-day' : ''}`
            }
        >
            {isOutOfMonth() &&
            <div className="text-muted month-name">
                {strings.monthNames[day.month()]}
            </div>
            }
            <Moment format="DD">
                {day}
            </Moment>
        </div>
    );
};

export default CommonCalendarCell;

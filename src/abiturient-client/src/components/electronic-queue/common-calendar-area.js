import React, {useState, useEffect, useContext, useCallback} from 'react';
import $ from 'jquery';
import moment from 'moment';
import {useDispatch} from 'react-redux';

import strings from '../../strings';
import client from '../../client';
import {set} from '../../redux/slices/queue-error-modal-content-slice';
import {AppContext, AuthContext} from '../../contexts';
import ConfirmModal from '../confirm-modal';
import Spinner from '../spinner';
import CalendarDatePicker from './calendar-date-picker';

import './common-calendar-area.scss';

moment.updateLocale('en', {
    week: {
        dow : 1,
    },
});

const CommonCalendarArea = ({
    admission,
    confirmUrl,
    confirmData,
    onConfirm = () => {},
    calendarCell = () => {},
    calendarTime = () => {},
    isSignUp = () => false,
}) => {
    const dispatch = useDispatch();

    const appContext = useContext(AppContext);
    const authContext = useContext(AuthContext);

    const getStartDate = useCallback(
        () => moment(admission.startDate),
        [admission.startDate]
    );

    const getEndDate = useCallback(
        () => moment(admission.endDate),
        [admission.endDate]
    );

    const getYear = date => parseInt(date.format('Y'), 10);

    const getStartYear = useCallback(
        () => getYear(getStartDate()),
        [getStartDate]
    );

    const getEndYear = useCallback(
        () => getYear(getEndDate()),
        [getEndDate]
    );

    const getMonth = date => parseInt(date.format('M'), 10);

    const getStartMonth = useCallback(
        () => getMonth(getStartDate()),
        [getStartDate]
    );

    const getEndMonth = useCallback(
        () => getMonth(getEndDate()),
        [getEndDate]
    );

    const [year, setYear] = useState(getStartDate());
    const [month, setMonth] = useState(getStartMonth());
    const [startYear, setStartYear] = useState(getStartYear());
    const [endYear, setEndYear] = useState(getEndYear());
    const [startMonth, setStartMonth] = useState(getStartMonth());
    const [endMonth, setEndMonth] = useState(getEndMonth());

    const getStartDay = useCallback(
        () => moment(month, 'M')
            .startOf('month')
            .startOf('isoWeek'),
        [month]
    );

    const getEndDay = useCallback(
        () => moment(month, 'M')
            .endOf('month')
            .endOf('isoWeek'),
        [month]
    );

    const [startDay, setStartDay] = useState(getStartDay());
    const [endDay, setEndDay] = useState(getEndDate());

    const getDays = useCallback(() => {
        const days = [];
        const day = startDay;
        while (day.isSameOrBefore(endDay)) {
            days.push(moment(day));
            day.add(1, 'days');
        }
        return days;
    }, [startDay, endDay]);

    const [days, setDays] = useState([]);

    useEffect(() => {
        setYear(getStartYear());
        setMonth(getStartMonth());
        setStartYear(getStartYear());
        setEndYear(getEndYear());
        setStartMonth(getStartMonth());
        setEndMonth(getEndMonth());
    }, [admission, getEndMonth, getEndYear, getStartMonth, getStartYear]);

    useEffect(() => {
        setStartDay(getStartDay());
        setEndDay(getEndDay());
    }, [month, getEndDay, getStartDay]);

    useEffect(
        () => setDays(getDays()),
        [startDay, endDay, getDays]
    );

    const daysForWeeks = days => {
        const weeks = [];
        let week = [];
        days.forEach(day => {
            week.push(day);
            if (week.length === 7) {
                weeks.push(week);
                week = [];
            }
        });
        return weeks;
    };

    const [selectedDay, setSelectedDay] = useState(null);
    const [isPosting, setIsPosting] = useState(false);

    const weekOfCalendar = date => date.week() - getStartDay().week();

    const getScheduleItemByDate = date => admission.schedule.find(
        item => moment(item.date).format('YYYY-MM-DD') === date.format('YYYY-MM-DD')
    );

    /* Получает время начала приёма в этот день. Если нет особого графика, берётся стандартная дата */
    const getStartTime = selectedDay => {
        const scheduledStartTime = getScheduleItemByDate(selectedDay);
        return moment(
            `${selectedDay.format('YYYY-MM-DD')} ${scheduledStartTime ?
                scheduledStartTime.startTime : admission.startTime}`,
            'YYYY-MM-DD HH:mm'
        );
    };

    /* Получает время окончания приёма в этот день. Если нет особого графика, берётся стандартная дата */
    const getEndTime = selectedDay => {
        const scheduledEndTime = getScheduleItemByDate(selectedDay);
        return moment(
            `${selectedDay.format('YYYY-MM-DD')} ${scheduledEndTime ?
                scheduledEndTime.endTime : admission.endTime}`,
            'YYYY-MM-DD HH:mm',
        );
    };

    /* Получает возможное время (массив возможных значений) для записи для указанного дня */
    const getTimes = selectedDay => {
        const times = [];
        /* Начинает добавлять с начала приёма в этот день */
        let time = getStartTime(selectedDay);

        /* Пока не конец приёма в этот день, делает: */
        while (time.isSameOrBefore(getEndTime(selectedDay))) {
            /* Сохраняет следующее время, после текущего */
            const nextTime = moment(time);
            nextTime.add(admission.timeForService, 'minutes');

            /*
             * Получает все перерывы, в которые попадает текущее и следующее за ним время.
             *
             * Верхняя граница не входит в перерыв, так как с этого времени уже можно начинать приём.
             *
             * Нижняя граница не входит для следующего времени (но хводит для текущего),
             * так как приём может закончится ровно в момент начала перерыва
             * (начало следующего времени = конец текущего)
            */
            const breaks = admission.breaks.filter(
                item => (time.isSameOrAfter(moment(item.start)) && time.isBefore(moment(item.end))) ||
                    (nextTime.isAfter(moment(item.start)) && nextTime.isBefore(moment(item.end)))
            );

            /*
             * Если такие перерывы найдены, находит крайнюю правую границу и присваивает её значение текущему времени.
             * Это начало приёма после перерыва.
             */
            if (breaks.length > 0)
                time = moment.max(breaks.map(item => moment(item.end)));

            /* Не добавлять последнее время, если его конец приходится на конец рабочего дня. */
            if (!nextTime.isAfter(getEndTime(selectedDay)))
                times.push(moment(time));

            /* Добавляет время приёма к текущему времени, находя следующее время. */
            time.add(admission.timeForService, 'minutes');
        }
        return times;
    };

    const [dateTime, setDateTime] = useState(moment());

    const handleConfirm = () => {
        setIsPosting(true);
        client.post(confirmUrl, {
            ...confirmData,
            dateTime: dateTime.format(),
        }).then(onConfirm).catch(error => {
            if (error.response) {
                switch (error.response.status) {
                    case 422:
                        dispatch(set(error.response.data));
                        $('#alertModalQueueError').modal('toggle');
                        break;
                    default:
                        console.log(error.response);
                }
            }
        }).finally(() => {
            setIsPosting(false);
            authContext.getMe();
            appContext.getAdmission();
            appContext.getMagistrantAdmission();
        });
    };

    return (
        <div>
            {isPosting ?
                <div className="text-center">
                    <Spinner />
                </div> :
                <div className="calendar">
                    <ConfirmModal
                        id="Register"
                        content={`${isSignUp() ? strings.changeTime : strings.signUp}?`}
                        onConfirm={handleConfirm}
                    />
                    <div className="calendar-row mb-3">
                        <h5>
                            {strings.pickDateAndTime}
                        </h5>
                    </div>
                    <div className="calendar-row">
                        <CalendarDatePicker
                            year={year}
                            setYear={setYear}
                            month={month}
                            setMonth={setMonth}
                            startYear={startYear}
                            endYear={endYear}
                            startMonth={startMonth}
                            endMonth={endMonth}
                        />
                    </div>
                    <div className="calendar-header">
                        {strings.daysOfWeek.map((dayOfWeek, i) =>
                            <div key={i + 1} className="calendar-header-col">
                                {dayOfWeek.shortName}
                            </div>
                        )}
                    </div>
                    {daysForWeeks(days).map((week, i) =>
                        <div key={i + 1}>
                            <div className="calendar-row">
                                {week.map((day, i) => calendarCell(i, day, month, selectedDay, setSelectedDay))}
                            </div>
                            {selectedDay && weekOfCalendar(selectedDay) === i &&
                            <div className="calendar-row calendar-row-time">
                                {getTimes(selectedDay).map((time, i) => calendarTime(i + 1, time, setDateTime))}
                            </div>
                            }
                        </div>
                    )}
                </div>
            }
        </div>
    );
};

export default CommonCalendarArea;

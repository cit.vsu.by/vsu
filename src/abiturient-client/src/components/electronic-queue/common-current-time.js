import React from 'react';
import moment from 'moment';
import Moment from 'react-moment';

import strings from '../../strings';

const CommonCurrentTime = ({currentTime = moment().format(), description = '', children}) => {
    return (
        <div className="alert alert-success mt-3" role="alert">
            <h5 className="alert-heading">
                {strings.youRegisteredAt}
                &nbsp;
                <Moment format="DD.MM.YYYY HH:mm">
                    {currentTime}
                </Moment>
            </h5>
            <p>
                {description}
            </p>
            {children}
        </div>
    );
};

export default CommonCurrentTime;

import React from 'react';
import $ from 'jquery';
import moment from 'moment';
import Moment from 'react-moment';

import strings from '../../strings';

import './common-calendar-time.scss';

const CommonCalendarTime = ({
    admission,
    isTimeYour = time => false,
    isTimeFree = time => true,
    time = moment(),
    setDateTime = () => {},
}) => {
    const isYourTime = () => isTimeYour(time);

    const isFree = () => isTimeFree(time);

    const isBreak = () => !!(admission.breaks.find(
        item => time.isSameOrAfter(moment(item.start)) && time.isBefore(item.end)
    ));

    const isEnabled = () => isFree() && !isBreak();

    const handleClick = () => {
        if (isEnabled()) {
            $('#confirmModalRegister').modal('toggle');
            setDateTime(time);
        }
    };

    return (
        <div
            onClick={handleClick}
            className={
                `calendar-time ${
                    isEnabled() ? 'enabled' : ''
                } ${
                    isYourTime() ? 'your-time' : !isFree() || isBreak() ? 'disabled' : ''
                }`
            }
            title={isYourTime() ? strings.isYourTime : !isFree() ? strings.noFree : ''}
        >
            <Moment format="HH:mm">
                {time}
            </Moment>
        </div>
    );
};

export default CommonCalendarTime;

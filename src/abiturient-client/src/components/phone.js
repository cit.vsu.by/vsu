import React from 'react';

import PhoneInput from './phone-input';
import FormGroup from './form-group';

const Phone = ({
    id = '',
    label = '',
    phone = '',
    setPhone = () => {},
    validationError = '',
    setValidationError = () => {},
    isValid = false,
    setIsValid = () => {},
    validators = [],
    afterChange = () => {},
}) => {
    return (
        <FormGroup
            id={id}
            label={label}
            input={
                <PhoneInput
                    id={id}
                    phone={phone}
                    setPhone={setPhone}
                    validationError={validationError}
                    setValidationError={setValidationError}
                    isValid={isValid}
                    setIsValid={setIsValid}
                    validators={validators}
                    afterChange={afterChange}
                />
            }
            validationError={validationError}
        />
    );
};

export default Phone;

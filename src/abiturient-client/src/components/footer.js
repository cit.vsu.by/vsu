import React from 'react';

import strings from '../strings';
import './footer.scss';

const Footer = () => {
    const Ico = ({src}) => {
        return (
            <img
                src={src}
                alt=""
                width="20"
                height="20"
            />
        );
    };

    return (
        <div className="footer mt-4">
            <div className="container">
                <div className="row pt-4">
                    <div className="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 mb-4">
                        <h6>{strings.selectionCommittee}</h6>
                        <Ico src="/img/person.png" /> {strings.executiveSecretary}: {strings.executiveSecretaryName}
                        <br />
                        <Ico src="/img/phone.png" /> {strings.phonesForInformation}: {strings.phonesForInformationNumbers}
                        <br />
                        <Ico src="/img/email.png" /> {strings.email}: {strings.selectionCommitteeEmail}
                        <br />
                        <br />
                        {strings.technicalSupport}: {strings.technicalSupportEmail},<br />{strings.technicalSupportPhone}
                    </div>
                    <div className="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 mb-4">
                        <h6>{strings.copyright}</h6>
                        <Ico src="/img/address.png" /> {strings.address}: {strings.organizationAddress}
                        <br />
                        <Ico src="/img/phone.png" /> {strings.phone}: {strings.organizationPhone}
                        <br />
                        <Ico src="/img/fax.png" /> {strings.fax}: {strings.organizationFax}
                        <br />
                        <Ico src="/img/email.png" /> {strings.email}: {strings.organizationEmail}
                    </div>
                    <div className="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                        <h6>{strings.webResources}</h6>
                        <Ico src="/img/link.png" /> <a href="https://edu.gov.by/">{strings.ministryOfEducation}</a>
                        <br />
                        <Ico src="/img/link.png" /> <a href="https://www.abiturient.by/">{strings.abiturientBy}</a>
                        <br />
                        <Ico src="/img/link.png" /> <a href="https://vsu.by/">{strings.vsuBy}</a>
                        <br />
                        <Ico src="/img/link.png" /> <a href="https://vsu.by/abiturientam/priemnaya-kampaniya.html">
                            {strings.abiturientVsuBy}-{new Date().getFullYear()}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Footer;

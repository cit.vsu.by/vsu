import React, {useContext} from 'react';
import moment from 'moment';
import {Redirect, Route} from 'react-router-dom';

import {AuthContext} from '../contexts';

export const hasRole = (user, roleName) => !!user.roles.find(role => role.name === roleName);

export const hasAnyRole = (user, roleNames) => {
    for (const roleName of roleNames)
        if (!!user.roles.find(role => role.name === roleName))
            return true;
    return false;
};

export const isAdmissionStarted = admission => admission && moment().isSameOrAfter(moment(admission.startRegistration));

const PrivateRoute = ({component: Component, role = true, condition = true, ...rest}) => {
    const {isAuthenticated} = useContext(AuthContext);
    return (
        <Route
            {...rest}
            render={props => !isAuthenticated || !role ?
                <Redirect to={{
                    pathname: '/login',
                    state: {
                        from: props.location,
                    },
                }} /> :
                !condition ?
                <Redirect to={{
                    pathname: '/',
                    state: {
                        from: props.location,
                    },
                }} /> :
                <Component {...props} />
            }
        />
    );
};

export default PrivateRoute;

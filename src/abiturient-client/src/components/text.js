import React from 'react';

import FormGroup from './form-group';
import TextInput from './text-input';

const Text = ({
    id = '',
    label = '',
    type = 'text',
    value = '',
    setValue = () => {},
    validationError = '',
    setValidationError = () => {},
    isValid = false,
    setIsValid = () => {},
    placeholder = '',
    validators = [],
    autoFocus = false,
    maxLength = 100,
    help = '',
    onChange = null,
    onBlur = null,
    afterChange = () => {},
    afterBlur = () => {},
}) => {
    return (
        <FormGroup
            id={id}
            label={label}
            validationError={validationError}
            input = {
                <TextInput
                    id={id}
                    type={type}
                    value={value}
                    setValue={setValue}
                    validationError={validationError}
                    setValidationError={setValidationError}
                    isValid={isValid}
                    setIsValid={setIsValid}
                    placeholder={placeholder}
                    validators={validators}
                    autoFocus={autoFocus}
                    maxLength={maxLength}
                    help={help}
                    onChange={onChange}
                    onBlur={onBlur}
                    afterChange={afterChange}
                    afterBlur={afterBlur}
                />
            }
        />
    );
};

export default Text;

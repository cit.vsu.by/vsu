import React from 'react';

import {validate} from '../validators';

const NumberInput = ({
    id = '',
    value = 0,
    setValue = () => {},
    validationError = '',
    setValidationError = () => {},
    isValid = false,
    setIsValid = () => {},
    validators = [],
    afterBlur = () => {},
    onKeyPress = null,
    afterChange = () => {},
}) => {
    const setNumber = value => {
        const formatted = value.replace(/[^0-9]/g, '');
        setValue(formatted && formatted !== '' ? parseInt(formatted) : 0);
    }

    const handleChange = ({target}) => {
        setValidationError('');
        setIsValid(false);
        setNumber(target.value);
        afterChange();
    };

    const handleBlur = () => {
        if (value !== 0)
            validate(value, setValidationError, setIsValid, validators);

        afterBlur();
    };

    return (
        <input
            type="text"
            id={id}
            value={value}
            onChange={handleChange}
            onBlur={handleBlur}
            className={`form-control ${isValid ? 'is-valid' : ''} ${validationError !== '' ? 'is-invalid' : ''}`}
            onKeyPress={onKeyPress ? onKeyPress : () => {}}
        />
    );
};

export default NumberInput;

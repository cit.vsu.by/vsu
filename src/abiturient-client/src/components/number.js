import React from 'react';

import NumberInput from './number-input';
import FormGroup from "./form-group";

const Number = ({
    id = '',
    label = '',
    value = '',
    setValue = () => {},
    validationError = '',
    setValidationError = () => {},
    isValid = false,
    setIsValid = () => {},
    validators = [],
    afterChange = () => {},
}) => {
    return (
        <FormGroup
            id={id}
            label={label}
            input={
                <NumberInput
                    id={id}
                    value={value}
                    setValue={setValue}
                    validationError={validationError}
                    setValidationError={setValidationError}
                    isValid={isValid}
                    setIsValid={setIsValid}
                    validators={validators}
                    afterChange={afterChange}
                />
            }
            validationError={validationError}
        />
    );
};

export default Number;

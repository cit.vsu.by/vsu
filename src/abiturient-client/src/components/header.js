import React, {useContext} from 'react';
import {useSelector} from 'react-redux';
import {Link, useLocation} from 'react-router-dom';

import strings from '../strings';
import {AuthContext} from '../contexts';
import {selectUser} from '../redux/slices/user-slice';
import {hasRole} from './private-route';
import LoadingIndicator from './loading-indicator';

import './header.scss';

const Header = () => {
    const {pathname} = useLocation();

    const {isAuthenticated} = useContext(AuthContext);

    const user = useSelector(selectUser);

    const getActiveClass = path => pathname === path ? 'active' : '';

    return (
        <div className="sticky-top">
            <nav className="navbar navbar-expand-lg navbar-light bg-light mb-4">
                <Link to="/" className="navbar-brand">
                    <img
                        src="/img/ico50.png"
                        alt=""
                        width="30"
                        height="30"
                        className="d-inline-block align-top mr-1"
                    />
                    {strings.organizationName}
                </Link>
                <button
                    type="button"
                    className="navbar-toggler" 
                    data-toggle="collapse" 
                    data-target="#navbarSupportedContent" 
                    aria-controls="navbarSupportedContent" 
                    aria-expanded="false" 
                    aria-label="Toggle navigation"
                >
                    <span className="navbar-toggler-icon" />
                </button> 

                <div id="navbarSupportedContent" className="collapse navbar-collapse">
                    <ul className="navbar-nav mr-auto">
                        {isAuthenticated && hasRole(user, 'EMPLOYEE') &&
                        <li className={`nav-item ${getActiveClass('/monitoring')}`}>
                            <Link to="/monitoring" className="nav-link">
                                {strings.monitoring}
                            </Link>
                        </li>
                        }
                        {isAuthenticated && hasRole(user, 'EMPLOYEE') &&
                        <li className={`nav-item ${getActiveClass('/abiturients')}`}>
                            <Link to="/abiturients" className="nav-link">
                                {strings.abiturients}
                            </Link>
                        </li>
                        }
                        {isAuthenticated && hasRole(user, 'DEVELOPER') &&
                        <li className={`nav-item ${getActiveClass('/refresher')}`}>
                            <Link to="/refresher" className="nav-link">
                                {strings.refresher}
                            </Link>
                        </li>
                        }
                    </ul>
                    {isAuthenticated &&
                    <Link to="/profile">
                        <svg 
                            xmlns="http://www.w3.org/2000/svg" 
                            width="30"
                            height="30"
                            fill="currentColor"
                            className="bi bi-person-fill"
                            viewBox="0 0 16 16"
                        >
                            <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                        </svg>
                    </Link>
                    }
                </div>
            </nav>
            <LoadingIndicator />
        </div>
    );
};

export default Header;

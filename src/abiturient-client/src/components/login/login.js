import React from 'react';

import strings from '../../strings';
import Layout from '../layout';
import LoginForm from './login-form';

const Login = () => {
    return (
        <Layout
            title={strings.titles.login}
            isWithHeader={false}
            isWithFooter={false}
        >
            <div className="container vertical-center">
                <div className="row">
                    <div className="col">
                        <div className="d-flex justify-content-center text-center">
                            <LoginForm />
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default Login;

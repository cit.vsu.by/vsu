import React, {useState, useContext} from 'react';
import {Link, useHistory} from 'react-router-dom';

import strings from '../../strings';
import client from '../../client';
import {AuthContext} from "../../contexts";
import Alert from '../alert';
import SubmitButton from '../submit-button';

const LoginForm = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isRemember, setIsRemember] = useState(false);
    const [isWrongCredentials, setIsWrongCredentials] = useState(false);
    const [isLoading, setIsLoading] = useState(false);

    const authContext = useContext(AuthContext);
    const history = useHistory();

    const handleSubmit = event => {
        event.preventDefault();

        setIsWrongCredentials(false);
        setIsLoading(true);

        client.get('/sanctum/csrf-cookie').then(response => {
            client
                .post('/login', {
                    email: email,
                    password: password,
                    isRemember: isRemember
                })
                .then(() => authContext.getMe(() => {
                    setIsLoading(false);
                    history.push('/profile');
                }))
                .catch(() => {
                    setIsWrongCredentials(true);
                    setIsLoading(false);
                });
        });
    };

    return (
        <form onSubmit={handleSubmit}>
            <Link to="/">
                <img
                    src="/img/ico50.png"
                    alt="ico50.png"
                    width="50"
                    height="50"
                    className="mb-3"
                />
            </Link>
            <h2>
                {strings.logInWelcome}
            </h2>
            <div className="form-group">
                <label htmlFor="email">
                    {strings.email}
                </label>
                <input
                    type="text"
                    value={email}
                    onChange={({target}) => setEmail(target.value)}
                    id="email"
                    className="form-control"
                    autoFocus
                    required
                />
            </div>
            <div className="form-group">
                <label htmlFor="password">
                    {strings.password}
                </label>
                <input
                    type="password"
                    value={password}
                    onChange={({target}) => setPassword(target.value)}
                    id="password"
                    className="form-control"
                    required
                />
            </div>
            <div className="custom-control custom-checkbox">
                <input
                    type="checkbox"
                    checked={isRemember}
                    onChange={({target}) => setIsRemember(target.checked)}
                    id="isRemember"
                    className="custom-control-input"
                />
                <label htmlFor="isRemember" className="custom-control-label">
                    {strings.isRemember}
                </label>
            </div>
            <Alert
                classes="alert-danger mt-3"
                content={isWrongCredentials ? strings.wrongCredentials : ''}
            />
            <SubmitButton
                isLoading={isLoading}
                value={strings.logIn}
            />
            <p>
                {strings.dontHaveAnAccount} <Link to="/">{strings.toRegister}</Link>
            </p>
            <p className="mt-3 text-muted">
                &copy; {strings.organizationName}, 2021
                {((new Date()).getUTCFullYear() !== 2021 ? '-' + (new Date()).getUTCFullYear() : '')}
            </p>
        </form>
    );
};

export default LoginForm;

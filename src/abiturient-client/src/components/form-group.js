import React from 'react';

import Feedback from './feedback';

const FormGroup = ({
    id = '',
    label = '',
    input = '',
    validationError = '',
    classes = '',
}) => {
    return (
        <div className={`form-group ${classes}`}>
            <label htmlFor={id}>
                {label}
            </label>
            {input}
            <Feedback
                isValid={false}
                feedback={validationError}
                isIndependent={true}
            />
        </div>
    );
};

export default FormGroup;

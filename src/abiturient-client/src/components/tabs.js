import React from 'react';

import './tabs.scss';

const Tabs = ({numberOfTabs, currentTab}) => {
    return (
        <div className="tabs">
            {[...Array(numberOfTabs)].map((item, key) =>
                <div key={key} className={'col' + (currentTab === key ? ' activeTab' : '')}>
                    {key + 1}
                </div>
            )}
        </div>
    );
};

export default Tabs;

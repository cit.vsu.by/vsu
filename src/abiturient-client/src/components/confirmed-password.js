import React from 'react';

import Text from './text';
import strings from '../strings';
import {passwordValidators} from './password';
import {validate as validatePassword} from '../validators';

export const validate = (
    value = '',
    setValidationError = () => {},
    setIsValid = () => {},
    password = '',
    setPasswordValidationError = () => {},
    setIsPasswordValid = () => {},
) => {
    setValidationError('');
    setIsValid(false);

    if (validatePassword(password, setPasswordValidationError, setIsPasswordValid, passwordValidators)) {
        if (value === '') {
            setValidationError(strings.required);
            return false;
        }
        if (value !== password) {
            setValidationError(strings.passwordMismatch);
            return false;
        }
        setIsValid(true);
    }
    return true;
};

const ConfirmedPassword = ({
    id = '',
    label = '',
    value = '',
    setValue = () => {},
    validationError = '',
    setValidationError = () => {},
    isValid = false,
    setIsValid = () => {},
    password = '',
    setPasswordValidationError = () => {},
    setIsPasswordValid = () => {},
}) => {
    const handleBlur = () => {
        if (value !== '')
            validate(
                value,
                setValidationError,
                setIsValid,
                password,
                setPasswordValidationError,
                setIsPasswordValid
            );
    };

    return (
        <Text
            id={id}
            label={label}
            type="password"
            value={value}
            setValue={setValue}
            validationError={validationError}
            setValidationError={setValidationError}
            isValid={isValid}
            setIsValid={setIsValid}
            onBlur={handleBlur}
        />
    );
};

export default ConfirmedPassword;

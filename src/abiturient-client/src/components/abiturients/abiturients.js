import React, {useState, useEffect} from 'react';
import moment from 'moment';

import client from '../../client';
import strings from '../../strings';
import Layout from '../layout';
import Spinner from '../spinner';
import FormGroup from '../form-group';
import Select from '../select';
import SortedColumn from './sorted-column';

const Abiturients = () => {
    const getValue = (value, defaultValue = '') => value ? value : defaultValue;

    const getFullName = abiturient => `${abiturient.lastName} ${abiturient.firstName} ${getValue(abiturient.patronymic)}`;

    const compareById = (a, b) => a.id - b.id;
    const compareByFullNameAsc = (a, b) => getFullName(a).localeCompare(getFullName(b));
    const compareByFullNameDesc = (a, b) => -compareByFullNameAsc(a, b);

    const [abiturients, setAbiturients] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [comparatorName, setComparatorName] = useState('id');
    const [year, setYear] = useState(moment().year());
    const [search, setSearch] = useState('');

    useEffect(() => {
        setIsLoading(true);
        client.get('/api/users')
            .then(({data}) => setAbiturients(data))
            .finally(() => setIsLoading(false));
    }, []);

    const getComparator = () => {
        switch (comparatorName) {
            case 'fullNameAsc':
                return compareByFullNameAsc;
            case 'fullNameDesc':
                return compareByFullNameDesc;
            case 'id':
            default:
                return compareById;
        }
    };

    const filterBySearch = abiturient => getFullName(abiturient).toLowerCase().includes(search.toLowerCase()) ||
        abiturient.email.includes(search) ||
        (abiturient.phone && abiturient.phone.includes(search));

    const getAbiturients = () => abiturients
        .filter(abiturient => moment(abiturient.updatedAt).year() === year)
        .filter(filterBySearch)
        .sort(getComparator());

    const getYearOptions = () => {
        const years = [];
        for (let year = 2020; year <= moment().year(); year++)
            years.push(year);
        return years.map(year => ({
            value: year,
            label: year
        }));
    };

    return (
        <Layout title={strings.titles.abiturients}>
            <div className="container">
                <div className="row mb-3">
                    <div className="col">
                        <div className="card">
                            <div className="card-body">
                                <div className="form-inline">
                                    <FormGroup
                                        id="year"
                                        label={strings.yearOfLastChange}
                                        input={
                                            <Select
                                                id="year"
                                                value={year}
                                                setValue={setYear}
                                                withNotSelected={true}
                                                options={getYearOptions()}
                                                classes="ml-2"
                                            />
                                        }
                                        classes="mr-3"
                                    />
                                    <FormGroup
                                        id="search"
                                        label={strings.search}
                                        input={
                                            <input
                                                type="text"
                                                id="search"
                                                value={search}
                                                onChange={({target}) => setSearch(target.value)}
                                                className="form-control ml-2"
                                            />
                                        }
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <div className="card">
                            <div className="card-body p-5">
                                {isLoading ?
                                <div className="text-center">
                                    <Spinner />
                                </div> :
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">
                                                #
                                            </th>
                                            <th scope="col">
                                                <SortedColumn
                                                    comparatorName="fullName"
                                                    setComparatorName={setComparatorName}
                                                >
                                                    {strings.fullName}
                                                </SortedColumn>
                                            </th>
                                            <th scope="col">
                                                {strings.phone}
                                            </th>
                                            <th scope="col">
                                                {strings.email}
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {getAbiturients().length === 0 ?
                                        <tr>
                                            <td colSpan="4">
                                                <div className="text-center">
                                                    {strings.noData}
                                                </div>
                                            </td>
                                        </tr> :
                                        getAbiturients().map((abiturient, i) =>
                                        <tr key={abiturient.id}>
                                            <th scope="row">
                                                {i + 1}
                                            </th>
                                            <td>
                                                {getFullName(abiturient)}
                                            </td>
                                            <td>
                                                {getValue(abiturient.phone, '-')}
                                            </td>
                                            <td>
                                                {getValue(abiturient.email, '-')}
                                            </td>
                                        </tr>
                                        )}
                                    </tbody>
                                </table>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default Abiturients;

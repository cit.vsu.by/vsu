import React, {useState} from 'react';
import {useSelector} from 'react-redux';
import $ from 'jquery';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';

import strings from '../strings';
import {selectConstants} from '../redux/slices/constants-slice';
import {selectDownloaded} from '../redux/slices/downloaded-slice';
import Spinner from './spinner';
import Modal from './modal';

const zIndexStyles = {
    menu: styles => ({
        ...styles,
        zIndex: 1061,
    }),
};

const SpecialtyPicker = () => {
    const animatedComponents = makeAnimated();
    const {trials, specialties, educationForms} = useSelector(selectConstants);
    const downloaded = useSelector(selectDownloaded);

    const [selectedTrials, setSelectedTrials] = useState([]);
    const [educationFormId, setEducationFormId] = useState(1);

    const getOptions = () => trials.filter(trial => trial.trialTypeId !== 2).map(trial => ({
        value: trial.id,
        label: `${trial.name} (${trial.trialType.shortName})`
    }));

    /* 
     * Эти значения задаются жёстким кодом в соответствии со значениями в базе данных.
     * Если значения перечисленных ниже предметов изменятся в БД, их также необходимо изменить здесь.
     * 
     * Первая переменная - id испытания, которое является общим для группы предметов (например, иностанный язык)
     * Вторая переменная - массив идентификаторов предметов ЦТ, которые являются частными представителями группы
     * (например, английский язык, немецкий язык и т.д.)
     * 
     * Так нужно, потому что для большинства специальностей требуется любой из предметов группы, а вот филфак,
     * как всегда, особенный, требует конкретный предмет.
     * 
     * Ну а вообще, спасибо ELUNI за этот геморой.
     */
    const commonOfficialLanguageGroupId = 1;
    const officialLanguageTrialIds = [2, 3];
    const commonForeignLanguageGroupId = 14;
    const foreignLanguageTrialIds = [8];

    const isTrialsEquals = (selectedTrial, trialId) => {
        let selectedTrialTemp = selectedTrial;
        let trialIdTemp = trialId;
        switch (trialId) {
            case commonOfficialLanguageGroupId:
                if (officialLanguageTrialIds.includes(selectedTrial))
                    selectedTrialTemp = commonOfficialLanguageGroupId;
                break;
            case commonForeignLanguageGroupId:
                if (foreignLanguageTrialIds.includes(selectedTrial))
                    selectedTrialTemp = commonForeignLanguageGroupId;
                break;
            default:
        }
        return selectedTrialTemp === trialIdTemp;
    };

    /*
     * Специальность выводится только в том случае, если выбранные предметы ЦТ полностью содержатся в списке предметов ЦТ,
     * необходимых для поступления на эту специальность, или наоброт: если список ЦТ, необходимый для поступления
     * на эту специальность полностью содержится в списке выбранных ЦТ.
     * 
     * [a, b] => [a, b, c] || [a, b, c] <= [a, b]
     * 
     * В итоге по поведению получается что-то похожее на:
     * 1. Если пользователь ввёл 1-2 ЦТ, ему выводится список специальностей, на которые он может поступить с этими ЦТ,
     * а также подсвечиваются ЦТ, которые ему необходимо досдать.
     * 2. Если пользователь ввёл 3 и больше ЦТ, ему выводятся только те специальности, на которые у него уже есть все
     * необходимые ЦТ.
     * 
     * Вступительные экзамены не влияют на фильтрацию, но при этом выводятся на экран.
     */
    const specialtyFilter = specialty => specialty.educationFormId === educationFormId && (selectedTrials.findIndex(
            selectedTrial => specialty.trials.filter(trial => trial.trialTypeId !== -2).findIndex(
                trial => isTrialsEquals(selectedTrial.value, trial.id)
            ) === -1
        ) === -1 || specialty.trials.filter(
        trial => trial.trialTypeId !== 2).findIndex(trial => selectedTrials.findIndex(
            selectedTrial => isTrialsEquals(selectedTrial.value, trial.id)
        ) === -1
    ) === -1);

    const getSpecialtiesGroupByFaculty = () => specialties.filter(specialtyFilter).reduce((row, specialty) => {
        (row[specialty.faculty.name] = row[specialty.faculty.name] || []).push(specialty);
        return row;
    }, {});

    const handlePickClick = () => $('#specialtyPickerModal').modal('toggle');

    const handleEducationFormIdChange = ({target}) => setEducationFormId(parseInt(target.value, 10));

    return (
        <div>
            <Modal
                id="specialtyPickerModal"
                header={strings.specialtyPicker}
                body={
                    <div>
                        <div className="form-group">
                            <label htmlFor="educationFormId">
                                {strings.educationForm}
                            </label>
                            <select
                                id="educationFormId"
                                value={educationFormId}
                                onChange={handleEducationFormIdChange}
                                className="custom-select"
                            >
                                {educationForms.map(educationForm =>
                                <option key={educationForm.id} value={educationForm.id}>
                                    {educationForm.name}
                                </option>
                                )}
                            </select>
                        </div>
                        {educationFormId !== 3 &&
                        <div>
                            <label>
                                {strings.selectTrials}
                            </label>
                            <Select
                                options={getOptions()}
                                value={selectedTrials}
                                onChange={setSelectedTrials}
                                isMulti
                                closeMenuOnSelect={true}
                                components={animatedComponents}
                                styles={zIndexStyles}
                            />
                        </div>
                        }
                        <div className="mt-3">
                            {Object.keys(getSpecialtiesGroupByFaculty()).length === 0 ?
                            <p align="center">
                                {strings.noSpecialties}
                            </p> :
                            Object.keys(getSpecialtiesGroupByFaculty()).map((facultyName, i) =>
                            <div key={i} className="mb-2">
                                <h4 align="center">
                                    {facultyName}
                                </h4>
                                <div className="list-group list-group-rounded">
                                    {getSpecialtiesGroupByFaculty()[facultyName].map(specialty =>
                                    <div key={specialty.id} className="list-group-item">
                                        <div className="d-flex w-100 justify-content-between">
                                            <h5 className="mb-1">
                                                {specialty.name}
                                            </h5>
                                        </div>
                                        <p className="mb-1">
                                            {specialty.trials.map((trial, i) =>
                                            <span
                                                key={trial.id}
                                                className={
                                                    selectedTrials.findIndex(
                                                        selectedTrial => isTrialsEquals(selectedTrial.value, trial.id)
                                                    ) !== -1 ? 'text-muted' : ''
                                                }
                                            >
                                                {trial.name} ({trial.trialType.shortName})
                                                {i + 1 < specialty.trials.length ? ', ' : ''}
                                            </span>
                                            )}
                                        </p>
                                    </div>
                                    )}
                                </div>
                            </div>
                            )}
                        </div>
                    </div>
                }
                footer={
                    <button
                        type="button"
                        className="btn btn-primary"
                        data-dismiss="modal"
                    >
                        {strings.ok}
                    </button>
                }
                isScrollable={false}
                modalDialogClasses="modal-xl"
            />
            <h5>
                {strings.specialtyPicker}
            </h5>
            {!downloaded.trials || !downloaded.specialties || !downloaded.educationForms ?
            <div className="d-flex justify-content-center">
                <Spinner />
            </div> :
            <button
                type="button"
                className="btn btn-primary btn-lg btn-block mt-3"
                onClick={handlePickClick}
            >
                {strings.pick}
            </button>
            }
        </div>
    );
};

export default SpecialtyPicker;

import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';
import {useSelector, useDispatch} from 'react-redux';

import strings from '../../strings';
import {required, validate as validateValue} from '../../validators';
import {setValue, selectRegistrationData} from '../../redux/slices/registration-data-slice';
import {selectConstants} from '../../redux/slices/constants-slice';
import Tabs from "../tabs";
import Radio from '../radio';
import Name from '../name';
import DateOfBirth, {dateOfBirthValidators} from '../date-of-birth';

const firstNameValidators = [required];
const lastNameValidators = [required];

const RegistrationPersonalInfoForm = () => {
    const registrationData = useSelector(selectRegistrationData);
    const constants = useSelector(selectConstants);
    const dispatch = useDispatch();
    const history = useHistory();

    const [firstNameError, setFirstNameError] = useState('');
    const [lastNameError, setLastNameError] = useState('');
    const [dateOfBirthError, setDateOfBirthError] = useState('');
    const [isFirstNameValid, setIsFirstNameValid] = useState(false);
    const [isLastNameValid, setIsLastNameValid] = useState(false);
    const [isPatronymicValid, setIsPatronymicValid] = useState(false);
    const [isDateOfBirthValid, setIsDateOfBirthValid] = useState(false);

    const validate = () => {
        const isFirstNameValid = validateValue(
            registrationData.firstName,
            setFirstNameError,
            setIsFirstNameValid,
            firstNameValidators
        );
        const isLastNameValid = validateValue(
            registrationData.lastName,
            setLastNameError,
            setIsLastNameValid,
            lastNameValidators
        );
        const isDateOfBirthValid = validateValue(
            registrationData.dateOfBirth,
            setDateOfBirthError,
            setIsDateOfBirthValid,
            dateOfBirthValidators
        );

        return isLastNameValid && isFirstNameValid && isDateOfBirthValid;
    };

    const handleSubmit = event => {
        event.preventDefault();

        if (validate())
            history.push('/registration/passport-info');
    };

    return (
        <form onSubmit={handleSubmit}>
            <Name
                id="lastName"
                label={strings.lastName + '*'}
                value={registrationData.lastName}
                setValue={value => dispatch(setValue({key: 'lastName', value: value}))}
                validationError={lastNameError}
                setValidationError={setLastNameError}
                isValid={isLastNameValid}
                setIsValid={setIsLastNameValid}
                validators={lastNameValidators}
                autoFocus={true}
                help={strings.lastNameHelp}
            />
            <Name
                id="firstName"
                label={strings.firstName + '*'}
                value={registrationData.firstName}
                setValue={value => dispatch(setValue({key: 'firstName', value: value}))}
                validationError={firstNameError}
                setValidationError={setFirstNameError}
                isValid={isFirstNameValid}
                setIsValid={setIsFirstNameValid}
                validators={firstNameValidators}
                help={strings.firstNameHelp}
            />
            <Name
                id="patronymic"
                label={
                    <div>
                        {strings.patronymic}
                        <small className="text-muted"> - {strings.ifAvailable}</small>
                    </div>
                }
                value={registrationData.patronymic}
                setValue={value => dispatch(setValue({key: 'patronymic', value: value}))}
                isValid={isPatronymicValid}
                setIsValid={setIsPatronymicValid}
                help={strings.patronymicHelp}
            />
            <Radio
                id="gender"
                name="gender"
                value={registrationData.genderId}
                setValue={value => dispatch(setValue({key: 'genderId', value: parseInt(value, 10)}))}
                values={constants.genders}
                isInline={false}
            />
            <DateOfBirth
                date={registrationData.dateOfBirth}
                setDate={date => dispatch(setValue({key: 'dateOfBirth', value: date}))}
                validationError={dateOfBirthError}
                setValidationError={setDateOfBirthError}
                isValid={isDateOfBirthValid}
                setIsValid={setIsDateOfBirthValid}
            />
            <input
                type="submit"
                value={strings.next}
                className="btn btn-primary btn-block btn-lg mb-3 mt-4"
            />
            <Tabs numberOfTabs={3} currentTab={0} />
        </form>
    );
}

export default RegistrationPersonalInfoForm;

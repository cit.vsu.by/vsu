import React, {useState, useEffect} from 'react';
import {useSelector} from 'react-redux';

import strings from '../../strings';
import client from '../../client';
import {selectConstants} from '../../redux/slices/constants-slice';

const Sport = () => {
    const constants = useSelector(selectConstants);

    const [sportTypeId, setSportTypeId] = useState(0);
    const [sportCategoryId, setSportCategoryId] = useState(0);
    const [sportTypesSportCategories, setSportTypesSportCategories] = useState([]);
    const [isDownloading, setIsDownloading] = useState(false);
    const [isSaving, setIsSaving] = useState(false);

    useEffect(() => {
        setIsDownloading(true);
        client.get('/api/users-sport-types-sport-categories')
            .then(({data}) => setSportTypesSportCategories(data.map(element => ({...element, isDeleting: false}))))
            .finally(() => setIsDownloading(false));
    }, []);

    const setSportTypeSportCategory = sportTypeSportCategory => {
        const mutableSportTypesSportCategories = [...sportTypesSportCategories];
        const index = mutableSportTypesSportCategories.findIndex(
            sportTypeSportCategory => sportTypeSportCategory.sportTypeId === sportTypeId
        );
        if (index !== -1)
            mutableSportTypesSportCategories[index] = sportTypeSportCategory;
        else
            mutableSportTypesSportCategories.push(sportTypeSportCategory);
        setSportTypesSportCategories(mutableSportTypesSportCategories);
    };

    const handleAddClick = () => {
        if (sportTypeId !== 0 && sportCategoryId !== 0) {
            setIsSaving(true);

            const sportTypeSportCategory = {
                sportTypeId: sportTypeId,
                sportCategoryId: sportCategoryId,
            };
            client.post('/api/users-sport-types-sport-categories', sportTypeSportCategory)
                .then(() => setSportTypeSportCategory(sportTypeSportCategory))
                .finally(() => setIsSaving(false));
        }
    };

    const deleteBySportTypeId = sportTypeId => {
        client.delete(`/api/users-sport-types-sport-categories/${sportTypeId}`).then(() => setSportTypesSportCategories(
            sportTypesSportCategories.filter(sportTypeSportCategory => sportTypeSportCategory.sportTypeId !== sportTypeId)
        ));
    };

    return (
        <div>
            <h5>
                {strings.sport}
            </h5>
            <p className="text-muted small">
                {strings.selectSomeSport}
            </p>
            {!isDownloading ?
            <div>
                <div className="form-group">
                    <label htmlFor="sportType">
                        {strings.sportType}
                    </label>
                    <select
                        id="sportType"
                        className="custom-select"
                        value={sportTypeId}
                        onChange={({target}) => setSportTypeId(parseInt(target.value, 10))}
                    >
                        <option value="0">
                            {strings.notSelected}
                        </option>
                        {constants.sportTypes.map(sportType =>
                        <option key={sportType.id} value={sportType.id}>
                            {sportType.name}
                        </option>
                        )}
                    </select>
                </div>
                <div className="form-group">
                    <select
                        id="sportCategory"
                        className="custom-select"
                        value={sportCategoryId}
                        onChange={({target}) => setSportCategoryId(parseInt(target.value, 10))}
                    >
                        <option value="0">
                            {strings.notSelected}
                        </option>
                        {constants.sportCategories.map(sportCategory =>
                        <option key={sportCategory.id} value={sportCategory.id}>
                            {sportCategory.name}
                        </option>
                        )}
                    </select>
                </div>
                <button
                    type="button"
                    className="btn btn-outline-secondary btn-block mb-3 btn-sport"
                    onClick={handleAddClick}
                    disabled={isSaving}
                >
                    {isSaving &&
                    <span 
                        className="spinner-border spinner-border-sm" 
                        role="status" 
                        aria-hidden="true" 
                    />
                    }
                    {strings.add}
                </button>
                <ul className="list-group list-group-rounded">
                {sportTypesSportCategories.map(sportTypeSportCategory =>
                <li key={sportTypeSportCategory.sportTypeId} className="list-group-item">
                    <div className="d-flex w-100 justify-content-between">
                        <h5 className="mb-1">
                            {constants.sportTypes.find(sportType => sportType.id === sportTypeSportCategory.sportTypeId).name}
                        </h5>
                        <small>
                            <svg
                                xmlns="http://www.w3.org/2000/svg" 
                                width="16" 
                                height="16" 
                                fill="currentColor"
                                className="bi bi-x delete"
                                viewBox="0 0 16 16"
                                onClick={() => deleteBySportTypeId(sportTypeSportCategory.sportTypeId)}
                            >
                                <path 
                                    d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647
                                    2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"
                                />
                            </svg>
                        </small>
                    </div>
                    <p className="mb-1">
                        {constants.sportCategories.find(sportCategory => sportCategory.id === sportTypeSportCategory.sportCategoryId).name}
                    </p>
                </li>
                )}
                </ul>
            </div>
            :
            <div className="text-center">
                <div className="spinner-border text-dark" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
            }
        </div>
    );
};

export default Sport;

import React, {useContext, useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import strings from '../../strings';
import client from '../../client';
import formatter from '../../formatter';
import {AuthContext} from '../../contexts';
import {selectUser, setValue} from '../../redux/slices/user-slice';
import {selectConstants} from '../../redux/slices/constants-slice';
import {selectDownloaded} from '../../redux/slices/downloaded-slice';
import Alert from '../alert';
import AutocompleteInput from '../autocomplete-input';
import Date from '../date';
import Text from '../text';
import SubmitButton from '../submit-button';
import HelpButton from '../help-button';
import Spinner from '../spinner';

const EducationForm = ({setIsChanged = () => {}}) => {
    const dispatch = useDispatch();
    const user = useSelector(selectUser);
    const constants = useSelector(selectConstants);
    const downloaded = useSelector(selectDownloaded);
    const authContext = useContext(AuthContext);

    const [educationalInstitutionTypeId, setEducationalInstitutionTypeId] = useState(0);
    const [educationalInstitution, setEducationalInstitution] = useState('');
    const [isHonor, setIsHonor] = useState(false);
    const [success, setSuccess] = useState('');
    const [graduationDateError, setGraduationDateError] = useState('');
    const [educationalDocumentNumberError, setEducationalDocumentNumberError] = useState('');
    const [isEducationalInstitutionValid, setIsEducationalInstitutionValid] = useState(false);
    const [isGraduationDateValid, setIsGraduationDateValid] = useState(false);
    const [isEducationalDocumentNumberValid, setIsEducationalDocumentNumberValid] = useState(false);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        if (
            user.educationalInstitutionId &&
            user.educationalInstitutionId !== 0 &&
            downloaded.educationalInstitutions
        ) {
            const educationalInstitution = constants.educationalInstitutions.find(
                educationalInstitution => educationalInstitution.id === user.educationalInstitutionId
            );
            setEducationalInstitution(educationalInstitution.name);
            setEducationalInstitutionTypeId(educationalInstitution.educationalInstitutionTypeId);
        }
    }, [user, downloaded, constants]);

    useEffect(() => {
        if (user.honorId !== 0)
            setIsHonor(true);
    }, [user.honorId]);

    const clearValidation = () => {
        setGraduationDateError('');
        setEducationalDocumentNumberError('');

        setIsGraduationDateValid(false);
        setIsEducationalInstitutionValid(false);
        setIsEducationalDocumentNumberValid(false);
    }

    const handleSubmit = event => {
        event.preventDefault();

        setIsLoading(true);
        client.post('/api/users/education', {
            educationalDocumentTypeId: formatter.foreignKey(user.educationalDocumentTypeId),
            educationalInstitutionId: formatter.foreignKey(user.educationalInstitutionId),
            graduationDate: formatter.date(user.graduationDate),
            educationalDocumentNumber: formatter.str(user.educationalDocumentNumber),
            isRuralEducationalInstitution: user.isRuralEducationalInstitution,
            isPedagogicalClass: user.isPedagogicalClass,
            foreignLanguageId: formatter.foreignKey(user.foreignLanguageId),
            honorId: isHonor ? getHonorId() : null,
            isPreUniversityTraining: user.isPreUniversityTraining,
        }).then(() => {
            clearValidation();
            authContext.getMe();
            setIsChanged(false);
            setSuccess(strings.done);
            setTimeout(() => setSuccess(''), 3000);
        }).catch(error => {
            if (error.response)
                console.log(error.response);
        }).finally(() => setIsLoading(false));
    };

    const setInstitutionType = value => {
        setEducationalInstitutionTypeId(value);
        setEducationalInstitution('');
        setIsEducationalInstitutionValid(false);
        dispatch(setValue({key: 'educationalInstitutionId', value: null}));

        if (value === 0)
            setIsHonor(false);

        setIsChanged(true);
    };

    const setDocumentType = value => {
        dispatch(setValue({key: 'educationalDocumentTypeId', value: value}));
        setInstitutionType(0);

        setIsChanged(true);
    };

    const handleEducationalDocumentTypeChange = ({target}) => setDocumentType(parseInt(target.value, 10));
    const handleEducationalInstitutionTypeChange = ({target}) => setInstitutionType(parseInt(target.value, 10));
    const handleIsRuralChange = ({target}) => {
        dispatch(setValue({
            key: 'isRuralEducationalInstitution',
            value: target.checked
        }));

        setIsChanged(true);
    };
    const handleIsHonorChange = ({target}) => {
        setIsHonor(target.checked);

        setIsChanged(true);
    };
    const handleIsPedagogicalClassChange = ({target}) => {
        dispatch(setValue({
            key: 'isPedagogicalClass',
            value: target.checked
        }));

        setIsChanged(true);
    };
    const handleIsPreUniversityTrainingChange = ({target}) => {
        dispatch(setValue({
            key: 'isPreUniversityTraining',
            value: target.checked
        }));

        setIsChanged(true);
    };
    const handleForeignLanguageChange = ({target}) => {
        dispatch(setValue({
            key: 'foreignLanguageId',
            value: parseInt(target.value)
        }));

        setIsChanged(true);
    };

    const getEducationalInstitutionTypes = () => {
        switch (user.educationalDocumentTypeId) {
            case 1: return constants.educationalInstitutionTypes.filter(item => [1, 2, 3].includes(item.id));
            case 2:
            case 3: return constants.educationalInstitutionTypes.filter(item => [4, 5, 6, 7, 8].includes(item.id));
            default: return [];
        }
    };

    const getHonorId = () => {
        switch (educationalInstitutionTypeId) {
            case 1:
            case 2:
            case 3: return 3;
            case 4:
            case 5:
            case 7: return 1;
            case 6: return 2;
            default: return 0;
        }
    };

    const getHonor = () => constants.honors.find(honor => honor.id === getHonorId());

    return (
        <div className="card">
            <div className="card-body">
                <form onSubmit={handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="educationalDocumentType">
                            {strings.educationalDocumentType}
                            <HelpButton content={strings.educationalDocumentTypeHelp} />
                        </label>
                        {!downloaded.educationalDocumentTypes ?
                        <div className="text-center">
                            <Spinner />
                        </div> :
                        <select
                            id="educationalDocumentType"
                            value={user.educationalDocumentTypeId}
                            onChange={handleEducationalDocumentTypeChange}
                            className="custom-select"
                        >
                            <option value="0">
                                {strings.notSelected}
                            </option>
                            {constants.educationalDocumentTypes.map(type =>
                            <option key={type.id} value={type.id}>
                                {type.name}
                            </option>
                            )}
                        </select>
                        }
                    </div>
                    <div className="form-group">
                        <label htmlFor="educationalInstitutionType">
                            {strings.educationalInstitutionType}
                        </label>
                        {!downloaded.educationalInstitutionTypes ?
                        <div className="text-center">
                            <Spinner />
                        </div> :
                        <select
                            id="educationalInstitutionType"
                            value={educationalInstitutionTypeId}
                            onChange={handleEducationalInstitutionTypeChange}
                            className="custom-select"
                        >
                            <option value="0">
                                {strings.notSelected}
                            </option>
                            {getEducationalInstitutionTypes().map(educationalInstitutionType =>
                            <option key={educationalInstitutionType.id} value={educationalInstitutionType.id}>
                                {educationalInstitutionType.name}
                            </option>
                            )}
                        </select>
                        }
                    </div>
                    <div className="form-group">
                        <label htmlFor="educationalInstitution">
                            {strings.educationalInstitution}
                            <HelpButton content={strings.educationalInstitutionHelp} />
                        </label>
                        {!downloaded.educationalInstitutions ?
                        <div className="text-center">
                            <Spinner />
                        </div> :
                        <AutocompleteInput
                            id="educationalInstitution"
                            items={constants.educationalInstitutions
                                .filter(item => item.educationalInstitutionTypeId === educationalInstitutionTypeId)}
                            itemId={user.educationalInstitutionId}
                            setItemId={id => dispatch(setValue({key: 'educationalInstitutionId', value: id}))}
                            value={educationalInstitution}
                            setValue={setEducationalInstitution}
                            isValid={isEducationalInstitutionValid}
                            setIsValid={setIsEducationalInstitutionValid}
                            afterChange={() => setIsChanged(true)}
                        />
                        }
                    </div>
                    <div className="form-group">
                        <div className="custom-control custom-checkbox">
                            <input
                                id="isRural"
                                type="checkbox"
                                checked={user.isRuralEducationalInstitution}
                                onChange={handleIsRuralChange}
                                className="custom-control-input"
                            />
                            <label htmlFor="isRural" className="custom-control-label">
                                {strings.isRural}
                            </label>
                        </div>
                    </div>
                    <Text
                        id="educationalDocumentNumber"
                        label={strings.educationalDocumentNumber}
                        value={user.educationalDocumentNumber}
                        setValue={value => dispatch(setValue({
                            key: 'educationalDocumentNumber', 
                            value: value.toUpperCase().replace(/[^A-Z0-9-]/g, formatter.translate)
                        }))}
                        validationError={educationalDocumentNumberError}
                        setValidationError={setEducationalDocumentNumberError}
                        isValid={isEducationalDocumentNumberValid}
                        setIsValid={setIsEducationalDocumentNumberValid}
                        afterChange={() => setIsChanged(true)}
                    />
                    <Date
                        id="date"
                        label={strings.graduationDate}
                        date={user.graduationDate}
                        setDate={date => dispatch(setValue({key: 'graduationDate', value: date}))}
                        validationError={graduationDateError}
                        setValidationError={setGraduationDateError}
                        isValid={isGraduationDateValid}
                        setIsValid={setIsGraduationDateValid}
                        afterChange={() => setIsChanged(true)}
                    />
                    <hr />
                    {getHonor() &&
                    <div className="form-group">
                        <div className="custom-control custom-checkbox">
                            <input
                                id="isHonor"
                                type="checkbox"
                                checked={isHonor}
                                onChange={handleIsHonorChange}
                                className="custom-control-input"
                            />
                            <label htmlFor="isHonor" className="custom-control-label">
                                {getHonor().name}
                            </label>
                        </div>
                    </div>
                    }
                    <div className="form-group">
                        <div className="custom-control custom-checkbox">
                            <input
                                id="isPedagogicalClass"
                                type="checkbox"
                                checked={user.isPedagogicalClass}
                                onChange={handleIsPedagogicalClassChange}
                                className="custom-control-input"
                            />
                            <label htmlFor="isPedagogicalClass" className="custom-control-label">
                                {strings.isPedagogicalClass}
                                <HelpButton content={strings.isPedagogicalClassHelp} />
                            </label>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="custom-control custom-checkbox">
                            <input
                                id="isPreUniversityTraining"
                                type="checkbox"
                                checked={user.isPreUniversityTraining}
                                onChange={handleIsPreUniversityTrainingChange}
                                className="custom-control-input"
                            />
                            <label htmlFor="isPreUniversityTraining" className="custom-control-label">
                                {strings.isPreUniversityTraining}
                            </label>
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="foreignLanguage">
                            {strings.foreignLanguage}
                            <HelpButton content={strings.foreignLanguageHelp} />
                        </label>
                        {!downloaded.foreignLanguages ?
                        <div className="text-center">
                            <Spinner />
                        </div> :
                        <select
                            id="foreignLanguage"
                            value={user.foreignLanguageId}
                            onChange={handleForeignLanguageChange}
                            className="custom-select"
                        >
                            <option value="0">
                                {strings.notSelected}
                            </option>
                            {constants.foreignLanguages.map(foreignLanguage =>
                            <option key={foreignLanguage.id} value={foreignLanguage.id}>
                                {foreignLanguage.name}
                            </option>
                            )}
                        </select>
                        }
                    </div>
                    <Alert
                        classes="alert-success"
                        content={success}
                    />
                    <SubmitButton isLoading={isLoading} />
                </form>
            </div>
        </div>
    );
};

export default EducationForm;

import React, {useState, useContext} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import strings from '../../strings';
import client from '../../client';
import formatter from '../../formatter';
import {required, validate as validateValue} from '../../validators';
import {AuthContext} from '../../contexts';
import {selectConstants} from '../../redux/slices/constants-slice';
import {selectUser, setValue} from '../../redux/slices/user-slice';
import Name from '../name';
import Radio from '../radio';
import Alert from '../alert';
import DateOfBirth, {dateOfBirthValidators} from '../date-of-birth';
import SubmitButton from '../submit-button';
import Phone from '../phone';

const firstNameValidators = [required];
const lastNameValidators = [required];

const BasicForm = ({setIsChanged = () => {}}) => {
    const dispatch = useDispatch();
    const constants = useSelector(selectConstants);
    const user = useSelector(selectUser);
    const authContext = useContext(AuthContext);

    const [firstNameError, setFirstNameError] = useState('');
    const [lastNameError, setLastNameError] = useState('');
    const [dateOfBirthError, setDateOfBirthError] = useState('');
    const [isFirstNameValid, setIsFirstNameValid] = useState(false);
    const [isLastNameValid, setIsLastNameValid] = useState(false);
    const [isPatronymicValid, setIsPatronymicValid] = useState(false);
    const [isFirstNameByValid, setIsFirstNameByValid] = useState(false);
    const [isLastNameByValid, setIsLastNameByValid] = useState(false);
    const [isPatronymicByValid, setIsPatronymicByValid] = useState(false);
    const [isFirstNameEnValid, setIsFirstNameEnValid] = useState(false);
    const [isLastNameEnValid, setIsLastNameEnValid] = useState(false);
    const [isDateOfBirthValid, setIsDateOfBirthValid] = useState(false);
    const [isPhoneValid, setIsPhoneValid] = useState(false);
    const [success, setSuccess] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    const validate = () => {
        const isFirstNameValid = validateValue(
            user.firstName,
            setFirstNameError,
            setIsFirstNameValid,
            firstNameValidators
        );
        const isLastNameValid = validateValue(
            user.lastName,
            setLastNameError,
            setIsLastNameValid,
            lastNameValidators
        );
        const isDateOfBirthValid = validateValue(
            user.dateOfBirth,
            setDateOfBirthError,
            setIsDateOfBirthValid,
            dateOfBirthValidators
        );

        return isFirstNameValid && isLastNameValid && isDateOfBirthValid;
    };

    const clearValidation = () => {
        setFirstNameError('');
        setLastNameError('');
        setDateOfBirthError('');

        setIsFirstNameValid(false);
        setIsLastNameValid(false);
        setIsPatronymicValid(false);
        setIsFirstNameByValid(false);
        setIsLastNameByValid(false);
        setIsPatronymicByValid(false);
        setIsFirstNameEnValid(false);
        setIsLastNameEnValid(false);
        setIsDateOfBirthValid(false);
        setIsPhoneValid(false);
    };

    const handleSubmit = event => {
        event.preventDefault();

        if (validate()) {
            setIsLoading(true);
            client.post('/api/users/basic-info', {
                firstName: formatter.str(user.firstName),
                lastName: formatter.str(user.lastName),
                patronymic: formatter.str(user.patronymic),
                firstNameBy: formatter.str(user.firstNameBy),
                lastNameBy: formatter.str(user.lastNameBy),
                patronymicBy: formatter.str(user.patronymicBy),
                firstNameEn: formatter.str(user.firstNameEn),
                lastNameEn: formatter.str(user.lastNameEn),
                genderId: formatter.foreignKey(user.genderId),
                dateOfBirth: formatter.date(user.dateOfBirth),
                phone: user.phone,
            }).then(() => {
                clearValidation();
                authContext.getMe();
                setIsChanged(false);
                setSuccess(strings.done);
                setTimeout(() => setSuccess(''), 3000);
            }).catch(error => {
                if (error.response)
                    console.log(error.response);
            }).finally(() => setIsLoading(false));
        }
    };

    return (
        <div className="card">
            <div className="card-body">
                <form onSubmit={handleSubmit}>
                    <Name
                        id="lastName"
                        label={strings.lastName + '*'}
                        value={user.lastName}
                        setValue={value => dispatch(setValue({key: 'lastName', value: value}))}
                        validationError={lastNameError}
                        setValidationError={setLastNameError}
                        isValid={isLastNameValid}
                        setIsValid={setIsLastNameValid}
                        validators={lastNameValidators}
                        help={strings.lastNameHelp}
                        afterChange={() => setIsChanged(true)}
                    />
                    <Name
                        id="firstName"
                        label={strings.firstName + '*'}
                        value={user.firstName}
                        setValue={value => dispatch(setValue({key: 'firstName', value: value}))}
                        validationError={firstNameError}
                        setValidationError={setFirstNameError}
                        isValid={isFirstNameValid}
                        setIsValid={setIsFirstNameValid}
                        validators={firstNameValidators}
                        help={strings.firstNameHelp}
                        afterChange={() => setIsChanged(true)}
                    />
                    <Name
                        id="patronymic"
                        label={
                            <div>
                                {strings.patronymic}
                                <small className="text-muted"> - {strings.ifAvailable}</small>
                            </div>
                        }
                        value={user.patronymic}
                        setValue={value => dispatch(setValue({key: 'patronymic', value: value}))}
                        isValid={isPatronymicValid}
                        setIsValid={setIsPatronymicValid}
                        help={strings.patronymicHelp}
                        afterChange={() => setIsChanged(true)}
                    />
                    <hr />
                    <Phone
                        id="phone"
                        label={strings.phone}
                        phone={user.phone}
                        setPhone={value => dispatch(setValue({key: 'phone', value: value}))}
                        isValid={isPhoneValid}
                        setIsValid={setIsPhoneValid}
                        afterChange={() => setIsChanged(true)}
                    />
                    <hr />
                    <Name
                        id="lastNameEn"
                        label={strings.lastNameEn}
                        value={user.lastNameEn}
                        setValue={value => dispatch(setValue({key: 'lastNameEn', value: value}))}
                        isValid={isLastNameEnValid}
                        setIsValid={setIsLastNameEnValid}
                        lang="en"
                        help={strings.lastNameEnHelp}
                        afterChange={() => setIsChanged(true)}
                    />
                    <Name
                        id="firstNameEn"
                        label={strings.firstNameEn}
                        value={user.firstNameEn}
                        setValue={value => dispatch(setValue({key: 'firstNameEn', value: value}))}
                        isValid={isFirstNameEnValid}
                        setIsValid={setIsFirstNameEnValid}
                        lang="en"
                        help={strings.firstNameEnHelp}
                        afterChange={() => setIsChanged(true)}
                    />
                    <hr />
                    <Name
                        id="lastNameBy"
                        label={strings.lastNameBy}
                        value={user.lastNameBy}
                        setValue={value => dispatch(setValue({key: 'lastNameBy', value: value}))}
                        isValid={isLastNameByValid}
                        setIsValid={setIsLastNameByValid}
                        lang="by"
                        help={strings.lastNameByHelp}
                        afterChange={() => setIsChanged(true)}
                    />
                    <Name
                        id="firstNameBy"
                        label={strings.firstNameBy}
                        value={user.firstNameBy}
                        setValue={value => dispatch(setValue({key: 'firstNameBy', value: value}))}
                        isValid={isFirstNameByValid}
                        setIsValid={setIsFirstNameByValid}
                        lang="by"
                        help={strings.firstNameByHelp}
                        afterChange={() => setIsChanged(true)}
                    />
                    <Name
                        id="patronymicBy"
                        label={
                            <div>
                                {strings.patronymicBy}
                                <small className="text-muted"> - {strings.ifAvailable}</small>
                            </div>
                        }
                        value={user.patronymicBy}
                        setValue={value => dispatch(setValue({key: 'patronymicBy', value: value}))}
                        isValid={isPatronymicByValid}
                        setIsValid={setIsPatronymicByValid}
                        lang="by"
                        help={strings.patronymicByHelp}
                        afterChange={() => setIsChanged(true)}
                    />
                    <hr />
                    <Radio
                        id="gender"
                        name="gender"
                        value={user.genderId}
                        setValue={value => dispatch(setValue({key: 'genderId', value: parseInt(value, 10)}))}
                        values={constants.genders}
                        isInline={false}
                        afterChange={() => setIsChanged(true)}
                    />
                    <DateOfBirth
                        date={user.dateOfBirth}
                        setDate={date => dispatch(setValue({key: 'dateOfBirth', value: date}))}
                        validationError={dateOfBirthError}
                        setValidationError={setDateOfBirthError}
                        isValid={isDateOfBirthValid}
                        setIsValid={setIsDateOfBirthValid}
                        afterChange={() => setIsChanged(true)}
                    />
                    <Alert
                        classes="alert-success"
                        content={success}
                    />
                    <SubmitButton isLoading={isLoading} />
                </form>
            </div>
        </div>
    );
};

export default BasicForm;

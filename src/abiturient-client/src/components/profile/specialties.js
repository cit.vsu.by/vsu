import React, {useState, useEffect} from 'react';
import {useSelector} from 'react-redux';

import strings from '../../strings';
import client from '../../client';
import {selectConstants} from '../../redux/slices/constants-slice';
import {selectDownloaded} from '../../redux/slices/downloaded-slice';
import Spinner from '../spinner';

const Specialties = () => {
    const {faculties, educationForms, specialties} = useSelector(selectConstants);
    const downloaded = useSelector(selectDownloaded);

    const [facultyId, setFacultyId] = useState(0);
    const [educationFormId, setEducationFormId] = useState(0);
    const [specialtyId, setSpecialtyId] = useState(0);
    const [isAdding, setIsAdding] = useState(false);
    const [selectedSpecialties, setSelectedSpecialties] = useState([]);
    const [isSelectedSpecialtiesLoading, setIsSelectedSpecialtiesLoading] = useState(false);

    useEffect(() => {
        setIsSelectedSpecialtiesLoading(true);
        client
            .get('/api/users-specialties/me')
            .then(({data}) => setSelectedSpecialties(data))
            .finally(() => setIsSelectedSpecialtiesLoading(false));
    }, []);

    const doSetEducationFormId = value => {
        setEducationFormId(value);
        setSpecialtyId(0);
    };

    const doSetFacultyId = value => {
        setFacultyId(value);
        doSetEducationFormId(0);
    };

    const handleFacultyIdChange = ({target}) => doSetFacultyId(parseInt(target.value, 10));
    const handleEducationFormIdChange = ({target}) => doSetEducationFormId(parseInt(target.value, 10));
    const handleSpecialtyIdChange = ({target}) => setSpecialtyId(parseInt(target.value, 10));

    const specialtyFilter = specialty => specialty.facultyId === facultyId &&
        specialty.educationFormId === educationFormId;

    const getSpecialty = specialtyId => specialties.find(specialty => specialty.id === specialtyId);

    const handleAddClick = () => {
        if (specialtyId !== 0) {
            setIsAdding(true);
            client.post('/api/users-specialties', {
                specialtyId: specialtyId
            }).then(({data}) => {
                if (!selectedSpecialties.find(selectedSpecialty => selectedSpecialty.specialtyId === specialtyId))
                    selectedSpecialties.push(data);
            }).catch(error => {
                if (error.response)
                    console.log(error.response);
            }).finally(() => setIsAdding(false));
        }
    };

    const handleDeleteClick = specialtyId => {
        client.delete(`/api/users-specialties/${specialtyId}`)
            .then(() => setSelectedSpecialties(selectedSpecialties.filter(
                selectedSpecialty => selectedSpecialty.specialtyId !== specialtyId
            )));
    };

    return (
        <div>
            <h5>
                {strings.whereAreYouGoingToGo}
            </h5>
            <p className="text-muted small">
                {strings.selectSomeSpecialties}
            </p>
            {!downloaded.faculties ||
                !downloaded.educationForms ||
                !downloaded.specialties ||
                isSelectedSpecialtiesLoading ?
            <div className="text-center">
                <Spinner />
            </div> :
            <div>
                <div className="form-group">
                    <label htmlFor="facultyId">
                        {strings.faculty}
                    </label>
                    <select
                        id="facultyId"
                        value={facultyId}
                        onChange={handleFacultyIdChange}
                        className="custom-select"
                    >
                        <option value="0">
                            {strings.notSelected}
                        </option>
                        {faculties.map(faculty =>
                        <option key={faculty.id} value={faculty.id}>
                            {faculty.name}
                        </option>
                        )}
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor="educationFormId">
                        {strings.educationForm}
                    </label>
                    <select
                        id="educationFormId"
                        value={educationFormId}
                        onChange={handleEducationFormIdChange}
                        className="custom-select"
                    >
                        <option value="0">
                            {strings.notSelected}
                        </option>
                        {educationForms.map(educationForm =>
                        <option key={educationForm.id} value={educationForm.id}>
                            {educationForm.name}
                        </option>
                        )}
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor="specialtyId">
                        {strings.specialty}
                    </label>
                    <select
                        id="specialtyId"
                        value={specialtyId}
                        onChange={handleSpecialtyIdChange}
                        className="custom-select"
                    >
                        <option value="0">
                            {strings.notSelected}
                        </option>
                        {specialties.filter(specialtyFilter).map(specialty =>
                        <option key={specialty.id} value={specialty.id}>
                            {specialty.name}
                        </option>
                        )}
                    </select>
                </div>
                <button
                    type="button"
                    onClick={handleAddClick}
                    disabled={isAdding}
                    className="btn btn-outline-secondary btn-block btn-sport mb-3"
                >
                    {isAdding &&
                    <span 
                        className="spinner-border spinner-border-sm" 
                        role="status" 
                        aria-hidden="true" 
                    />
                    }
                    {strings.add}
                </button>
                <ul className="list-group list-group-rounded">
                    {selectedSpecialties.map(selectedSpecialty =>
                    <li key={selectedSpecialty.specialtyId} className="list-group-item">
                        <div className="d-flex w-100 justify-content-between">
                            <div />
                            <small> 
                                <svg
                                    xmlns="http://www.w3.org/2000/svg" 
                                    width="16" 
                                    height="16" 
                                    fill="currentColor"
                                    className="bi bi-x delete"
                                    viewBox="0 0 16 16"
                                    onClick={() => handleDeleteClick(selectedSpecialty.specialtyId)}
                                >
                                    <path 
                                        d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707
                                        8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0
                                        1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"
                                    />
                                </svg>
                            </small>
                        </div>
                        <p className="mb-1 small" title={getSpecialty(selectedSpecialty.specialtyId).name}>
                            {getSpecialty(selectedSpecialty.specialtyId).name.substring(0, 100)}...
                        </p>
                    </li>
                    )}
                </ul>
            </div>
            }
        </div>
    );
};

export default Specialties;

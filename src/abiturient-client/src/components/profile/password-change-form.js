import React, {useState} from 'react';

import strings from '../../strings';
import client from '../../client';
import {validate as validateValue} from '../../validators';
import Password, {passwordValidators} from '../password';
import ConfirmedPassword, {validate as validateConfirmedPassword} from '../confirmed-password';
import SubmitButton from '../submit-button';
import Alert from '../alert';

const PasswordChangeForm = () => {
    const [oldPassword, setOldPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [confirmedPassword, setConfirmedPassword] = useState('');
    const [oldPasswordError, setOldPasswordError] = useState('');
    const [newPasswordError, setNewPasswordError] = useState('');
    const [confirmedPasswordError, setConfirmedPasswordError] = useState('');
    const [isOldPasswordValid, setIsOldPasswordValid] = useState(false);
    const [isNewPasswordValid, setIsNewPasswordValid] = useState(false);
    const [isConfirmedPasswordValid, setIsConfirmedPasswordValid] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [success, setSuccess] = useState('');

    const clearValidation = () => {
        setOldPasswordError('');
        setNewPasswordError('');
        setConfirmedPasswordError('');

        setIsOldPasswordValid(false);
        setIsNewPasswordValid(false);
        setIsConfirmedPasswordValid(false);
    };

    const validate = () => {
        const isNewPasswordValid = validateValue(
            newPassword,
            setNewPasswordError,
            setIsNewPasswordValid,
            passwordValidators
        );
        const isConfirmedPasswordValid = validateConfirmedPassword(
            confirmedPassword,
            setConfirmedPasswordError,
            setIsConfirmedPasswordValid,
            newPassword,
            setNewPasswordError,
            setIsNewPasswordValid
        );

        return isNewPasswordValid && isConfirmedPasswordValid;
    };

    const handleSubmit = event => {
        event.preventDefault();

        if (validate()) {
            setIsLoading(true);
            client.post('/api/change-password', {
                oldPassword: oldPassword,
                newPassword: newPassword,
            }).then(() => {
                clearValidation();
                setOldPassword('');
                setNewPassword('');
                setConfirmedPassword('');
                setSuccess(strings.done);
                setTimeout(() => setSuccess(''), 3000);
            }).catch(error => {
                if (error.response) {
                    switch (error.response.status) {
                        case 422:
                            if (error.response.data.errors.oldPassword)
                                setOldPasswordError(strings.invalidPassword);
                            break;
                        default:
                            console.log(error.response);
                    }
                }
            }).finally(() => setIsLoading(false));
        }
    };

    return (
        <div className="card">
            <div className="card-body">
                <form onSubmit={handleSubmit}>
                    <Password
                        id="oldPassword"
                        label={strings.oldPassword}
                        value={oldPassword}
                        setValue={setOldPassword}
                        validationError={oldPasswordError}
                        setValidationError={setOldPasswordError}
                        isValid={isOldPasswordValid}
                        setIsValid={setIsOldPasswordValid}
                        withHelp={false}
                        withValidators={false}
                    />
                    <Password
                        id="newPassword"
                        label={strings.newPassword}
                        value={newPassword}
                        setValue={setNewPassword}
                        validationError={newPasswordError}
                        setValidationError={setNewPasswordError}
                        isValid={isNewPasswordValid}
                        setIsValid={setIsNewPasswordValid}
                    />
                    <ConfirmedPassword
                        id="confirmedPassword"
                        label={strings.confirmPassword}
                        value={confirmedPassword}
                        setValue={setConfirmedPassword}
                        validationError={confirmedPasswordError}
                        setValidationError={setConfirmedPasswordError}
                        isValid={isConfirmedPasswordValid}
                        setIsValid={setIsConfirmedPasswordValid}
                        password={newPassword}
                        setPasswordValidationError={setNewPasswordError}
                        setIsPasswordValid={setIsNewPasswordValid}
                    />
                    <Alert
                        classes="alert-success"
                        content={success}
                    />
                    <SubmitButton isLoading={isLoading} />
                </form>
            </div>
        </div>
    );
};

export default PasswordChangeForm;

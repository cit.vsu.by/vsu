import React, {useState, useContext} from 'react';
import {useDispatch, useSelector} from 'react-redux';

import strings from '../../strings';
import client from '../../client';
import formatter from '../../formatter';
import {AuthContext} from '../../contexts';
import {selectUser, setValue} from '../../redux/slices/user-slice';
import Name from '../name';
import Text from '../text';
import Phone from '../phone';
import Alert from '../alert';
import Number from '../number';
import SubmitButton from '../submit-button';

const FamilyForm = ({setIsChanged = () => {}}) => {
    const user = useSelector(selectUser);
    const dispatch = useDispatch();
    const authContext = useContext(AuthContext);

    const [isMotherFirstNameValid, setIsMotherFirstNameValid] = useState(false);
    const [isMotherLastNameValid, setIsMotherLastNameValid] = useState(false);
    const [isMotherPatronymicValid, setIsMotherPatronymicValid] = useState(false);
    const [isMotherWorkPlaceValid, setIsMotherWorkPlaceValid] = useState(false);
    const [isMotherPhoneValid, setIsMotherPhoneValid] = useState(false);
    const [isFatherFirstNameValid, setIsFatherFirstNameValid] = useState(false);
    const [isFatherLastNameValid, setIsFatherLastNameValid] = useState(false);
    const [isFatherPatronymicValid, setIsFatherPatronymicValid] = useState(false);
    const [isFatherWorkPlaceValid, setIsFatherWorkPlaceValid] = useState(false);
    const [isFatherPhoneValid, setIsFatherPhoneValid] = useState(false);
    const [isNumberOfSiblingsValid, setIsNumberOfSiblingsValid] = useState(false);
    const [success, setSuccess] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    const clearValidation = () => {
        setIsMotherFirstNameValid(false);
        setIsMotherLastNameValid(false);
        setIsMotherPatronymicValid(false);
        setIsMotherWorkPlaceValid(false);
        setIsMotherPhoneValid(false);
        setIsFatherFirstNameValid(false);
        setIsFatherLastNameValid(false);
        setIsFatherPatronymicValid(false);
        setIsFatherWorkPlaceValid(false);
        setIsFatherPhoneValid(false);
        setIsNumberOfSiblingsValid(false);
    };

    const validate = () => true;

    const handleIsMarriedChange = ({target}) => {
        dispatch(setValue({
            key: 'isMarried',
            value: target.checked
        }));
        setIsChanged(true);
    };

    const handleSubmit = event => {
        event.preventDefault();

        if (validate()) {
            setIsLoading(true);
            client.post('/api/users/family', {
                motherFirstName: formatter.str(user.motherFirstName),
                motherLastName: formatter.str(user.motherLastName),
                motherPatronymic: formatter.str(user.motherPatronymic),
                motherWorkPlace: formatter.str(user.motherWorkPlace),
                motherPhone: user.motherPhone,
                fatherFirstName: formatter.str(user.fatherFirstName),
                fatherLastName: formatter.str(user.fatherLastName),
                fatherPatronymic: formatter.str(user.fatherPatronymic),
                fatherWorkPlace: formatter.str(user.fatherWorkPlace),
                fatherPhone: user.fatherPhone,
                isMarried: user.isMarried,
                numberOfSiblings: user.numberOfSiblings,
            }).then(() => {
                clearValidation();
                authContext.getMe();
                setIsChanged(false);
                setSuccess(strings.done);
                setTimeout(() => setSuccess(''), 3000);
            }).catch(error => {
                if (error.response)
                    console.log(error.response);
            }).finally(() => setIsLoading(false));
        }
    };

    return (
        <div className="card">
            <div className="card-body">
                <form onSubmit={handleSubmit}>
                    <h5>
                        {strings.mother}
                    </h5>
                    <Name
                        id="motherLastName"
                        label={strings.lastName}
                        value={user.motherLastName}
                        setValue={value => dispatch(setValue({key: 'motherLastName', value: value}))}
                        isValid={isMotherLastNameValid}
                        setIsValid={setIsMotherLastNameValid}
                        afterChange={() => setIsChanged(true)}
                    />
                    <Name
                        id="motherFirstName"
                        label={strings.firstName}
                        value={user.motherFirstName}
                        setValue={value => dispatch(setValue({key: 'motherFirstName', value: value}))}
                        isValid={isMotherFirstNameValid}
                        setIsValid={setIsMotherFirstNameValid}
                        afterChange={() => setIsChanged(true)}
                    />
                    <Name
                        id="motherPatronymic"
                        label={
                            <div>
                                {strings.patronymic}
                                {<small className="text-muted"> - {strings.ifAvailable}</small>}
                            </div>
                        }
                        value={user.motherPatronymic}
                        setValue={value => dispatch(setValue({key: 'motherPatronymic', value: value}))}
                        isValid={isMotherPatronymicValid}
                        setIsValid={setIsMotherPatronymicValid}
                        afterChange={() => setIsChanged(true)}
                    />
                    <Text
                        id="motherWorkPlace"
                        label={
                            <div>
                                {strings.workPlace}
                                {<small className="text-muted"> - {strings.ifAvailable}</small>}
                            </div>
                        }
                        value={user.motherWorkPlace}
                        setValue={value => dispatch(setValue({key: 'motherWorkPlace', value: value}))}
                        isValid={isMotherWorkPlaceValid}
                        setisValid={setIsMotherWorkPlaceValid}
                        afterChange={() => setIsChanged(true)}
                    />
                    <Phone
                        id="motherPhone"
                        label={
                            <div>
                                {strings.phone}
                                {<small className="text-muted"> - {strings.ifAvailable}</small>}
                            </div>
                        }
                        phone={user.motherPhone}
                        setPhone={value => dispatch(setValue({key: 'motherPhone', value: value}))}
                        isValid={isMotherPhoneValid}
                        setIsValid={setIsMotherPhoneValid}
                        afterChange={() => setIsChanged(true)}
                    />
                    <hr />
                    <h5>
                        {strings.father}
                    </h5>
                    <Name
                        id="fatherLastName"
                        label={strings.lastName}
                        value={user.fatherLastName}
                        setValue={value => dispatch(setValue({key: 'fatherLastName', value: value}))}
                        isValid={isFatherLastNameValid}
                        setIsValid={setIsFatherLastNameValid}
                        afterChange={() => setIsChanged(true)}
                    />
                    <Name
                        id="fatherFirstName"
                        label={strings.firstName}
                        value={user.fatherFirstName}
                        setValue={value => dispatch(setValue({key: 'fatherFirstName', value: value}))}
                        isValid={isFatherFirstNameValid}
                        setIsValid={setIsFatherFirstNameValid}
                        afterChange={() => setIsChanged(true)}
                    />
                    <Name
                        id="fatherPatronymic"
                        label={
                            <div>
                                {strings.patronymic}
                                {<small className="text-muted"> - {strings.ifAvailable}</small>}
                            </div>
                        }
                        value={user.fatherPatronymic}
                        setValue={value => dispatch(setValue({key: 'fatherPatronymic', value: value}))}
                        isValid={isFatherPatronymicValid}
                        setIsValid={setIsFatherPatronymicValid}
                        afterChange={() => setIsChanged(true)}
                    />
                    <Text
                        id="fatherWorkPlace"
                        label={
                            <div>
                                {strings.workPlace}
                                {<small className="text-muted"> - {strings.ifAvailable}</small>}
                            </div>
                        }
                        value={user.fatherWorkPlace}
                        setValue={value => dispatch(setValue({key: 'fatherWorkPlace', value: value}))}
                        isValid={isFatherWorkPlaceValid}
                        setIsValid={setIsFatherWorkPlaceValid}
                        afterChange={() => setIsChanged(true)}
                    />
                    <Phone
                        id="fatherPhone"
                        label={
                            <div>
                                {strings.phone}
                                {<small className="text-muted"> - {strings.ifAvailable}</small>}
                            </div>
                        }
                        phone={user.fatherPhone}
                        setPhone={value => dispatch(setValue({key: 'fatherPhone', value: value}))}
                        isValid={isFatherPhoneValid}
                        setIsValid={setIsFatherPhoneValid}
                        afterChange={() => setIsChanged(true)}
                    />
                    <hr />
                    <div className="form-group">
                        <div className="custom-control custom-checkbox">
                            <input
                                id="isMarried"
                                type="checkbox"
                                checked={user.isMarried}
                                onChange={handleIsMarriedChange}
                                className="custom-control-input"
                            />
                            <label htmlFor="isMarried" className="custom-control-label">
                                {strings.isMarried}
                            </label>
                        </div>
                    </div>
                    <Number
                        id="numberOfSiblings"
                        label={strings.numberOfSiblings}
                        value={user.numberOfSiblings}
                        setValue={value => dispatch(setValue({key: 'numberOfSiblings', value: value}))}
                        isValid={isNumberOfSiblingsValid}
                        setIsValid={setIsNumberOfSiblingsValid}
                        afterChange={() => setIsChanged(true)}
                    />
                    <Alert
                        classes="alert-success mt-3"
                        content={success}
                    />
                    <SubmitButton isLoading={isLoading} />
                </form>
            </div>
        </div>
    );
};

export default FamilyForm;

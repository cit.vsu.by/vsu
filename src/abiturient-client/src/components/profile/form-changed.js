import React from 'react';
import strings from '../../strings';

const FormChanged = () => {
    return (
        <span className="badge badge-warning ml-1" title={strings.changedTitle}>
            {strings.changed}
        </span>
    );
};

export default FormChanged;

import React from 'react';
import {useCookies} from 'react-cookie';

import strings from '../../strings';

const SettingsForm = () => {
    const [cookies, setCookie] = useCookies([
        'isShownHelp',
    ]);

    const handleIsShownHelpChange = ({target}) => setCookie('isShownHelp', target.checked);

    return (
        <div className="card">
            <div className="card-body">
                <form onSubmit={event => event.preventDefault()}>
                    <div className="form-group">
                        <div className="custom-control custom-switch">
                            <input
                                id="isShownHelp"
                                type="checkbox"
                                checked={cookies.isShownHelp === true || cookies.isShownHelp === 'true'}
                                onChange={handleIsShownHelpChange}
                                className="custom-control-input"
                            />
                            <label htmlFor="isShownHelp" className="custom-control-label">
                                {strings.isShownHelp}
                            </label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default SettingsForm;

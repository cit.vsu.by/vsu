import React, {useState, useContext, useEffect} from 'react';
import {useHistory} from 'react-router-dom';
import {useBeforeunload} from 'react-beforeunload';

import strings from '../../strings';
import {AuthContext} from '../../contexts';
import Layout from '../layout';
import Progress from './progress';
import NotVerifiedEmailToast from '../not-verified-email-toast';
import FormChanged from './form-changed';
import BasicForm from './basic-form';
import PassportForm from './passport-form';
import AddressForm from './address-form';
import EducationForm from './education-form';
import FamilyForm from './family-form';
import OtherForm from './other-form';
import PasswordChangeForm from './password-change-form';
import SpecialtiesForm from './specialties-form';
import SettingsForm from './settings-form';

const tabs = [
    {key: 'basic', name: strings.basic},
    {key: 'passport', name: strings.passportData},
    {key: 'address', name: strings.address},
    {key: 'education', name: strings.education},
    {key: 'family', name: strings.family},
    {key: 'other', name: strings.other},
    {key: 'specialties', name: strings.specialties},
];

const rightTabs = [
    {key: 'settings', name: strings.settings},
    {key: 'changePassword', name: strings.changePassword},
];

const Profile = () => {
    useBeforeunload(event => event.preventDefault());

    const authContext = useContext(AuthContext);
    const history = useHistory();

    const [currentTab, setCurrentTab] = useState('basic');
    const [changedForms, setChangedForms] = useState({});

    useEffect(() => {
        const mutableChangedForms = {};
        tabs.forEach(tab => mutableChangedForms[tab.key] = false);
        setChangedForms(mutableChangedForms);
    }, []);

    const setFormChanged = (key, isChanged) => {
        const mutableChangedForms = {...changedForms};
        mutableChangedForms[key] = isChanged;
        setChangedForms(mutableChangedForms);
    };

    const handleTabLinkClick = (event, tab) => {
        event.preventDefault();
        setCurrentTab(tab);
        window.scrollTo(0, 0);
    };

    const handleLogoutLinkClick = event => {
        event.preventDefault();
        authContext.logOut(() => history.push('/'));
    };

    const getFormByTab = () => {
        switch (currentTab) {
            case 'basic':
                return <BasicForm setIsChanged={isChanged => setFormChanged('basic', isChanged)} />;
            case 'passport':
                return <PassportForm setIsChanged={isChanged => setFormChanged('passport', isChanged)} />;
            case 'address':
                return <AddressForm setIsChanged={isChanged => setFormChanged('address', isChanged)} />;
            case 'education':
                return <EducationForm setIsChanged={isChanged => setFormChanged('education', isChanged)} />;
            case 'family':
                return <FamilyForm setIsChanged={isChanged => setFormChanged('family', isChanged)} />;
            case 'other':
                return <OtherForm setIsChanged={isChanged => setFormChanged('other', isChanged)} />;
            case 'changePassword':
                return <PasswordChangeForm />
            case 'specialties':
                return <SpecialtiesForm />
            case 'settings':
                return <SettingsForm />
            default: return '';
        }
    };

    const getTabLink = (tab, isChangedShown = true) => (
        <a
            key={tab.key}
            href="/profile"
            className={`list-group-item list-group-item-action 
                ${tab.key === currentTab ? 'active' : ''}`}
            onClick={event => handleTabLinkClick(event, tab.key)}
        >
            {tab.name}
            {(isChangedShown && changedForms[tab.key] === true) &&
            <FormChanged />
            }
        </a>
    );

    return (
        <Layout title={strings.titles.profile}>
            <NotVerifiedEmailToast />
            <div className="container">
                <div className="row">
                    <div className="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 mb-3">
                        <div className="row sticky-top">
                            <div className="col-12 mb-3">
                                <div className="card">
                                    <div className="card-body">
                                        <h4 align="center">
                                            {strings.profile}
                                        </h4>
                                        <hr />
                                        <ul className="list-group list-group-flush">
                                            {tabs.map(tab => getTabLink(tab))}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12">
                                <div className="card">
                                    <div className="card-body">
                                        <Progress />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 mb-3">
                        {getFormByTab()}
                    </div>
                    <div className="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                        <div className="card sticky-top">
                            <div className="card-body">
                                <ul className="list-group list-group-flush">
                                    {rightTabs.map(tab => getTabLink(tab, false))}
                                    <a
                                        href="/logout"
                                        className="list-group-item list-group-item-action"
                                        onClick={handleLogoutLinkClick}
                                    >
                                        {strings.logOut}
                                    </a>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default Profile;

import React, {useContext, useState, useEffect} from "react";
import {useSelector, useDispatch} from 'react-redux';

import strings from '../../strings';
import client from '../../client';
import formatter from '../../formatter';
import {AuthContext} from '../../contexts';
import {selectUser, setValue} from '../../redux/slices/user-slice';
import {selectConstants} from '../../redux/slices/constants-slice';
import Alert from '../alert';
import SubmitButton from '../submit-button';
import Sport from './sport';
import Text from '../text';
import NumberInput from '../number-input';

const OtherForm = ({setIsChanged = () => {}}) => {
    const user = useSelector(selectUser);
    const constants = useSelector(selectConstants);
    const dispatch = useDispatch();
    const authContext = useContext(AuthContext);

    const [seniorityYears, setSeniorityYears] = useState(Math.floor(user.seniority / 12));
    const [seniorityMonths, setSeniorityMonths] = useState(user.seniority % 12);
    const [success, setSuccess] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [isWorkPlaceValid, setIsWorkPlaceValid] = useState(false);
    const [isSeniorityYearsValid, setIsSeniorityYearsValid] = useState(false);
    const [isSeniorityMonthsValid, setIsSeniorityMonthsValid] = useState(false);

    useEffect(() => {
        setSeniorityYears(Math.floor(user.seniority / 12));
        setSeniorityMonths(user.seniority % 12);
    }, [user.seniority])

    const clearValidation = () => {
        setIsWorkPlaceValid(false);
        setIsSeniorityYearsValid(false);
        setIsSeniorityMonthsValid(false);
    };

    const validate = () => true;

    const recount = () => {
        let years = seniorityYears;
        let months = seniorityMonths;
        while (months >= 12) {
            years++;
            months -= 12;
        }
        setSeniorityYears(years);
        setSeniorityMonths(months);
    };

    const handleSeniorityMonthsBlur = () => recount();

    const handleSeniorityMonthsKeyPress = event => {
        if (event.key === 'Enter')
            recount();
    };

    const handleIsBrsmMemberChange = ({target}) => {
        dispatch(setValue({
            key: 'isBrsmMember',
            value: target.checked
        }));
        setIsChanged(true);
    };

    const handleIsDormitoryNeededChange = ({target}) => {
        dispatch(setValue({
            key: 'isDormitoryNeeded',
            value: target.checked
        }));
        setIsChanged(true);
    };

    const handleIsMilitaryDutyChange = ({target}) => {
        dispatch(setValue({
            key: 'isMilitaryDuty',
            value: target.checked
        }));
        setIsChanged(true);
    };

    const handleRecruitingOfficeChange = ({target}) => {
        dispatch(setValue({
            key: 'recruitingOfficeId', 
            value: parseInt(target.value, 10)
        }));
        setIsChanged(true);
    };

    const handleSubmit = event => {
        event.preventDefault();

        if (validate()) {
            setIsLoading(true);
            client.post('/api/users/other', {
                isBrsmMember: user.isBrsmMember,
                isDormitoryNeeded: user.isDormitoryNeeded,
                isMilitaryDuty: user.isMilitaryDuty,
                recruitingOfficeId: formatter.foreignKey(user.recruitingOfficeId),
                workPlace: formatter.str(user.workPlace),
                seniority: seniorityYears * 12 + seniorityMonths,
            }).then(() => {
                clearValidation();
                authContext.getMe();
                setIsChanged(false);
                setSuccess(strings.done);
                setTimeout(() => setSuccess(''), 3000);
            }).catch(error => {
                if (error.response)
                    console.log(error.response);
            }).finally(() => setIsLoading(false));
        }
    };

    return (
        <div>
            <div className="card">
                <div className="card-body">
                    <form onSubmit={handleSubmit}>
                        <div className="form-group mt-3">
                            <div className="custom-control custom-checkbox">
                                <input
                                    id="isBrsmMember"
                                    type="checkbox"
                                    checked={user.isBrsmMember}
                                    onChange={handleIsBrsmMemberChange}
                                    className="custom-control-input"
                                />
                                <label htmlFor="isBrsmMember" className="custom-control-label">
                                    {strings.isBrsmMember}
                                </label>
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="custom-control custom-checkbox">
                                <input
                                    id="isDormitoryNeeded"
                                    type="checkbox"
                                    checked={user.isDormitoryNeeded}
                                    onChange={handleIsDormitoryNeededChange}
                                    className="custom-control-input"
                                />
                                <label htmlFor="isDormitoryNeeded" className="custom-control-label">
                                    {strings.isDormitoryNeeded}
                                </label>
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="custom-control custom-checkbox">
                                <input
                                    id="isMilitaryDuty"
                                    type="checkbox"
                                    checked={user.isMilitaryDuty}
                                    onChange={handleIsMilitaryDutyChange}
                                    className="custom-control-input"
                                />
                                <label htmlFor="isMilitaryDuty" className="custom-control-label">
                                    {strings.isMilitaryDuty}
                                </label>
                            </div>
                        </div>
                        {user.isMilitaryDuty &&
                        <div className="form-group">
                            <label htmlFor="recruitingOffice">
                                {strings.recruitingOffice}
                            </label>
                            <select
                                id="recruitingOffice"
                                value={user.recruitingOfficeId}
                                onChange={handleRecruitingOfficeChange}
                                className="custom-select"
                            >
                                <option value="0">
                                    {strings.notSelected}
                                </option>
                                {constants.recruitingOffices.map(recruitingOffice =>
                                <option key={recruitingOffice.id} value={recruitingOffice.id}>
                                    {recruitingOffice.name}
                                </option>
                                )}
                            </select>
                        </div>
                        }
                        <hr />
                        <Text
                            id="workPlace"
                            label={strings.workPlace}
                            value={user.workPlace}
                            setValue={value => dispatch(setValue({
                                key: 'workPlace',
                                value: value
                            }))}
                            isValid={isWorkPlaceValid}
                            setIsValid={setIsWorkPlaceValid}
                            help={strings.workPlaceHelp}
                            afterChange={() => setIsChanged(true)}
                        />
                        <div className="form-group">
                            <h5>
                                {strings.seniority}
                            </h5>
                            <div className="row">
                                <div className="col">
                                    <label htmlFor="seniorityYears">
                                        {strings.years}
                                    </label>
                                    <NumberInput
                                        id="seniorityYears"
                                        value={seniorityYears}
                                        setValue={setSeniorityYears}
                                        isValid={isSeniorityYearsValid}
                                        setIsValid={setIsSeniorityYearsValid}
                                        afterChange={() => setIsChanged(true)}
                                    />
                                </div>
                                <div className="col">
                                    <label htmlFor="seniorityMonths">
                                        {strings.months}
                                    </label>
                                    <NumberInput
                                        id="seniorityMonths"
                                        value={seniorityMonths}
                                        setValue={setSeniorityMonths}
                                        isValid={isSeniorityMonthsValid}
                                        setIsValid={setIsSeniorityMonthsValid}
                                        afterBlur={handleSeniorityMonthsBlur}
                                        onKeyPress={handleSeniorityMonthsKeyPress}
                                        afterChange={() => setIsChanged(true)}
                                    />
                                </div>
                            </div>
                        </div>
                        <Alert
                            classes="alert-success"
                            content={success}
                        />
                        <SubmitButton isLoading={isLoading} />
                    </form>
                </div>
            </div>
            <div className="card mt-3">
                <div className="card-body">
                    <Sport />
                </div>
            </div>
        </div>
    );
};

export default OtherForm;

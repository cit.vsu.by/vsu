import React, {useState, useEffect, useCallback} from 'react';
import {useSelector} from 'react-redux';

import strings from '../../strings';
import {selectConstants} from '../../redux/slices/constants-slice';
import {selectDownloaded} from '../../redux/slices/downloaded-slice';
import {isBelarus} from '../../validators';
import Spinner from '../spinner';

const AddressInput = ({
    id = '',
    localityId = 0,
    setLocalityId = () => {},
    setExternalCountryId = () => {},
    fixCountryId = null,
    afterChange = () => {},
}) => {
    const {localities, districts, regions, countries} = useSelector(selectConstants);
    const downloaded = useSelector(selectDownloaded);

    const [countryId, setCountryId] = useState(fixCountryId ? fixCountryId : 0);
    const [regionId, setRegionId] = useState(0);
    const [districtId, setDistrictId] = useState(0);

    const isDownloading = useCallback(() => !downloaded.localities || !downloaded.districts ||
        !downloaded.regions || !downloaded.countries, [downloaded]);

    const doSetCountryId = useCallback(value => {
        setCountryId(value);
        setExternalCountryId(value);
    }, [setExternalCountryId]);

    const doSetLocalityId = value => {
        setLocalityId(value);
        afterChange();
    };

    useEffect(() => {
        if (localityId !== 0 && !isDownloading()) {
            const locality = localities.find(locality => locality.id === localityId);
            const district = districts.find(district => district.id === locality.districtId);
            const region = regions.find(region => region.id === district.regionId);
            const country = countries.find(country => country.id === region.countryId);

            doSetCountryId(country.id);
            setRegionId(region.id);
            setDistrictId(district.id);
        }
    }, [
        countries,
        regions,
        districts,
        localities,
        localityId,
        doSetCountryId,
        isDownloading,
    ]);

    const setDistrictIdWithLocality = value => {
        setDistrictId(parseInt(value, 10));
        doSetLocalityId(0);
    };

    const setRegionIdWithDistrict = value => {
        setRegionId(parseInt(value, 10));
        setDistrictIdWithLocality(0);
    };

    const setCountryIdWithRegion = value => {
        doSetCountryId(parseInt(value, 10));
        setRegionIdWithDistrict(0);
    };

    const handleCountryChange = ({target}) => {
        setCountryIdWithRegion(target.value);
    };

    const handleRegionChange = ({target}) => {
        setRegionIdWithDistrict(target.value);
    };

    const handleDistrictChange = ({target}) => {
        setDistrictIdWithLocality(target.value);
    };

    const handleLocalityChange = ({target}) => {
        doSetLocalityId(parseInt(target.value, 10));
    };

    const getCountryOption = country => (
        <option key={country.id} value={country.id}>
            {country.name}
        </option>
    );

    const getBelarusOption = () => {
        const belarus = countries.find(country => isBelarus(country));
        return belarus ? getCountryOption(belarus) : '';
    };

    return (
        isDownloading() ?
        <div className="text-center">
            <Spinner />
        </div> :
        <div>
            <div className="form-group">
                <label htmlFor={`country_${id}`}>
                    {strings.country}
                </label>
                <select
                    id={`country_${id}`}
                    value={countryId}
                    onChange={handleCountryChange}
                    className="custom-select"
                    disabled={(fixCountryId ? true : false)}
                >
                    <option value="0">
                        {strings.notSelected}
                    </option>
                    {getBelarusOption()}
                    {countries
                        .filter(country => !isBelarus(country))
                        .map(country => getCountryOption(country))}
                </select>
            </div>
            <div className="form-group">
                <label htmlFor={`region_${id}`}>
                    {strings.region}
                </label>
                <select
                    id={`region_${id}`}
                    value={regionId}
                    onChange={handleRegionChange}
                    className="custom-select"
                >
                    <option value="0">
                        {strings.notSelected}
                    </option>
                    {regions.filter(region => region.countryId === countryId).map(region =>
                    <option key={region.id} value={region.id}>
                        {region.name}
                    </option>
                    )}
                </select>
            </div>
            <div className="form-group">
                <label htmlFor={`district_${id}`}>
                    {strings.district}
                </label>
                <select
                    id={`district_${id}`}
                    value={districtId}
                    onChange={handleDistrictChange}
                    className="custom-select"
                >
                    <option value="0">
                        {strings.notSelected}
                    </option>
                    {districts.filter(district => district.regionId === regionId).map(district =>
                    <option key={district.id} value={district.id}>
                        {district.name}
                    </option>
                    )}
                </select>
            </div>
            <div className="form-group">
                <label htmlFor={`locality_${id}`}>
                    {strings.locality}
                </label>
                <select
                    id={`locality_${id}`}
                    value={localityId}
                    onChange={handleLocalityChange}
                    className="custom-select"
                >
                    <option value="0">
                        {strings.notSelected}
                    </option>
                    {localities.filter(locality => locality.districtId === districtId).map(locality =>
                    <option key={locality.id} value={locality.id}>
                        {locality.name}
                    </option>
                    )}
                </select>
            </div>
        </div>
    );
};

export default AddressInput;

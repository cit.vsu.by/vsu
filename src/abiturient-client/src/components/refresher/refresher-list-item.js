import React, {useState} from 'react';
import $ from 'jquery';
import {useDispatch} from 'react-redux';

import strings from '../../strings';
import client from '../../client';
import {set} from '../../redux/slices/refresh-error-slice';

const RefresherListItem = ({item}) => {
    const dispatch = useDispatch();

    const [isLoading, setIsLoading] = useState(false);

    const handleRefreshClick = () => {
        setIsLoading(true);
        client.post(`/api/${item.path}/refresh`)
            .catch(error => {
                if (error.response) {
                    switch (error.response.status) {
                        case 500:
                            dispatch(set(error.response.data));
                            $('#alertModalRefreshErrors').modal('toggle');
                            break;
                        default:
                            console.log(error.response);
                    }
                }
            })
            .finally(() => setIsLoading(false));
    };

    return (
        <li className="list-group-item d-flex justify-content-between align-items-center">
            {item.name}
            <button
                type="button"
                id={`${item.path}Refresh`}
                className="btn btn-primary"
                onClick={handleRefreshClick}
                disabled={isLoading}
            >
                {isLoading && <span
                    className="spinner-border spinner-border-sm"
                    role="status"
                    aria-hidden="true"
                />
                }
                {strings.refresh}
            </button>
        </li>
    );
};

export default RefresherListItem;

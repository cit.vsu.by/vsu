import React from 'react';

import strings from '../../strings';
import RefresherListItem from './refresher-list-item';

export const items = [
    {
        name: strings.educationalInstitutionsEducationalInstitutionTypes,
        path: 'educational-institutions'
    },
    {
        name: strings.localitiesLocalityTypesDistrictRegionsCountries,
        path: 'localities'
    },
    {
        name: strings.recruitingOffices,
        path: 'recruiting-offices'
    },
    {
        name: strings.sportCategories,
        path: 'sport-categories'
    },
    {
        name: strings.sportTypes,
        path: 'sport-types'
    },
    {
        name: strings.specialtiesTrialsFaculties,
        path: 'specialties-trials'
    },
];

const RefresherList = ({search = ''}) => {
    const getItems = () => items.filter(
        item => item.name
            .toLowerCase()
            .includes(search.toLowerCase())
    ).sort((a, b) => a.name.localeCompare(b.name));

    return (
        <ul className="list-group list-group-rounded">
            {getItems().map((item, i) =>
            <RefresherListItem key={i} item={item} />)
            }
        </ul>
    );
};

export default RefresherList;

import React from 'react';
import {Helmet} from 'react-helmet';

import strings from '../strings';
import Header from './header';
import Footer from './footer';

const Layout = ({title = strings.applicationName, isWithHeader = true, isWithFooter = true, children}) => (
    <div>
        <Helmet>
            <title>{title}</title>
        </Helmet>
        {isWithHeader && <Header />}
        {children}
        {isWithFooter && <Footer />}
    </div>
);

export default Layout;

import React from 'react';

import strings from '../strings';
import {required, password} from '../validators';
import Text from './text';

export const passwordValidators = [required, password];

const Password = ({
    id="",
    label="",
    value = '',
    setValue = () => {},
    validationError = '',
    setValidationError = () => {},
    isValid = false,
    setIsValid = () => {},
    withHelp = true,
    withValidators = true,
}) => {
    return (
        <Text
            id={id}
            label={label}
            type="password"
            value={value}
            setValue={setValue}
            validationError={validationError}
            setValidationError={setValidationError}
            isValid={isValid}
            setIsValid={setIsValid}
            validators={withValidators ? passwordValidators : []}
            help={withHelp === false ? '' : strings.passwordHelp}
        />
    );
};

export default Password;

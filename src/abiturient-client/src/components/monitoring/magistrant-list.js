import React, {useState, useEffect} from 'react';
import moment from 'moment';

import client from '../../client';
import CommonList from './common-list';

const MagistrantList = ({deskId = 0, date = moment(), isAllDates = false}) => {
    const [queue, setQueue] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setIsLoading(true);
        client.get(`/api/magistrant-monitoring/${deskId}`)
            .then(({data}) => setQueue(data))
            .catch(error => {
                if (error.response)
                    console.log(error.response);
            }).finally(() => setIsLoading(false));
    }, [deskId]);

    const getElements = () => queue.map(item => ({
        time: moment(item.dateTime),
        person: `${item.lastName} ${item.firstName} ${item.patronymic ? item.patronymic : ''}`,
        desk: item.desk.description,
        phone: item.phone,
    }));

    return (
        <CommonList
            elements={getElements()}
            isLoading={isLoading}
            date={date}
            deskId={deskId}
            isAllDates={isAllDates}
        />
    );
};

export default MagistrantList;

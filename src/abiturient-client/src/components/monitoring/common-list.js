import React from 'react';
import Moment from 'react-moment';
import moment from 'moment';

import strings from '../../strings';
import Spinner from '../spinner';

const CommonList = ({elements = [], isLoading = false, date = moment(), deskId = 0, isAllDates = false}) => {
    const getFilteredElements = () => isAllDates ? elements : date ? elements.filter(
        item => moment(item.time).format('YYYY-MM-DD') === date.format('YYYY-MM-DD')
    ) : [];

    return (
        isLoading ?
            <div className="text-center">
                <Spinner />
            </div> : getFilteredElements().length === 0 ?
            <p align="center">
                {strings.queueIsEmpty}
            </p> :
            <ul className="list-group list-group-rounded">
                {getFilteredElements().map((element, i) =>
                <li
                    key={i}
                    className="list-group-item d-flex justify-content-between align-items-center"
                >
                    {isAllDates &&
                    <spam className="text-muted">
                        <Moment format="DD.MM">
                            {element.time}
                        </Moment>
                    </spam>
                    }
                    <span className="text-success font-weight-bold">
                        <Moment format="HH:mm">
                            {element.time}
                        </Moment>
                    </span>
                    <div className="d-flex flex-column text-center">
                        <span>
                            {element.person}
                        </span>
                        <span className="text-muted">
                            {element.phone}
                        </span>
                    </div>
                    {deskId === 0 &&
                    <span className="text-muted">
                        {element.desk}
                    </span>
                    }
                </li>
                )}
            </ul>
    );
};

export default CommonList;

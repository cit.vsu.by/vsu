import React, {useState, useEffect} from 'react';
import moment from 'moment';

import client from '../../client';
import CommonList from './common-list';

const AbiturientList = ({deskId = 0, date = moment(), isAllDates = false}) => {
    const [queue, setQueue] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setIsLoading(true);
        client.get(`/api/monitoring/${deskId}`)
            .then(({data}) => setQueue(data))
            .catch(error => {
                if (error.response)
                    console.log(error.response);
            })
            .finally(() => setIsLoading(false));
    }, [deskId]);

    const getElements = () => queue.map(item => ({
        time: moment(item.dateTime),
        person: `${item.user.lastName} ${item.user.firstName} ${item.user.patronymic ? item.user.patronymic : ''}`,
        desk: item.desk.description,
        phone: item.user.phone ? item.user.phone : item.user.email,
    }));

    return (
        <CommonList
            elements={getElements()}
            isLoading={isLoading}
            date={date}
            deskId={deskId}
            isAllDates={isAllDates}
        />
    );
};

export default AbiturientList;

import React from 'react';

import TextInput from './text-input';
import formatter from '../formatter';

const NameInput = ({
    id = '',
    value = '',
    setValue = () => {},
    validationError = '',
    setValidationError = () => {},
    isValid = false,
    setIsValid = () => {},
    placeholder = '',
    lang = 'ru',
    validators = [],
    autoFocus = false,
    help = '',
    afterChange = () => {},
}) => {
    const formatName = (value, lang = 'ru') => {
        switch (lang) {
            case 'en':
                return value.replace(/[^A-Za-z' -]/g, '');
            case 'by':
                return value.replace(/[^А-За-зІіЙ-Уй-уЎўФ-Шф-шЫ-Яы-я' -]/g, formatter.translateRuToBy);
            case 'ru':
            default:
                return value.replace(/[^А-Яа-я' -]/g, '');
        }
    };
    const setName = value => setValue(formatter.capitalize(formatName(value, lang)));

    return (
        <TextInput
            id={id}
            type="text"
            value={value}
            setValue={setName}
            validationError={validationError}
            setValidationError={setValidationError}
            isValid={isValid}
            setIsValid={setIsValid}
            placeholder={placeholder}
            validators={validators}
            autoFocus={autoFocus}
            help={help}
            afterChange={afterChange}
        />
    );
};

export default NameInput;

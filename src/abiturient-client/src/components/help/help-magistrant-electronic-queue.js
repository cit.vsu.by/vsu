import React from 'react';

import strings from '../../strings';

const HelpMagistrantElectronicQueue = () => (
    <div>
        <p align="justify">
            {strings.magistrantElectronicQueueHelp1}
        </p>
        <p align="justify">
            {strings.magistrantElectronicQueueHelp2}
        </p>
        <p align="justify">
            {strings.magistrantElectronicQueueHelp3}
        </p>
    </div>
);

export default HelpMagistrantElectronicQueue;

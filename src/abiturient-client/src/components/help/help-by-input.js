import React, {useState} from 'react';

import strings from '../../strings';
import Name from '../name';

const HelpByInput = () =>  {
    const [exampleByInput, setExampleByInput] = useState('');

    return (
        <div>
            <p align="justify">
                {strings.byInputHelp1}
            </p>
            <p align="justify" className="mb-4">
                {strings.byInputHelp2}
            </p>
            <Name
                id="exampleByInput"
                label={strings.exampleByInput}
                value={exampleByInput}
                setValue={setExampleByInput}
                lang="by"
            />
        </div>
    );
};

export default HelpByInput;

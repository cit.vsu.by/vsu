import React from 'react';

import strings from '../../strings';
import LoadingIndicatorSpinner from '../loading-indicator-spinner';

const HelpLoadingIndicator = () => (
    <div>
        <div className="d-flex justify-content-center mb-3">
            <LoadingIndicatorSpinner isOnTop={false} />
        </div>
        <p align="justify">
            {strings.loadingIndicatorHelp1}
        </p>
        <p align="justify">
            {strings.loadingIndicatorHelp2}
        </p>
    </div>
);

export default HelpLoadingIndicator;

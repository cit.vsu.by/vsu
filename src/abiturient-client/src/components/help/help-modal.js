import React, {useEffect, useState} from 'react';
import {useCookies} from 'react-cookie';
import $ from 'jquery';

import strings from '../../strings';
import Modal from '../modal';
import HelpLoadingIndicator from './help-loading-indicator';
import HelpByInput from './help-by-input';
import HelpMagistrantElectronicQueue from './help-magistrant-electronic-queue';
import HelpSupport from './help-support';

const helps = [
    <HelpLoadingIndicator />,
    <HelpByInput />,
    <HelpMagistrantElectronicQueue />,
    <HelpSupport />,
];

const HelpModal = () => {
    const [cookies, setCookie] = useCookies([
        'isShownHelp',
        'currentHelp',
    ]);

    const [isDisableHelp, setIsDisableHelp] = useState(false);

    useEffect(() => {
        if (!cookies.isShownHelp) {
            setCookie('isShownHelp', true);
            $('#helpModal').modal('toggle')
        }
        if (!cookies.currentHelp)
            setCookie('currentHelp', 0);
    }, [cookies, setCookie]);

    useEffect(() => {
        if (cookies.isShownHelp === true || cookies.isShownHelp === 'true')
            $('#helpModal').modal('toggle');
    }, [cookies.isShownHelp]);

    const handleOkClick = () => {
        if (isDisableHelp)
            setCookie('isShownHelp', false);
    };

    const handleNextClick = () => {
        if (cookies.currentHelp < helps.length - 1)
            setCookie('currentHelp', parseInt(cookies.currentHelp, 10) + 1);
        else
            setCookie('currentHelp', 0);
    };

    return (
        <Modal
            id="helpModal"
            header={strings.didYouKnow}
            body={
                <div>
                    {helps[cookies.currentHelp]}
                    <div className="custom-control custom-checkbox mt-4">
                        <input
                            id="isDisableHelp"
                            type="checkbox"
                            checked={isDisableHelp}
                            onChange={({target}) => setIsDisableHelp(target.checked)}
                            className="custom-control-input"
                        />
                        <label htmlFor="isDisableHelp" className="custom-control-label">
                            {strings.doNotShowAgainHelp}
                        </label>
                    </div>
                </div>
            }
            footer={
                <div>
                    <button
                        type="button"
                        onClick={handleNextClick}
                        className="btn btn-link"
                    >
                        {strings.nextHelp}
                    </button>
                    <button
                        type="button"
                        onClick={handleOkClick}
                        className="btn btn-primary ml-2"
                        data-dismiss="modal"
                    >
                        {strings.ok}
                    </button>
                </div>
            }
        />
    );
};

export default HelpModal;

import React from 'react';

import RegistrationLayout from "../registration-layout";
import RegistrationPassportInfoForm from "./registration-passport-info-form";

const RegistrationPassportInfo = () => (
    <RegistrationLayout form={<RegistrationPassportInfoForm />} />
);

export default RegistrationPassportInfo;

import React, {useState} from 'react';
import {Link, useHistory} from 'react-router-dom';
import {useSelector, useDispatch} from 'react-redux';

import strings from '../../strings';
import {setValue, selectRegistrationData} from '../../redux/slices/registration-data-slice';
import Tabs from '../tabs';
import Passport, {validateNumber, validateSeries} from '../passport';

const RegistrationPassportInfoForm = () => {
    const registrationData = useSelector(selectRegistrationData);
    const dispatch = useDispatch();
    const history = useHistory();

    const [seriesError, setSeriesError] = useState('');
    const [numberError, setNumberError] = useState('');
    const [isSeriesValid, setIsSeriesValid] = useState(false);
    const [isNumberValid, setIsNumberValid] = useState(false);

    const validate = () => {
        const isSeriesValid =
            registrationData.personalDocumentTypeId === 3 ||
            validateSeries(
                registrationData.personalDocumentSeries,
                setSeriesError,
                setIsSeriesValid
            );
        const isNumberValid =
            registrationData.personalDocumentTypeId === 3 ||
                validateNumber(
                registrationData.personalDocumentNumber,
                setNumberError,
                setIsNumberValid
            );

        return isSeriesValid && isNumberValid;
    };

    const handleSubmit = event => {
        event.preventDefault();

        if (validate()) {
            history.push('/registration/ending');
        }
    };

    return (
        <form onSubmit={handleSubmit}>
            <Passport
                citizenship={registrationData.citizenship}
                setCitizenship={value => dispatch(setValue({
                    key: 'citizenship',
                    value: value
                }))}
                documentTypeId={registrationData.personalDocumentTypeId}
                setDocumentTypeId={value => dispatch(setValue({
                    key: 'personalDocumentTypeId',
                    value: value
                }))}
                series={registrationData.personalDocumentSeries}
                setSeries={value => dispatch(setValue({
                    key: 'personalDocumentSeries',
                    value: value
                }))}
                number={registrationData.personalDocumentNumber}
                setNumber={value => dispatch(setValue({
                    key: 'personalDocumentNumber',
                    value: value
                }))}
                seriesError={seriesError}
                setSeriesError={setSeriesError}
                numberError={numberError}
                setNumberError={setNumberError}
                isSeriesValid={isSeriesValid}
                setIsSeriesValid={setIsSeriesValid}
                isNumberValid={isNumberValid}
                setIsNumberValid={setIsNumberValid}
            />
            <input
                type="submit"
                value={strings.next}
                className="btn btn-primary btn-lg btn-block mt-4 mb-3"
            />
            <Tabs numberOfTabs={3} currentTab={1} />
            <Link to="/registration/personal-info">
                {strings.back}
            </Link>
        </form>
    );
};

export default RegistrationPassportInfoForm;

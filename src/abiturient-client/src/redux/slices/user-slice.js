import {createSlice} from '@reduxjs/toolkit';

export const defaultUser = {
    firstName: '',
    lastName: '',
    patronymic: '',
    firstNameBy: '',
    lastNameBy: '',
    patronymicBy: '',
    firstNameEn: '',
    lastNameEn: '',
    genderId: 1,
    dateOfBirth: null,
    citizenship: 1,
    personalDocumentTypeId: 1,
    personalDocumentSeries: '',
    personalDocumentNumber: '',
    personalIdentificationNumber: '',
    personalDocumentOrganization: '',
    personalDocumentIssueDate: null,
    personalDocumentValidity: null,
    placeOfBirth: 0,
    placeOfBirthLocality: null,
    placeOfRegistration: 0,
    placeOfRegistrationLocality: null,
    street: '',
    house: '',
    housing: '',
    apartment: '',
    postcode: '',
    educationalDocumentTypeId: 0,
    educationalInstitutionId: 0,
    educationalInstitution: null,
    graduationDate: null,
    educationalDocumentNumber: '',
    isRuralEducationalInstitution: false,
    isPedagogicalClass: false,
    foreignLanguageId: 0,
    honorId: 0,
    isPreUniversityTraining: false,
    motherFirstName: '',
    motherLastName: '',
    motherPatronymic: '',
    motherWorkPlace: '',
    motherPhone: '',
    fatherFirstName: '',
    fatherLastName: '',
    fatherPatronymic: '',
    fatherWorkPlace: '',
    fatherPhone: '',
    isMarried: false,
    numberOfSiblings: 0,
    isBrsmMember: false,
    isDormitoryNeeded: false,
    isMilitaryDuty: false,
    recruitingOfficeId: 0,
    phone: '',
    workPlace: '',
    seniority: 0,
    queue: [],
    roles: [],
};

export const userSlice = createSlice({
    name: 'user',
    initialState: {
        value: defaultUser
    },
    reducers: {
        set: (state, {payload}) => {
            state.value = payload;
        },
        setValue: (state, {payload}) => {
            state.value[payload.key] = payload.value;
        },
    },
});

export const {set, setValue} = userSlice.actions;

export const selectUser = state => state.user.value;

export default userSlice.reducer;

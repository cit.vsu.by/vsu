import {createSlice} from '@reduxjs/toolkit';

export const defaultConstants = {
    genders: [],
    countries: [],
    personalDocumentTypes: [],
    regions: [],
    districts: [],
    localityTypes: [],
    localities: [],
    streetTypes: [],
    educationalDocumentTypes: [],
    educationalInstitutionTypes: [],
    educationalInstitutions: [],
    foreignLanguages: [],
    honors: [],
    sportTypes: [],
    sportCategories: [],
    recruitingOffices: [],
    faculties: [],
    educationForms: [],
    specialties: [],
    trials: [],
};

export const constantsSlice = createSlice({
    name: 'constants',
    initialState: {
        value: defaultConstants
    },
    reducers: {
        set: (state, {payload}) => {state.value = payload;},
        setValue: (state, {payload}) => {state.value[payload.key] = payload.value;},
    },
});

export const {set, setValue} = constantsSlice.actions;

export const selectConstants = state => state.constants.value;

export default constantsSlice.reducer;

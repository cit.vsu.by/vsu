import {createSlice} from '@reduxjs/toolkit';

export const refreshErrorSlice = createSlice({
    name: 'refreshError',
    initialState: {
        value: '',
    },
    reducers: {
        set: (state, {payload}) => {
            state.value = payload;
        },
    }
});

export const {set} = refreshErrorSlice.actions;

export const selectRefreshError = state => state.refreshError.value;

export default refreshErrorSlice.reducer;

import {createSlice} from '@reduxjs/toolkit';

export const admissionDownloadedSlice = createSlice({
    name: 'admissionDownloaded',
    initialState: {
        value: false,
    },
    reducers: {
        set: (state, {payload}) => {state.value = payload;},
    }
});

export const {set} = admissionDownloadedSlice.actions;

export const selectAdmissionDownloaded = state => state.admissionDownloaded.value;

export default admissionDownloadedSlice.reducer;

import {createSlice} from '@reduxjs/toolkit';
import {defaultUser} from './user-slice';

export const dbUserSlice = createSlice({
    name: 'dbUser',
    initialState: {
        value: defaultUser
    },
    reducers: {
        set: (state, {payload}) => {
            state.value = payload;
        },
        setValue: (state, {payload}) => {
            state.value[payload.key] = payload.value;
        },
    },
});

export const {set, setValue} = dbUserSlice.actions;

export const selectDbUser = state => state.dbUser.value;

export default dbUserSlice.reducer;

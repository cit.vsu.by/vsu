import {createSlice} from '@reduxjs/toolkit';

export const queueErrorModalContentSlice = createSlice({
    name: 'queueErrorModalContent',
    initialState: {
        value: ''
    },
    reducers: {
        set: (state, {payload}) => {
            state.value = payload;
        },
    },
});

export const {set} = queueErrorModalContentSlice.actions;

export const selectQueueErrorModalContent = state => state.queueErrorModalContent.value;

export default queueErrorModalContentSlice.reducer;

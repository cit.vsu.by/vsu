import {createSlice} from '@reduxjs/toolkit';

export const admissionSlice = createSlice({
    name: 'admission',
    initialState: {
        value: null,
    },
    reducers: {
        set: (state, {payload}) => {state.value = payload;},
    }
});

export const {set} = admissionSlice.actions;

export const selectAdmission = state => state.admission.value;

export default admissionSlice.reducer;

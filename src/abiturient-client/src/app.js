import React, { useState, useEffect, useCallback } from 'react';
import { Route, Switch } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useCookies } from 'react-cookie';

import client from './client';
import { AuthContext, AppContext } from './contexts';
import {setValue as setConstantValue, defaultConstants} from './redux/slices/constants-slice';
import {set as setUser, setValue as setUserValue, defaultUser, selectUser} from './redux/slices/user-slice';
import {set as setDbUser, setValue as setDbUserValue, selectDbUser} from './redux/slices/db-user-slice';
import {setDownloaded} from './redux/slices/downloaded-slice';
import {set as setAdmission, selectAdmission} from './redux/slices/admission-slice';
import {set as setAdmissionDownloaded} from './redux/slices/admission-downloaded-slice';
import {set as setMagistrantAdmission} from './redux/slices/magistrant-admission-slice';
import {set as setMagistrantAdmissionDownloaded} from './redux/slices/magistrant-admission-downloaded-slice';
import PrivateRoute, {hasRole, isAdmissionStarted} from './components/private-route';
import Monitoring from './components/monitoring/monitoring';
import Profile from './components/profile/profile';
import Login from './components/login/login';
import RegistrationEnding from './components/registration-ending/registration-ending';
import RegistrationPassportInfo from './components/registration-passport-info/registration-passport-info';
import RegistrationPersonalInfo from './components/registration-personal-info/registration-personal-info';
import Home from './components/home/home';
import EmailVerified from './components/email-verified/email-verified';
import ElectronicQueue from './components/electronic-queue/electronic-queue';
import HelpModal from './components/help/help-modal';
import Abiturients from './components/abiturients/abiturients';
import Refresher from './components/refresher/refresher';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.bundle';

const App = () => {
    const dispatch = useDispatch();

    const [cookies, setCookie] = useCookies(['isAuthenticated', 'isEmailVerified',]);

    const admission = useSelector(selectAdmission);
    const {queue} = useSelector(selectDbUser);
    const user = useSelector(selectUser);

    const checkIsCookieValueTrue = value => value === 'true' || value === true;

    const [isAuthenticated, setIsAuthenticated] = useState(checkIsCookieValueTrue(cookies.isAuthenticated));
    const [isEmailVerified, setIsEmailVerified] = useState(checkIsCookieValueTrue(cookies.isEmailVerified));

    const doSetIsAuthenticated = useCallback(value => {
        setIsAuthenticated(value);
        setCookie('isAuthenticated', value);
    }, [setCookie]);

    const doSetIsEmailVerified = useCallback(value => {
        setIsEmailVerified(value);
        setCookie('isEmailVerified', value);
    }, [setCookie]);

    const getMe = useCallback((onSuccess = () => {}, onError = () => {}, onFinal = () => {}) => {
        client
            .get('/api/users/me')
            .then(({data}) => {
                doSetIsAuthenticated(true);
                if (data.emailVerifiedAt)
                    doSetIsEmailVerified(true);
                else
                    doSetIsEmailVerified(false);

                Object.keys(data).forEach((key) => {
                    switch (key) {
                        case 'dateOfBirth':
                        case 'personalDocumentIssueDate':
                        case 'personalDocumentValidity':
                        case 'graduationDate':
                            if (data[key])
                                dispatch(setUserValue({
                                        key: key,
                                        value: data[key] ? new Date(data[key]) : null
                                }));
                            dispatch(setDbUserValue({
                                key: key,
                                value: data[key] ? new Date(data[key]) : null
                            }));
                            break;
                        default:
                            if (data[key])
                                dispatch(setUserValue({
                                    key: key,
                                    value: data[key]
                                }));
                            dispatch(
                                setDbUserValue({
                                    key: key,
                                    value: data[key],
                                }),
                            );
                    }
                });

                onSuccess();
            })
            .catch(() => {
                doSetIsAuthenticated(false);
                dispatch(setUser(defaultUser));
                dispatch(setDbUser(defaultUser));
                onError();
            })
            .finally(onFinal);
    }, [dispatch, doSetIsAuthenticated, doSetIsEmailVerified]);

    const getAdmission = useCallback((
        onSuccess = () => {},
        onError = () => {},
        onFinal = () => {},
        url,
        setAdmission,
        setAdmissionDownloaded
    ) => {
        dispatch(setAdmissionDownloaded(false));
        client
            .get(url)
            .then(({data}) => {
                dispatch(setAdmission(data));
                onSuccess();
            })
            .catch(() => onError())
            .finally(() => {
                dispatch(setAdmissionDownloaded(true));
                onFinal();
            })
    }, [dispatch]);

    const getAbiturientAdmission = useCallback((onSuccess = () => {}, onError = () => {}, onFinal = () => {}) => getAdmission(
        onSuccess,
        onError,
        onFinal,
        '/api/admissions/current',
        setAdmission,
        setAdmissionDownloaded
    ), [getAdmission]);

    const getMagistrantAdmission = useCallback((onSuccess = () => {}, onError = () => {}, onFinal = () => {}) => getAdmission(
        onSuccess,
        onError,
        onFinal,
        '/api/admissions/current-magistrant',
        setMagistrantAdmission,
        setMagistrantAdmissionDownloaded
    ), [getAdmission]);

    const getCurrentTime = () => queue.find(e => e.admissionId === admission.id);
    const isTimeSelected = () => !!(admission && getCurrentTime());

    useEffect(() => {
        getMe();
        getAbiturientAdmission();
        getMagistrantAdmission();

        Object.keys(defaultConstants).forEach((constant) => {
            client
                .get(`/api/${constant.replace(/[A-Z]/g, s => `-${s.toLowerCase()}`)}`)
                .then(({data}) => {
                    dispatch(setConstantValue({
                        key: constant,
                        value: data.sort((a, b) => a.name.localeCompare(b.name))
                    }));
                    dispatch(setDownloaded({
                        key: constant
                    }));
                });
        });
    }, [dispatch, getAbiturientAdmission, getMagistrantAdmission, getMe]);

    const logOut = (onSuccess = () => {}) => {
        client.post('/logout').then(() => {
            doSetIsAuthenticated(false);
            dispatch(setUser(defaultUser));
            dispatch(setDbUser(defaultUser));

            onSuccess();
        });
    };

    return (
        <AppContext.Provider value={{
            getAdmission: getAbiturientAdmission,
            getMagistrantAdmission: getMagistrantAdmission,
            getCurrentTime: getCurrentTime,
            isTimeSelected: isTimeSelected,
        }}>
            <HelpModal />
            <AuthContext.Provider value={{
                isAuthenticated: isAuthenticated,
                isEmailVerified: isEmailVerified,
                getMe: getMe,
                logOut: logOut,
            }}>
                <Switch>
                    <PrivateRoute
                        path="/refresher"
                        component={Refresher}
                        role={hasRole(user, 'DEVELOPER')}
                    />
                    <PrivateRoute
                        path="/abiturients"
                        component={Abiturients}
                        role={hasRole(user, 'EMPLOYEE')}
                    />
                    <Route path="/magistrant-electronic-queue">
                        <ElectronicQueue isMagistrant={true} />
                    </Route>
                    <PrivateRoute
                        path="/electronic-queue"
                        component={ElectronicQueue}
                        condition={isAdmissionStarted(admission)}
                    />
                    <Route path="/email-verified" component={EmailVerified} />
                    <PrivateRoute
                        path="/monitoring"
                        role={hasRole(user, 'EMPLOYEE')}
                        component={Monitoring}
                    />
                    <PrivateRoute path="/profile" component={Profile} />
                    <Route path="/login" component={Login} />
                    <Route path="/registration/ending" component={RegistrationEnding} />
                    <Route path="/registration/passport-info" component={RegistrationPassportInfo} />
                    <Route path="/registration/personal-info" component={RegistrationPersonalInfo} />
                    <Route path="/" component={Home} />
                </Switch>
            </AuthContext.Provider>
        </AppContext.Provider>
    );
};

export default App;

@ECHO OFF
RMDIR /S /Q ..\abiturient\public\static\
XCOPY .\build\* ..\abiturient\public\ /S /Y /F
DEL ..\abiturient\public\index.html
XCOPY .\build\index.html ..\abiturient\resources\views\index.blade.php /Y /F
PAUSE
